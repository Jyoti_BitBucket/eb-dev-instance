@RestResource(urlMapping='/DemoUrl/*')

global class MyRestResourcedemo {

    global class RequestWrapper{
        public Contact[] con;
       
    }
    
    global class ResponseWrapper { 
        public String StatusCode;
        public String StatusMessage;
        public Contact[] con;   
    }

    @Httpget
    global static string doGet()
    {
        return 'Hello';
    } 

    @HttpPost
    global static ResponseWrapper doPost(RequestWrapper reqst) {
        ResponseWrapper resp = new ResponseWrapper(); 
        try{
        system.debug('::Request.con::'+reqst.con);
            Update reqst.con;
        }
        catch( Exception e ) {
            resp.statusCode = 'Error';
            resp.statusMessage = 'Exception : ' + e.getMessage();
       }

        resp.statusCode = 'Done';
        resp.statusMessage = 'Test success message';
        resp.con = reqst.con;
        return resp;

    }

  }