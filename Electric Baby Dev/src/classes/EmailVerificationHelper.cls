@RestResource(urlMapping='/SendVerification/*')
global class EmailVerificationHelper {
    // method to update the dummy Contact for sending email verification
    @HttpGet
    global static string doGet() {
        string email,verificationCode;
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String urlParamString = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        system.debug(' urlParamString '+urlParamString);
        string [] paramArr = urlParamString.split('&');
       
        if( paramArr != null && paramArr.size() > 0 )
        {
            for(string iter : paramArr)
            {
                list<string> params = new list<string>();
                params = iter.split('=');
                if( params != null && params.size() > 0 )
                {
                    if(params[0] == 'email')
                    {
                        email = params[1];
                    }
                    
                    if(params[0] == 'code')
                    {
                        verificationCode = params[1];
                    }
                }
            }   
        }
        
        list<Contact> result = [SELECT Id, Name, Phone FROM Contact WHERE Title like 'Testing%'];
        if( result != null && result.size() > 0 )
        {
            string conID = result[0].id;
            Contact con = new Contact(id=conID);
            con.email = email;
            con.Verification_Code__c = verificationCode;
            update con;
        }
        else
        {
            Contact con = new Contact();
            con.Title = 'Testing';
            con.FirstName = 'Test';
            con.LastName = 'Person';
            con.email = email;
            con.Verification_Code__c = verificationCode;
            insert con;
        }
        //Account result = [SELECT Id, Name, Phone, Website FROM Account WHERE Id = :accountId];
        return urlParamString; 
    }
}