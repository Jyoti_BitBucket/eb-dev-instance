public with sharing class OnlineDonationsDev {

  // Getters and Setters
  public CnP_IaaS.PaaS_Class cnpxmlstring{get;set;}
  public CnP_IaaS.PaaS_Class.Operation Operation{get;set;}
  public CnP_IaaS.PaaS_Class.Order Order{get;set;}
  public CnP_IaaS.PaaS_Class.Authentication Authentication{get;set;}
  public CnP_IaaS.PaaS_Class.BillingInformation BillingInfo{get;set;}
  public CnP_IaaS.PaaS_Class.BillingAddress BillingAddress{get;set;}
  public CnP_IaaS.PaaS_Class.PaymentMethod PaymentMethod{get;set;}
  public CnP_IaaS.PaaS_Class.Receipt Receipt{get;set;}
  public CnP_IaaS.PaaS_Class.cnpTransaction cnpTransaction{get;set;}

  //Fields to Hold Settings from CNP    
  private CnP_PaaS__CnP_API_Settings__c[] SettingsList { get; set; }
  public List<CnP_IaaS.PaaS_Class.Items> Itemlist{get;set;}
  public CnP_IaaS.PaaS_Class.Items item { get; set; }     
  public Boolean settingsError{set;get;}

  // Custom Setting Fields
  public String thankYouPage { get; set; } // For redirect upon succesful transaction
  public String orderMode { get; set; } // To show hide sections in VF page if "Test" mode
  public String supportEmail { get; set; } // To show support email 
  public String CnPAccountGUID { get; set; } // To show support email 
  public String CnPAccountNumber { get; set; } // To show support email  
  public Donation_Form_Settings__c formSettings {get;set;}

    
  // Fields for Custom Questions

  // Other Amount    
  public Decimal OtherAmount{ get;set; } 
  
  // for showing error block
  
  public integer isSuccess { get; set ;}
  
  
  // field for news letter checkbox
  public boolean chkNewsLetter { get; set;}
  // Designation
  string DesignationType = '';  
  string DesignationDefault;
  public void setDesignationType(string S){this.DesignationType = S;}  // We aren't setting but still required by select list in VF
  public string getDesignationType(){return DesignationType;}

  // Dediaction
  public String DedicationType = 'No Dedication'; 
  public void setDedicationType(string S){this.DedicationType = S;} // We aren't setting but still required by select list in VF
  public string getDedicationType(){return DedicationType;}
  public String DedicatedToName{ get;set; } 

  // Recognition
  public String RecognitionName{ get;set; } //Grab text question value
  public String RecognitionAnonymous{get;set;} //Grab text question value

  // Lead Source
  public string LeadSource{ get; set; }

  // Newsletter
  public String Newsletter { get; set; } // For Newsletter sign up- string not boolean casue we can only pass text
  
  // Extra Select List 
  public string ExtraSelect{ get; set; } 

  // Extra Text Input
  public string ExtraText{ get; set; }   

  // Extra Hidden Field
  public String ExtraHidden { get; set; }
  
  // SKU
  public String CustomSKU { get; set; }  // SKU passed from VisualForce page
  public String URLSKU { get; set; }  // SKU passed from URL

  // Campaign
  public String CustomCampaign { get; set; }  // Campaign passed from VisualForce page
  public String URLCampaign { get; set; }  // Campaign passed from URL
    
  // Amount
  public String URLAmount { get; set; }  // Amount passed from URL

  // Fields for C&P Results  
  public string transactionCode { get; set; }
  public string transactionResult { get; set; }
  public string transactionNumber { get; set; }   
  
  // Fields for showing amount radio buttons
  
  public string country {get; set;}
  
  //method for getting amount
  public List<SelectOption> getAmounts() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('2500.0','$2500')); 
        options.add(new SelectOption('1000.0','$1000')); 
        options.add(new SelectOption('500.0','$500')); 
        options.add(new SelectOption('100.0','$100')); 
        options.add(new SelectOption('50.0','$50'));
        options.add(new SelectOption('0.0','Other'));
        return options; 
    }
  
    
  // Fields passed in from VisualForce Page  
  public PageReference HiddenParams() {
    CustomSKU = Apexpages.currentPage().getParameters().get('customSKU');
    ExtraHidden = Apexpages.currentPage().getParameters().get('extraHidden');
    return null;
  }
      
  // Fields for Mailing on Failed Transacitons
  public Boolean flag{get;set;}
  public Boolean flag1{get;set;}
  public Boolean MailCheck {get;set;}
  public String  MailTextArea {get;set;}
  public String MailAcknowledgement {get;set;}
  public String MailAddress {get;set;}
  public Boolean getflag(){return flag;}
  public Boolean getflag1(){return flag1;}
  public void doRefresh(){
    if ( MailAcknowledgement == 'Send Email acknowledgement' ){
      flag = true;
      flag1 = false;
    }else{
      flag=false;
      flag1 = true;
    }
    getflag1();//Return the Set Boolean
    getFlag(); //Return the Set Boolean
  }
  public String GivingDonation {get;set;}   //?????????????
 
    // String to be displayed in the VF page Optional
  public String CustomString { get; set; }  


/************ Constructor Starts **************/

public OnlineDonationsDev(){
    flag = false;
    flag1 = false;
    isSuccess = 0;
	chkNewsLetter = false;
  // Instantiate C&P objects
  Operation=new CnP_IaaS.PaaS_Class.Operation(); 
  Order=new CnP_IaaS.PaaS_Class.Order();  
  Authentication=new CnP_IaaS.PaaS_Class.Authentication();
  BillingInfo=new CnP_IaaS.PaaS_Class.BillingInformation();
  BillingAddress=new CnP_IaaS.PaaS_Class.BillingAddress();
  PaymentMethod=new CnP_IaaS.PaaS_Class.PaymentMethod();
  Receipt=new CnP_IaaS.PaaS_Class.Receipt();
  Receipt.SendReceipt = false; // Do not send C&P default receipt
  CnPXMLString=new CnP_IaaS.PaaS_Class();
  CnPTransaction=new CnP_IaaS.PaaS_Class.cnpTransaction();
  Itemlist=new list<CnP_IaaS.PaaS_Class.Items>();

  //Set default donation to .01
  item = new CnP_IaaS.PaaS_Class.Items();
  item.UnitPrice = 25.00; // Default Amount to keep Other Amount from showing
  // Using only one item in this transaction. Set non-price details
  item.SKU = 'ONLINE'; // Default Sku ???Not necessary
  item.ItemName = 'Donation'; // Can get renamed in C&P settings
  ItemList.add(item);

  // Set Default Other Amount
  OtherAmount = 20;

  // Set Default DediacationType (not back to VF)
  DedicationType = 'No Dedication';
    
  // Get Custom Settings
  orderMode = 'Test';
  Donation_Form_Settings__c formSettings = Donation_Form_Settings__c.getInstance();       // This is used in javascript processing() function
  if(formSettings == null || formSettings.Thank_You_Page__c == null || formSettings.Support_Email__c == null || formSettings.CnP_Account_GUID__c == null || formSettings.CnP_Account_Number__c == null) {
    settingsError = true;
    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.warning,'Please enter C&P Account details in Setup > Develop > Custom Settings > Donation Form Settings');  
    ApexPages.addMessage(myMsg);
  }
    
  thankYouPage = formSettings.Thank_You_Page__c; // Assign page for redirect
  supportEmail = formSettings.Support_Email__c; // Assign email for support and for failed transactions
  Order.OrderMode = (formSettings.Order_Mode__c != null) ? formSettings.Order_Mode__c : 'Test';   
  orderMode=Order.OrderMode; // for VF page flag
  CnPTransaction.TransactionType = 'Payment';
  CnPAccountGUID = formSettings.CnP_Account_GUID__c;
  CnPAccountNumber = formSettings.CnP_Account_Number__c;
  CnPTransaction.Periodicity = 'Month'; //Set default recurring periodicity as monthly
  BillingAddress.BillingCountryCode='840'; // Set USA as default country
  DesignationType='Undesignated';

  // Changing the Format for date
  string dd=datetime.now().format('MM/dd/yyyy');
  string mon=dd.substring(0,2);
  string year=dd.substring(7,10);
  PaymentMethod.ExpirationMonth=mon;
  PaymentMethod.ExpirationYear=year;

// Add params from URL
  Map<string, string> params = ApexPages.currentPage().getParameters();
  String paramsReceived = 'Received:';

  String a = params.get('cString'); // Retreive Custom Output from URL
  if (a != null && a != '') {
      CustomString = a;
    paramsReceived += 'CustomString=' + CustomString + ', '; 
  } 

  string b = params.get('firstName'); // Retreive First Name from URL
  if (b != null && b != '') {
    BillingInfo.BillingFirstName = b;
    paramsReceived += 'FirstName=' + BillingInfo.BillingFirstName + ', ';
  } 

  string c = params.get('lastName'); // Retreive Last Name from URL
  if (c != null && c != '') {
    BillingInfo.BillingLastName = c;
    paramsReceived += 'LastName=' + BillingInfo.BillingLastName + ', ';
  } 

  string d = params.get('email'); // Retreive Email from URL
  if (d != null && d != '') {
    BillingInfo.BillingEmail = d;
    paramsReceived += 'Email=' + BillingInfo.BillingEmail + ', ';
  } 

  string e = params.get('phone'); // Retreive Phone from URL
  if (e != null && e != '') {
    BillingInfo.BillingPhone = e;
    paramsReceived += 'Phone=' + BillingInfo.BillingEmail + ', ';
  } 

  string f = params.get('address'); // Retreive Address from URL
  if (f != null && f != '') {
    BillingAddress.BillingAddress1 = f;
    paramsReceived += 'Address=' + BillingInfo.BillingEmail + ', ';
  }
 
  string g = params.get('city'); // Retreive Address from URL
  if (g != null && g != '') {
    BillingAddress.BillingCity = g;
    paramsReceived += 'City=' + BillingInfo.BillingEmail + ', ';
  } 
    
  string h = params.get('zip'); // Retreive Zip Code from URL
  if (h != null && h != '') {
    BillingAddress.BillingPostalCode = h;
    paramsReceived += 'ZipCode=' + BillingAddress.BillingPostalCode + ', ';
  } 

  string i = params.get('state'); // Retreive Address from URL
  if (i != null && i != '') {
    BillingAddress.BillingStateProvince = i;
    paramsReceived += 'State=' + BillingInfo.BillingEmail + ', ';
  } 
  
  string j = params.get('county'); // Retreive Address from URL
  if (j != null && j != '') {
    BillingAddress.BillingCountryCode = j;
    paramsReceived += 'Country=' + BillingInfo.BillingEmail + ', ';
  }       

  string k = params.get('amount'); // Retreive Amount from URL
  if (k != null && k != '') {
    URLAmount = k;
    item.unitprice = Decimal.valueOf(k);
    paramsReceived += 'Amount=' + URLAmount + ', ';
  }    

  string l = params.get('sku'); // Retreive SKU from URL
  if (l != null && l != '') {
    URLSKU = l;
    paramsReceived += 'SKU=' + URLSKU + ', ';
    
  }    
  system.debug(paramsReceived);

}
/************* Constructor ends ***********/

    
/******** Process button Action goes here *********/
Public void Submit(){
  if(chkNewsLetter == true )
  {
      NewsLetter = 'TRUE';
  }
  else
  {
      NewsLetter = 'FALSE';
  }
  Authentication.AccountGuid=CnPAccountGUID; //inserting value of Account no getting from C & P Account
  Authentication.AccountID=CnPAccountNumber;  //inserting value of Account GUID getting from C & P Account


  /******** PASSING CUSTOM INFO HERE AND AT BOTTOM LINE *********/  
  
  
  // Check to see if an Amount has been passed in via URL, if not, check if an other amount is used


  if(URLAmount != null){ 
      item.UnitPrice = Decimal.valueOf(URLAmount);
  }else{
    if(item.UnitPrice == 0 && OtherAmount > 0) item.UnitPrice = OtherAmount;
  }
  system.debug('Amount=' + item.UnitPrice);

  // Check to see if a SKU has been passed in via URL, if not use the SKU from the VF page
  if(URLSKU != null){  
    item.SKU = URLSKU;
    DesignationType = URLSKU;
  }else{
    item.SKU = CustomSKU;
  }
  system.debug('SKU=' + item.SKU);

  // Check to see if a Campaign has been passed in via URL, if not use the Campaign from the VF page
  if(URLCampaign != null){
    Order.Campaign = URLCampaign;
  }else if(CustomCampaign != 'null'){
    Order.Campaign = CustomCampaign;
  }
  system.debug('Campaign=' + URLCampaign);

 

  // Concat First name & Last name and passing value into "Name on card"
  PaymentMethod.Nameoncard = BillingInfo.BillingFirstName + ' ' + BillingInfo.BillingLastName ; 
  
  
  // Recurring Info
  cnptransaction.RecurringMethod = 'Subscription';// Passing the Recurring Method as Subscription 
  cnptransaction.Installment = '999';// Passing No. of installments as 999 by default

 
  // Custom Fields
  list<CnP_IaaS.PaaS_Class.Customfields> XmlCustomfieldlist_list = new list<CnP_IaaS.PaaS_Class.Customfields>(); 


  // Fund Designation
  if(DesignationType != ''){
    system.debug('DesignationType=' + DesignationType);
    XmlCustomfieldlist_list.add(createCustomField('Designation',DesignationType)); //adding Fieldname,Fieldvalue to the list
  }


  // Dedication
  XmlCustomfieldlist_list.add(createCustomField('Dedication',DedicationType)); //adding Fieldname,Fieldvalue to the list
  XmlCustomfieldlist_list.add(createCustomField('DedicatedTo',DedicatedToName)); //adding Fieldname,Fieldvalue to the list


  // Recognition
  String recognitionresult = 'Anonymous';
    system.debug('RecognitionAnonymous=' + RecognitionAnonymous);
  if(RecognitionAnonymous == 'false'){
    system.debug('RecognitionAnonymous=' + RecognitionAnonymous);
    recognitionresult = RecognitionName;
  }    
  XmlCustomfieldlist_list.add(createCustomField('Recognition',recognitionresult)); //adding Fieldname,Fieldvalue to the list


  // Lead Source
  system.debug('LeadSource=' + LeadSource);
  XmlCustomfieldlist_list.add(createCustomField('LeadSource',LeadSource)); //adding Fieldname,Fieldvalue to the list


  // Newsletter
  system.debug('Newsletter=' + Newsletter);
  XmlCustomfieldlist_list.add(createCustomField('Newsletter',Newsletter)); //adding Fieldname,Fieldvalue to the list


  // Extra Select
  system.debug('ExtraSelect=' + ExtraSelect);
  XmlCustomfieldlist_list.add(createCustomField('ExtraSelectQuestion',ExtraSelect)); //adding Fieldname,Fieldvalue to the list


  // Extra Text Question
  system.debug('ExtraText=' + ExtraText);
  XmlCustomfieldlist_list.add(createCustomField('ExtraTextQuestion',ExtraText)); //adding Fieldname,Fieldvalue to the list


  // Extra Hidden Field
  XmlCustomfieldlist_list.add(createCustomField('Extra Hidden',ExtraHidden)); //adding Fieldname,Fieldvalue to the list


  /******* Getting the Signatures of related fields ********/
  cnpxmlstring.getOperation_node(Operation); // getting the values of operation node
  cnpxmlstring.getOrder_node(Order); //getting the values of Order node
  cnpxmlstring.getAuthentication_node(Authentication);  //getting the values of Authentication node
  cnpxmlstring.getBillingInformation_node(BillingInfo); //getting the values of Billing Information 
  cnpxmlstring.getBillingAddress_node(BillingAddress); //getting the values of Billing Address node
  cnpxmlstring.getItemslist_node(Itemlist); 
  cnpxmlstring.getTransactionDetails_node(cnpTransaction); //getting the values of Authentication node
  cnpxmlstring.setfieldslist(XmlCustomfieldlist_list); //setting the values of Customfield list
  cnpxmlstring.getCustomFieldList_node(cnpxmlstring); //getting the values of Customfield list
  cnpxmlstring.getPaymentMethod_node(PaymentMethod); //getting the values of payment node
  cnpxmlstring.getreceipt_node(Receipt); //getting the values of Receipt node
  cnpxmlstring.getgeneratexml(cnpxmlstring); // generating the Xml file
  // Methods defined as TestMethod do not support Web service callouts
  //code to send the email for the failed transactions
 

  if (!Test.isRunningTest()){
    cnpxmlstring.Send_Transaction_api(cnpxmlstring.xmlfile); //sending it to the C&P data

    } else {
 
      Account acct0 = new Account(Name='Franklin Tester', Phone = '1234567890', BillingStreet = '123 Main St', BillingCity = 'Frisco',  BillingState = 'CO');
      insert acct0;

      // Instances with NPSP
      //Contact cont0 = new Contact(AccountID=acct0.ID, FirstName='Franklin', LastName='Tester', npe01__Preferred_Email__c='Work', npe01__WorkEmail__c='franklin.joyce@idealistconsulting.com');
      // Instances without NPSP
      Contact cont0 = new Contact(AccountID=acct0.ID, FirstName='Franklin', LastName='Tester', Email='franklin.joyce@idealistconsulting.com');

      insert cont0;

     // Create sample date
     DateTime dt=System.now();
     date d = Date.newInstance(dt.year(),dt.month(),dt.day());

      Opportunity oppt0 = new Opportunity(AccountId=acct0.id, StageName='Posted', CloseDate=d, Amount=100, Name='Test Donation', Description='Test');    
            insert oppt0; 
  }
}

 
public void SendEmail(){   
  system.debug('in the send email function transactionCode ::'+transactionCode );
  system.debug('in the send email function transactionCode ::'+transactionResult );
  system.debug('in the send email function transactionCode ::'+transactionNumber );
  if(transactionCode != '0'){
  isSuccess = 1234;
  system.debug('in the if ');
    string strEmailSub ='Donation Failed';
    string strEmailBody ='';
            
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    string emailAddr = UserInfo.getUserEmail();
    Donation_Form_Settings__c formSettings = Donation_Form_Settings__c.getInstance(); 
    system.debug('formSetting is:: '+formSettings);
    system.debug('formSetting Email is:: '+formSettings.Support_Email__c);
    supportEmail  = 'pravin.waykar@enzigma.com';//formSettings.Support_Email__c;
    system.debug('supportEmail   '+supportEmail  );
    strEmailBody += '<div><h3>Donation</h3></div><br/>'+
      '<div>'+
      '<table>'+
      '<tr><td>Amount:</td><td>'+item.UnitPrice+'</td></tr>'+
      '<tr><td>Frequency:</td><td>'+ cnptransaction.Periodicity +'</td></tr>'+
      '<tr><td>Designation:</td><td>'+DesignationType +'</td></tr>'+
      +'</table>'+
      '</div>'+
      '<div><h3>CONTACT INFORMATION</h3></div><br/>'+
      '<div>'+
      '<table>'+
      '<tr><td>First Name:</td><td>'+ BillingInfo.BillingFirstName+'</td></tr>'+
      '<tr><td>Last Name:</td><td>'+ BillingInfo.BillingLastName+'</td></tr>'+
      '<tr><td>Email:</td><td>'+ BillingInfo.BillingEmail+'</td></tr>'+
      '<tr><td>Confirm Email:</td><td>'+ BillingInfo.BillingEmail+'</td></tr>'+
      '<tr><td>Phone:</td><td>'+BillingInfo.BillingPhone+'</td></tr>'+
      '<tr><td>Address1:</td><td>'+BillingAddress.BillingAddress1+'</td></tr>'+
      '<tr><td>Address2:</td><td>'+BillingAddress.BillingAddress2+'</td></tr>'+
      '<tr><td>City:</td><td>'+BillingAddress.BillingCity+'</td></tr>'+
      '<tr><td>Postal Code:</td><td>'+BillingAddress.BillingPostalCode+'</td></tr>'+
      '<tr><td>Province(non-US):</td><td>'+BillingAddress.BillingStateProvince+'</td></tr>'+
      '<tr><td>Country:</td><td>'+ BillingAddress.BillingCountryCode+'</td></tr>'+
      '</table>'+
      '</div>'+
      '<div><h3>Processing Result</h3></div><br/>'+
      '<div>'+
      '<table>'+
      '<tr><td>Trasaction status:</td><td>'+cnpxmlstring.ErrorData +'</td></tr>'+
      '<tr><td>Trasaction Result:</td><td>'+cnpxmlstring.TransactionResultCode +'</td></tr>'+
      '</table>'+
      '</div>';
                        
      string[] toAddresses = new String[] {supportEmail};
      try{
        if( toAddresses != null && toAddresses.size() > 0  ){
          mail.setToAddresses(toAddresses);
          mail.setSubject(strEmailSub);
          mail.setHtmlBody(strEmailBody);
          system.debug('***mail Content'+mail);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
      }
      catch(Exception ex){
        system.debug('Exception is:: '+ex);
      }
    }
  }  
/******* Create a C&P XML custom field entry ********/

  private static CnP_IaaS.PaaS_Class.Customfields createCustomField(String name, String value) {
    CnP_IaaS.PaaS_Class.Customfields cf = new CnP_IaaS.PaaS_Class.Customfields();
    cf.FieldName = name;
    cf.FieldValue = value;
    return cf;
 
  }
}