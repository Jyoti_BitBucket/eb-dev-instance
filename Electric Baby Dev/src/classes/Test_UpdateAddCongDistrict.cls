/*
 *Summary : This is the class for the congressional district functionality. It contains the unit tests for the Account and Contact 
 *          triggers.
 *Written By : Pravin Waykar
 *
 *Date : 28/07/2014
 */ 

@isTest(seeAllData = true)
public class Test_UpdateAddCongDistrict
{
    static testMethod void myUnitTestForAccount() 
    {
        
         
        //get the custom setting if the trigger is active or not
        Triggers__c  TriggerSettingAccount  = Triggers__c.getValues('OnAccountInsertUpdateProcessCongDist');
        Test.StartTest();
        System.runAs ( new User(Id = UserInfo.getUserId()) ) 
        {
            if( TriggerSettingAccount == null )
            {
                TriggerSettingAccount = new Triggers__c(Name = 'OnAccountInsertUpdateProcessCongDist');
                TriggerSettingAccount.Active__c = true;
                insert TriggerSettingAccount;
                System.assertNotEquals(TriggerSettingAccount.id,null);
            }  
        } 
        
        Account Test_Ac0 = new Account(Name='Test Account1',ShippingPostalCode='80443');
        insert Test_Ac0;
        System.assertNotEquals(Test_Ac0.id, null, 'Account not inserted');
        update Test_Ac0;
        list<id> lstAccIds = new list<id>();
        lstAccIds.add(Test_Ac0.id);   
       
       
        US_Geocoder_Settings__c  GeoCoderSetting  = US_Geocoder_Settings__c.getValues('GeoCoder Settings');
        System.runAs ( new User(Id = UserInfo.getUserId()) ) 
        {
            
            if( GeoCoderSetting == null )
            {
                GeoCoderSetting = new US_Geocoder_Settings__c( Name = 'GeoCoder Settings');
                GeoCoderSetting.API_Key__c = '7910bf06a73d6003ca4c6dcc13db2422';
                GeoCoderSetting.End_Point_URL__c = 'https://usgeocoder.com/api/get_info.php';
                insert GeoCoderSetting;
                System.assertNotEquals( GeoCoderSetting.Id, null);
            }
            
            if( TriggerSettingAccount == null )
            {
                TriggerSettingAccount = new Triggers__c(Name = 'OnAccountInsertUpdateProcessCongDist');
                TriggerSettingAccount.Active__c = true;
                insert TriggerSettingAccount;
                System.assertNotEquals(TriggerSettingAccount.id,null);
            }
        }
        
        // Create Congressional District
        Congressional_District__c CD1 = new Congressional_District__c(Name='AL-02', Congressional_District__c='Congressional District 2',Party__c='Republican',Representative__c ='Test Rep',State__c='Colorado'); 
        insert CD1;
        System.assertNotEquals(CD1.id,null,'Congressional District not inserted');
        
        // Create Congressional District
        Congressional_District__c CD2 = new Congressional_District__c(Name='AL-02', Congressional_District__c='Congressional District 3',Party__c='Republican',Representative__c ='Test Rep',State__c='Colorado'); 
        insert CD2;
        System.assertNotEquals(CD2.id,null,'Congressional District not inserted');
        
         //create test data i.e. Account       
     
        Account Test_Ac1 = new Account(Name='Test Account1',ShippingPostalCode='80443');
        insert Test_Ac1;
        System.assertNotEquals(Test_Ac1.id, null, 'Account not inserted');
        lstAccIds = new list<id>();
        lstAccIds.add(Test_Ac1.id);
        
        // call update trigger
        
        Test_Ac1.USG_Trigger_Update__c = true;
        update Test_Ac1;
        //create test data i.e. Account with Postalcode is not null And Street is not null      
        
        Account Test_Ac2 = new Account(Name='Test Account1',ShippingPostalCode='80443',ShippingStreet ='123 No Address St');
        insert Test_Ac2;
        System.assertNotEquals(Test_Ac2.id, null, 'Account not inserted');
        lstAccIds.add(Test_Ac2.id);
        
        //create test data i.e. Account with Postalcode is null And Street is null      
        
        Account Test_Ac3 = new Account(Name='Test Account1');
        insert Test_Ac3;
        System.assertNotEquals(Test_Ac3.id, null, 'Account not inserted');
        lstAccIds.add(Test_Ac3.id);
         
        CNTRL_AccountInsertUpdateAddCongDistrict.CNTRL_AccountInsertUpdateAddCongDistrict(lstAccIds);
        list<Account> lstAccount = new list<Account>([SELECT id,Name,Latitude__c,Longitude__c,Congressional_District__c  FROM Account WHERE id =: Test_Ac1.id LIMIT 1]);
        Account TempAcc = new Account(id=lstAccount[0].id);
        Test.StopTest();
        
       
    }
    
    // test method for contact trigger
    
    static testMethod void myUnitTestForContact() 
    {
         Triggers__c  TriggerSettingContact  = Triggers__c.getValues('OnContactInsertUpdateProcessCongDist');
         
         Test.StartTest();
        if( TriggerSettingContact == null )
        {
            TriggerSettingContact = new Triggers__c(Name = 'OnContactInsertUpdateProcessCongDist');
            TriggerSettingContact.Active__c = true;
            insert TriggerSettingContact;
            System.assertNotEquals(TriggerSettingContact.id,null);
        }
        
        Contact Test_Con0 = new Contact(LastName='Test Contact1',OtherPostalCode='80443');
        insert Test_Con0;
        System.assertNotEquals(Test_Con0.id, null, 'Contact not inserted');
        list<id> lstContIds = new list<id>();
        lstContIds.add(Test_Con0.id);
        
        US_Geocoder_Settings__c  GeoCoderSetting  = US_Geocoder_Settings__c.getValues('GeoCoder Settings');
        System.runAs ( new User(Id = UserInfo.getUserId()) ) 
        {
            if( GeoCoderSetting == null )
            {
                GeoCoderSetting = new US_Geocoder_Settings__c( Name = 'GeoCoder Settings');
                GeoCoderSetting.API_Key__c = '7910bf06a73d6003ca4c6dcc13db2422';
                GeoCoderSetting.End_Point_URL__c = 'https://usgeocoder.com/api/get_info.php';
                insert GeoCoderSetting;
                System.assertNotEquals( GeoCoderSetting.Id, null);
            }
            
            if( TriggerSettingContact == null )
            {
                TriggerSettingContact = new Triggers__c(Name = 'OnContactInsertUpdateProcessCongDist');
                TriggerSettingContact.Active__c = true;
                insert TriggerSettingContact;
                System.assertNotEquals(TriggerSettingContact.id,null);
            }
        }
        
        // Create Congressional District
        Congressional_District__c CD1 = new Congressional_District__c(Name='AL-02', Congressional_District__c='Congressional District 2',Party__c='Republican',Representative__c ='Test Rep',State__c='Colorado'); 
        insert CD1;
        System.assertNotEquals(CD1.id,null,'Congressional District not inserted');
        
        
         // Create Congressional District
        Congressional_District__c CD2 = new Congressional_District__c(Name='AL-02', Congressional_District__c='Congressional District 3',Party__c='Republican',Representative__c ='Test Rep',State__c='Colorado'); 
        insert CD2;
        System.assertNotEquals(CD2.id,null,'Congressional District not inserted');
         //create test data i.e. contact
        
        Contact Test_Con1 = new Contact(LastName='Test Contact1',OtherPostalCode='80443');
        insert Test_Con1;
        System.assertNotEquals(Test_Con1.id, null, 'Contact not inserted');
        lstContIds = new list<id>();
        lstContIds.add(Test_Con1.id);
        
        //create test data i.e. contact with Postalcode is not null And Street is not null      
        
        Contact Test_Con2 = new Contact(LastName='Test Contact',OtherPostalCode='80443',OtherStreet ='123 No Address St');
        insert Test_Con2;
        System.assertNotEquals(Test_Con2.id, null, 'Contact not inserted');
        lstContIds.add(Test_Con2.id);
        
         // call update trigger
        
        Test_Con2.USG_Trigger_Update__c = true;
        update Test_Con2;
        
        //create test data i.e. contact with Postalcode is null And Street is null      
        
        Contact Test_Con3 = new Contact(LastName='Test Contact');
        insert Test_Con3;
        System.assertNotEquals(Test_Con3.id, null, 'Contact not inserted');
        lstContIds.add(Test_Con3.id);
         
        CNTRL_ContactInsertUpdateAddCongDistrict.CNTRL_ContactInsertUpdateAddCongDistrict(lstContIds);
        Test.StopTest();
    }
    
    
    static testMethod void myUnitTestForAccountUpdate() 
    {
        
         
        //get the custom setting if the trigger is active or not
        Triggers__c  TriggerSettingAccount  = Triggers__c.getValues('OnAccountInsertUpdateProcessCongDist');
        Test.StartTest();
        System.runAs ( new User(Id = UserInfo.getUserId()) ) 
        {
            if( TriggerSettingAccount == null )
            {
                TriggerSettingAccount = new Triggers__c(Name = 'OnAccountInsertUpdateProcessCongDist');
                TriggerSettingAccount.Active__c = true;
                insert TriggerSettingAccount;
                System.assertNotEquals(TriggerSettingAccount.id,null);
            }  
        } 
        
        
        US_Geocoder_Settings__c  GeoCoderSetting  = US_Geocoder_Settings__c.getValues('GeoCoder Settings');
        System.runAs ( new User(Id = UserInfo.getUserId()) ) 
        {
            
            if( GeoCoderSetting == null )
            {
                GeoCoderSetting = new US_Geocoder_Settings__c( Name = 'GeoCoder Settings');
                GeoCoderSetting.API_Key__c = '7910bf06a73d6003ca4c6dcc13db2422';
                GeoCoderSetting.End_Point_URL__c = 'https://usgeocoder.com/api/get_info.php';
                insert GeoCoderSetting;
                System.assertNotEquals( GeoCoderSetting.Id, null);
            }
            
            if( TriggerSettingAccount == null )
            {
                TriggerSettingAccount = new Triggers__c(Name = 'OnAccountInsertUpdateProcessCongDist');
                TriggerSettingAccount.Active__c = true;
                insert TriggerSettingAccount;
                System.assertNotEquals(TriggerSettingAccount.id,null);
            }
        }
        
        list<account> lstAccount = new list<Account>([Select id,Name,ShippingCountry, ShippingPostalCode,ShippingStreet FROM Account LIMIT 5]);
        if( lstAccount != null && lstAccount.size() > 0 )
        {
            update lstAccount;        
        }
        
    }
    
    
    
    
     // test method for contact trigger
    
    static testMethod void myUnitTestForContactUpdate() 
    {
         Triggers__c  TriggerSettingContact  = Triggers__c.getValues('OnContactInsertUpdateProcessCongDist');
         
         Test.StartTest();
        if( TriggerSettingContact == null )
        {
            TriggerSettingContact = new Triggers__c(Name = 'OnContactInsertUpdateProcessCongDist');
            TriggerSettingContact.Active__c = true;
            insert TriggerSettingContact;
            System.assertNotEquals(TriggerSettingContact.id,null);
        }
        
       
        
        US_Geocoder_Settings__c  GeoCoderSetting  = US_Geocoder_Settings__c.getValues('GeoCoder Settings');
        System.runAs ( new User(Id = UserInfo.getUserId()) ) 
        {
            if( GeoCoderSetting == null )
            {
                GeoCoderSetting = new US_Geocoder_Settings__c( Name = 'GeoCoder Settings');
                GeoCoderSetting.API_Key__c = '7910bf06a73d6003ca4c6dcc13db2422';
                GeoCoderSetting.End_Point_URL__c = 'https://usgeocoder.com/api/get_info.php';
                insert GeoCoderSetting;
                System.assertNotEquals( GeoCoderSetting.Id, null);
            }
            
            if( TriggerSettingContact == null )
            {
                TriggerSettingContact = new Triggers__c(Name = 'OnContactInsertUpdateProcessCongDist');
                TriggerSettingContact.Active__c = true;
                insert TriggerSettingContact;
                System.assertNotEquals(TriggerSettingContact.id,null);
            }
        }
        
       list<contact> lstcontact = new list<contact>([Select id,Name,OtherCountry, OtherPostalCode,OtherStreet FROM Contact LIMIT 5]);
        if( lstcontact != null && lstcontact.size() > 0 )
        {
            update lstcontact;        
        }
     }
        
      
}