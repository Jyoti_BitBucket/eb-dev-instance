public class CNTRL_DedicationRequest
{
   
    public Dedication__c dedication {get;set;}
    public string orderNo { get; set; }
    public CNTRL_DedicationRequest()
    {
        dedication = new Dedication__c();
        
        // get the order number from the url which is encoded in base64 format
        orderNo = ApexPages.currentPage().getParameters().get('order');
        
        if( orderNo != null )
        {/*
            // take the base64 encoded parameter and create base64 decoded Blob from it
            Blob oredrNoblob = EncodingUtil.base64Decode(orderNo);
            
            // Convert the blob back to a string and print it in the debug log 
            orderNo = oredrNoblob.toString();*/
            dedication.Order_Number__c = orderNo;
        }
    }
    public void submit()
    {
        if( dedication != null )
        {
            try
            {
                insert dedication;
            }
            catch( System.Dmlexception e )
            {
                system.debug('Exception in dedication insert '+e.getMessage());
            }
        }
    }
   
}