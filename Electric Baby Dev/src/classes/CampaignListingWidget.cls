//This Apex Class is used for Showing CampaignListingWidget related to their Chapter
//This class is also used for showing IndividualCampaignSignUpWidget
public class CampaignListingWidget {

    public List<Campaign> listOfCampaignToShow{get;set;}
    
    //Url Parameter to control behaviour
    public ID idCampaignRecordType{get;set;}
    public ID idCampaignRecord{get;set;}
    public String strChapter{get;set;}
    public String  strRecordTypeId{get;set;}
    public Integer intMonthsToDisplay{get;set;}
    public String strCampaignType{get;set;}
    public map<String,List<Campaign>> mapOfCampaignToShow{get;set;}
    
    
    //UrlPramaters Individual
    public string strFirstName{get;set;}
    public string strLastName{get;set;}
    public string strEmail{get;set;}
    public List<Campaign> listOfIndividualCampaign{get;set;}
    public CampaignWrapperClass objCampaignWrapperClass{get;set;}

    public Date campaignStartDate{get;set;}

    //Constructor   
    public CampaignListingWidget()
    {
        //default 
        intMonthsToDisplay = 4;
        campaignStartDate = system.today();
        map<string,string> UrlParams = ApexPages.currentPage().getParameters();
        String strURLParameters;
        if(UrlParams.isEmpty() || UrlParams == null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'URL Parameters are required');
                ApexPages.addMessage(myMsg);
            }
        else{
            try{
            //idCampaignRecordType = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get(strRecordType).getRecordTypeId();
                strURLParameters = UrlParams.get('recordType');
                if(strURLParameters != null && strURLParameters != '')
                strRecordTypeId = strURLParameters;
                strURLParameters = UrlParams.get('monthstToDisplay');
                if(strURLParameters != null && strURLParameters != '')  
                intMonthsToDisplay = integer.valueof(strURLParameters);
                strURLParameters = UrlParams.get('type');
                if(strURLParameters != null && strURLParameters != '')
                strCampaignType = strURLParameters;
                strURLParameters = UrlParams.get('campaignid');
                if(strURLParameters != '' || strURLParameters != null)
                idCampaignRecord = strURLParameters;
                strURLParameters = UrlParams.get('chapter');
                if(strURLParameters != '' || strURLParameters != null)
                strChapter = strURLParameters;
                campaignStartDate = campaignStartDate.addMonths(intMonthsToDisplay);
                
                strURLParameters = UrlParams.get('fname');
                if(strURLParameters != '' || strURLParameters != null)
                strFirstName = strURLParameters;
                strURLParameters = UrlParams.get('lname');
                if(strURLParameters != '' || strURLParameters != null)
                strLastName = strURLParameters;
                strURLParameters = UrlParams.get('email');
                if(strURLParameters != '' || strURLParameters != null)
                strEmail = strURLParameters;
                //getting chapter picklist value
                List<String> listOFChapterPicklistValues = new List<String>();
                listOFChapterPicklistValues = CampaignListingWidget.getPicklistValues(new Campaign(),'Chapter__c');
                mapOfCampaignToShow = new map<String,List<Campaign>>();     
                Date TodayDate = System.today();
                
                String strCampaignQuery = 'select Id, Description, Chapter__c, Name, StartDate, Location_Name__c, Location_Street__c, Location_City__c, Open_Spaces__c, Time__c, Capacity__c, Registration_Deadline__c FROM Campaign';
                //if CampaignRecordTypeId , CampaignId and Chapter not null
                if(idCampaignRecord != null && strRecordTypeId != null && strChapter!= null) 
                    strCampaignQuery += ' where ID =: idCampaignRecord AND RecordTypeId =: strRecordTypeId AND StartDate <=: campaignStartDate AND Display_on_Web__c = true AND IsActive = true AND Chapter__c =: strChapter'; //AND Deadline__c >=: TodayDate 
                //If CampaignRecordTypeId and CampaignId is not null
                else if(idCampaignRecord != null && strRecordTypeId != null)
                    strCampaignQuery += ' where ID =: idCampaignRecord AND RecordTypeId =: strRecordTypeId AND StartDate <=: campaignStartDate AND Display_on_Web__c = true AND IsActive = true';  
                //If recordtypeid and Chapter not null
                else if(strRecordTypeId != null && strChapter != null)
                    strCampaignQuery += ' where RecordTypeId =: strRecordTypeId AND StartDate <=: campaignStartDate AND Display_on_Web__c = true AND IsActive = true AND Chapter__c =: strChapter';  
                //If Recordtypeid is not null
                else
                    strCampaignQuery += ' where RecordTypeId =: strRecordTypeId AND StartDate <=: campaignStartDate AND Display_on_Web__c = true AND IsActive = true';  
               //Campaign Type
                if(strCampaignType != null && idCampaignRecord != null)
                    strCampaignQuery += ' AND Type =: strCampaignType';
               //Query Selection For CampaignListingWidget and Individual
                if(idCampaignRecord != null && strRecordTypeId != null){
                   listOfCampaignToShow = Database.query(strCampaignQuery);
                system.debug('listOfCampaignToShow::'+listOfCampaignToShow);}
                else if(idCampaignRecord == null && strRecordTypeId != null) 
                   listOfCampaignToShow = Database.query(strCampaignQuery);
                else if(idCampaignRecord != null && strRecordTypeId == null)
                   listOfIndividualCampaign = [SELECT Id, Name, Chapter__c, Description, Time__c, StartDate, Location_Name__c, Location_Street__c, Location_City__c, Open_Spaces__c, Capacity__C, Registration_Deadline__c From Campaign Where Id =: idCampaignRecord];  
                else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Valid Parameters');
                    ApexPages.addMessage(myMsg);
                }
                
                //Showing Multiple Campaign On CampaignListingWidget
                if(listOfCampaignToShow != null && listOfCampaignToShow.size() > 0){
                    if(listOFChapterPicklistValues != null && listOFChapterPicklistValues.size() > 0){
                        for(String Chapter : listOFChapterPicklistValues){
                            for(Campaign objCampaign : listOfCampaignToShow){
                                List<Campaign> listOfChapterCampaign = new List<Campaign>();
                                if(objCampaign.Chapter__c != null && objCampaign.Chapter__c == Chapter && !mapOfCampaignToShow.containsKey(Chapter)){
                                    listOfChapterCampaign.add(objCampaign);
                                    mapOfCampaignToShow.put(Chapter,listOfChapterCampaign);
                                }
                                else if(objCampaign.Chapter__c == Chapter && mapOfCampaignToShow.containsKey(Chapter)){
                                    listOfChapterCampaign = mapOfCampaignToShow.get(Chapter);
                                    listOfChapterCampaign.add(objCampaign);
                                    mapOfCampaignToShow.put(Chapter,listOfChapterCampaign);
                                }
                            }
                        }
                    }
                }
                //Showing Individual Campaign
                else if(listOfIndividualCampaign != null && listOfIndividualCampaign.size() > 0){
                    String strChapter = listOfIndividualCampaign[0].Chapter__c;
                    String strDescription = listOfIndividualCampaign[0].Description;
                    String strTime = listOfIndividualCampaign[0].Time__c;
                    String strLocation = listOfIndividualCampaign[0].Location_Name__c;
                    DateTime d = listOfIndividualCampaign[0].StartDate;
                    String monthName= d.format('MMM');
                    String dateName = d.Format('dd');
                    String strMonthDate = monthName+' '+dateName;
                    String strLocationStreet = listOfIndividualCampaign[0].Location_Street__c;
                    String strLocationCity = listOfIndividualCampaign[0].Location_City__c;
                    Integer intOpenSpaces = listOfIndividualCampaign[0].Open_Spaces__c.intValue();
                    Integer intCapacity = listOfIndividualCampaign[0].Capacity__c.intValue();
                    DateTime Deadline = listOfIndividualCampaign[0].Registration_Deadline__c;
                    String strMonName = Deadline.format('MMM');
                    String strdate = Deadline.Format('dd');
                    String DeadlineDate= strMonName+' '+strdate;
                    objCampaignWrapperClass = new CampaignWrapperClass(idCampaignRecord,strFirstName,strLastName,strEmail,strChapter,strMonthDate,strTime,strDescription,strLocation,strLocationStreet,strLocationCity,intCapacity,intOpenSpaces,DeadlineDate);
                   
                }//When Both Lists are null Showing Information message
                else if((mapOfCampaignToShow == null || mapOfCampaignToShow.isEmpty()) && (listOfIndividualCampaign == null || listOfIndividualCampaign.size() <= 0)){
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'No Information Sessions Scheduled This Time');
                  ApexPages.addMessage(myMsg);
                }
                
                
            }
        
            catch(Exception ex){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'No Information Available');
                ApexPages.addMessage(myMsg);
            }
     }
       
        
    }
    //Getting Picklist Value
    public static List<String> getPicklistValues(SObject obj, String fld)
    {
      List<String> lstPickvals=new List<String>();
      // Get the object type of the SObject.
      Schema.sObjectType objType = obj.getSObjectType(); 
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
      // Get a map of fields for the SObject
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
      // Get the list of picklist values for this field.
      list<Schema.PicklistEntry> values =
         fieldMap.get(fld).getDescribe().getPickListValues();
      // Add these values to the  list.
      for (Schema.PicklistEntry a : values){ 
          lstPickvals.add(a.getValue());
      }
      return lstPickvals;
    }
    
    //Map of Campaign
   /* public map<String,List<Campaign>> GetMapOfCampaignToShow()
    {
        List<String> listOFChapterPicklistValues = new List<String>();
        listOFChapterPicklistValues = CampaignListingWidget.getPicklistValues(new Campaign(),'Chapter__c');
        mapOfCampaignToShow = new map<String,List<Campaign>>();     
        Date TodayDate = System.today();
        
        String strCampaignQuery = 'select Id, Description, Chapter__c, Name, StartDate, Location_Name__c, Location_Street__c, Location_City__c, Open_Spaces__c, Time__c, Capacity__c, Registration_Deadline__c FROM Campaign';
        
        if(idCampaignRecord != null && strRecordTypeId != null && strChapter!= null) //\'' + Filter + '\''
            strCampaignQuery += ' where ID =: idCampaignRecord AND RecordTypeId =: strRecordTypeId AND StartDate <=: campaignStartDate AND Display_on_Web__c = true AND IsActive = true AND Chapter__c =: strChapter'; //AND Deadline__c >=: TodayDate 
        else if(idCampaignRecord != null && strRecordTypeId != null)
            strCampaignQuery += ' where ID =: idCampaignRecord AND RecordTypeId =: strRecordTypeId AND StartDate <=: campaignStartDate AND Display_on_Web__c = true AND IsActive = true';  
        else if(strRecordTypeId != null && strChapter != null)
            strCampaignQuery += ' where RecordTypeId =: strRecordTypeId AND StartDate <=: campaignStartDate AND Display_on_Web__c = true AND IsActive = true AND Chapter__c =: strChapter';  
        else
            strCampaignQuery += ' where RecordTypeId =: strRecordTypeId AND StartDate <=: campaignStartDate AND Display_on_Web__c = true AND IsActive = true';  
        
        if(strCampaignType != null && idCampaignRecord != null)
            strCampaignQuery += ' AND Type =: strCampaignType';
          
        listOfCampaignToShow = Database.query(strCampaignQuery);
        if(listOfCampaignToShow != null && listOfCampaignToShow.size() > 0){
            if(listOFChapterPicklistValues != null && listOFChapterPicklistValues.size() > 0){
                for(String Chapter : listOFChapterPicklistValues){
                    for(Campaign objCampaign : listOfCampaignToShow){
                        List<Campaign> listOfChapterCampaign = new List<Campaign>();
                        if(objCampaign.Chapter__c != null && objCampaign.Chapter__c == Chapter && !mapOfCampaignToShow.containsKey(Chapter)){
                            listOfChapterCampaign.add(objCampaign);
                            mapOfCampaignToShow.put(Chapter,listOfChapterCampaign);
                        }
                        else if(objCampaign.Chapter__c == Chapter && mapOfCampaignToShow.containsKey(Chapter)){
                            listOfChapterCampaign = mapOfCampaignToShow.get(Chapter);
                            listOfChapterCampaign.add(objCampaign);
                            mapOfCampaignToShow.put(Chapter,listOfChapterCampaign);
                        }
                    }
                }
            }
        }
        if(mapOfCampaignToShow == null || mapOfCampaignToShow.isEmpty()){
          ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Information Sessions Scheduled This Time');
          ApexPages.addMessage(myMsg);
        }
        return mapOfCampaignToShow;
        
    }*/
   //Wrapper class for IndividualCampaign Processing 
  public class CampaignWrapperClass{
        public Id CampaignId{get;set;}
        public String strFirstName{get;set;}
        public String strLastName{get;set;}
        public String strEmail{get;set;}
        public String strChapter{get;set;}
        public String strMonthDate{get;set;}
        public String strTime{get;set;}
        public String strDescription{get;set;}
        public String strLocation{get;set;}
        public String strLocationStreet{get;set;}
        public String strLocationCity{get;set;}
        public Integer intCapacity{get;set;}
        public Integer intOpenSpaces{get;set;}
        public String DeadineDate{get;set;}
        public CampaignWrapperClass(Id CampaignId, String strFirstName, String strLastName, String strEmail,  String strChapter, String strMonthDate, String strTime, String strDescription, String strLocation, String strLocationStreet, String strLocationCity, Integer intCapacity, Integer intOpenSpaces, String DeadineDate )
        {
            this.CampaignId = CampaignId;
            this.strFirstName = strFirstName;
            this.strLastName = strLastName;
            this.strEmail = strEmail;
            this.strChapter = strChapter;
            this.strMonthDate = strMonthDate;
            this.strTime = strTime;
            this.strDescription = strDescription;
            this.strLocation = strLocation;
            this.strLocationStreet = strLocationStreet;
            this.strLocationCity = strLocationCity;
            this.intCapacity = intCapacity;
            this.intOpenSpaces = intOpenSpaces;
            this.DeadineDate = DeadineDate ;
        }
    }
}