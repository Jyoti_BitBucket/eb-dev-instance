public class UpdateContact{
    public String conId{ get; set; }
    public Contact UpdateContact{get;set;}
    public static string updateId{get;set;}
    public UpdateContact() {
        conId = ApexPages.currentPage().getParameters().get('conId');
        if (conId != null && conId != '') {
            system.debug('::contactId::'+conId);
        }       
        UpdateContact = [Select FirstName From Contact Where Id =: conId];
        updateId = conId; 
        system.debug('::updateId ::'+updateId );     
    }
    @RemoteAction
    public static string save(String firstName){
        Contact UpdateContact = new Contact();
        UpdateContact.FirstName  = firstName;
        UpdateContact.id = updateId;
        List<Contact> lstcon = new List<Contact>();
        lstcon.add(UpdateContact);
        system.debug(':::lstCon:'+lstcon);
        MyRestResourcedemo.RequestWrapper reqst = new MyRestResourcedemo.RequestWrapper();
        reqst.con = lstcon;
        String JsonMsg = JSON.serialize(reqst);
        //As Per Best Practice it is important to instantiate the Rest Context
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        MyRestResourcedemo.ResponseWrapper resp = new MyRestResourcedemo.ResponseWrapper();
        resp = MyRestResourcedemo.doPost(reqst); 
        system.debug('::firstName::'+firstName);
      
        return 'Update Successfully';
    }
}