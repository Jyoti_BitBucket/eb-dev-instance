public with sharing class Cntrl_AngularOnlineDonation {
  // C&P Getters and Setters
  public CnP_IaaS.PaaS_Class cnpxmlstring{get;set;}
  public CnP_IaaS.PaaS_Class.Operation Operation{get;set;}
  public CnP_IaaS.PaaS_Class.Order Order{get;set;}
  public CnP_IaaS.PaaS_Class.Authentication Authentication{get;set;}
  public CnP_IaaS.PaaS_Class.BillingInformation BillingInfo{get;set;}
  public CnP_IaaS.PaaS_Class.BillingAddress BillingAddress{get;set;}
  public CnP_IaaS.PaaS_Class.PaymentMethod PaymentMethod{get;set;}
  public CnP_IaaS.PaaS_Class.Receipt Receipt{get;set;}
  public CnP_IaaS.PaaS_Class.cnpTransaction cnpTransaction{get;set;}
  private CnP_PaaS__CnP_API_Settings__c[] SettingsList { get; set; }
  public List<CnP_IaaS.PaaS_Class.Items> Itemlist{get;set;}
  public CnP_IaaS.PaaS_Class.Items item { get; set; }     
  public Boolean settingsError{set;get;}
  
  // Fields from Donation Form Custom Settings
  public Donation_Form_Settings__c formSettings {get;set;}
  public String CnPAccountGUID { get; set; } // To show support email 
  public String CnPAccountNumber { get; set; } // To show support email  
  public String orderMode { get; set; } // To show hide sections in VF page if "Test" mode
  public String supportEmail { get; set; } // To show support email 
  public String thankYouPage { get; set; } // For redirect upon succesful transaction
  public String GivingDonation {get;set;} // this is referred in communities donation page
  public boolean DefaultStateFromSetting { get;set; } // used in vf page for showing correct input field for state
  public boolean DefaultCountryFromSetting { get;set;}//used in vf page for showing correct country
  // Designation
  String DesignationType = '';  
  String DesignationDefault;
  public void setDesignationType(String S){this.DesignationType = S;}  // We aren't setting but still required by select list in VF
  public String getDesignationType(){return DesignationType;}
  
  // Dedication
  public String DedicationType = 'No Dedication'; 
  public void setDedicationType(String S){this.DedicationType = S;} // We aren't setting but still required by select list in VF
  public String getDedicationType(){return DedicationType;}
  public String DedicatedToName{ get;set; } 
  
  // Recognition
  public String RecognitionName{ get;set; } //Grab text question value
  public String RecognitionAnonymous{get;set;} //Grab text question value
  
  // Lead Source
  public String LeadSource{ get; set; }
  
  // Newsletter
  public boolean chkNewsLetter { get; set;} // Check box for the general donation form
  public String Newsletter { get; set; } // String because we can only pass text
   
  // Extra Select List 
  public String ExtraSelect{ get; set; } 
  
  // Extra Text Input
  public String ExtraText{ get; set; }   
  
  // Extra Hidden Field
  public String ExtraHidden { get; set; }
      
  // SKU
  public String CustomSKU { get; set; }  // SKU passed from VisualForce page
  public String URLSKU { get; set; }  // SKU passed from URL
   
  // Campaign
  public String CustomCampaign { get; set; }  // Campaign passed from VisualForce page
        
  // Amount
  public String URLAmount { get; set; }  // Amount passed from URL

  // Custom String to be displayed in the VF page Optional
  public String CustomString { get; set; } 

  // Fields for C&P Results  
  public String transactionCode { get; set; }
  public String transactionResult { get; set; }
  public String transactionNumber { get; set; }   
      
  // For checking if USA or Canada holds the country code
  public String country {get; set;}
  public String state {get;set;}
  
 // For checking if USA or Canada holds the country lable in string format
  public String countryLabel {get; set;}
  public String stateLabel {get;set;}
     
  // customSKU and extraHidden fields passed in from VisualForce Page 
  public PageReference HiddenParams() {
    CustomSKU = Apexpages.currentPage().getParameters().get('customSKU');
    ExtraHidden = Apexpages.currentPage().getParameters().get('extraHidden');
    return null;
  }
  //For checking dedication ackowledgment  
  public boolean DedicationRequest{get;set;}
   
    
  /*********** Constructor Starts **************/
    
  public Cntrl_AngularOnlineDonation(){
    DefaultStateFromSetting = true;
    DefaultCountryFromSetting = true;
    countryLabel = null;
    stateLabel = null;
    country = '000';
    DedicationRequest = false;
    // Instantiate C&P objects
    Operation=new CnP_IaaS.PaaS_Class.Operation(); 
    Order=new CnP_IaaS.PaaS_Class.Order();  
    Authentication=new CnP_IaaS.PaaS_Class.Authentication();
    BillingInfo=new CnP_IaaS.PaaS_Class.BillingInformation();
    BillingAddress=new CnP_IaaS.PaaS_Class.BillingAddress();
    PaymentMethod=new CnP_IaaS.PaaS_Class.PaymentMethod();
    Receipt=new CnP_IaaS.PaaS_Class.Receipt();
    Receipt.SendReceipt = false; // Do not send C&P default receipt
    cnpxmlstring = new CnP_IaaS.PaaS_Class();
    system.debug('cnpxmlstring::-->'+cnpxmlstring.getExpireMonth());
    CnPTransaction=new CnP_IaaS.PaaS_Class.cnpTransaction();
    Itemlist=new list<CnP_IaaS.PaaS_Class.Items>();
    item = new CnP_IaaS.PaaS_Class.Items();
 
    // Using only one item in this transaction. Set non-price details
    item.SKU = 'NONE'; // Default Sku 
    item.ItemName = 'Donation'; // Can get renamed in C&P settings
    ItemList.add(item);
    
    // Set Default DedicationType (not back to VF)
    DedicationType = 'No Dedication';

    // Set Default Newletter sign up to True
    chkNewsLetter = True;
        
    // Get Custom Settings
    orderMode = 'Test'; // Default to Test Mode if not specified
    formSettings = Donation_Form_Settings__c.getInstance();       // This is used in javascript processing() function
    settingsError = false;
    if(formSettings == null || formSettings.Default_Amount__c == null || formSettings.Thank_You_Page__c == null || formSettings.Support_Email__c == null || formSettings.CnP_Account_GUID__c == null || formSettings.CnP_Account_Number__c == null ) {
      settingsError = true;
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.warning,'Please enter C&P Account details in Setup > Develop > Custom Settings > Donation Form Settings');  
      ApexPages.addMessage(myMsg);
    }
    
    // check if default state and country is null
    if( settingsError == false && (formSettings.Default_State__c != null || formSettings.Default_Country__c != null) )
    {
       if( formSettings.Default_State__c != null )
           stateLabel =  formSettings.Default_State__c;
           
       if( formSettings.Default_Country__c != null )    
        countryLabel = formSettings.Default_Country__c;
    }
    
    thankYouPage = formSettings.Thank_You_Page__c; // Assign page for redirect
    supportEmail = formSettings.Support_Email__c; // Assign email for support and for failed transactions
    Order.OrderMode = (formSettings.Order_Mode__c != null) ? formSettings.Order_Mode__c : 'Test';   
    orderMode=Order.OrderMode; // for VF page flag
    CnPAccountGUID = formSettings.CnP_Account_GUID__c; // Set C&P Guid 
    CnPAccountNumber = formSettings.CnP_Account_Number__c; // Set C&P Account Number
    if( formSettings != null && formSettings.Default_Amount__c != null )
        item.UnitPrice = (formSettings.Default_Amount__c).setScale(2);  // Assign default Amount
    else
        item.UnitPrice = 25.00;
    BillingAddress.BillingStateProvince = formSettings.Default_State__c;  // Assign Default State
    state = formSettings.Default_State__c;
    if( formSettings.Default_State__c != null )
    if( formSettings.Default_State__c == 'None')
        DefaultStateFromSetting = false;  
     
    if( cnpxmlstring.getcountryCodes() != null && cnpxmlstring.getcountryCodes().size() > 0 )
    {
        for(SelectOption  option : cnpxmlstring.getcountryCodes())
        {  
            if(formSettings.Default_Country__c == option.getLabel() )
            {
                 BillingAddress.BillingCountryCode = option.getValue();
            }
        }
    }
    if(cnpxmlstring.getUsStates() != null && cnpxmlstring.getUsStates().size() > 0)
    {
        for(SelectOption option : cnpxmlstring.getUsStates())
        {
           if( formSettings.Default_State__c == option.getLabel() )
           {
               BillingAddress.BillingStateProvince = option.getValue();
           }
        }
    }
    
    if(formSettings.Default_Country__c == 'United States' || formSettings.Default_Country__c == 'US' || formSettings.Default_Country__c == 'USA'){ BillingAddress.BillingCountryCode = '840';country = '840'; }// Assign Default Country
  
    if( formSettings.Default_Country__c == null || formSettings.Default_Country__c == null )
        DefaultCountryFromSetting = false;
    
    CnPTransaction.TransactionType = 'Payment'; // Important for donations to get charged and not just authorized
    CnPTransaction.Periodicity = ''; //Set default recurring periodicity as OneTime
    DesignationType='Undesignated';
    
    // Changing the Format for date
    String dd=datetime.now().format('MM/dd/yyyy');
    String mon=dd.substring(0,2);
    String year=dd.substring(7,10);
    PaymentMethod.ExpirationMonth=mon;
    PaymentMethod.ExpirationYear=year;
    
    // Add params from URL
    Map<String, String> params = ApexPages.currentPage().getParameters();
    String paramsReceived = 'Received:';
    
    String a = params.get('cString'); // Retreive Custom Output from URL
    if (a != null && a != '') {
      CustomString = a;
      //paramsReceived += 'CustomString=' + CustomString + ', '; 
    } 
   
    String b = params.get('firstName'); // Retreive First Name from URL
    if (b != null && b != '') {
      BillingInfo.BillingFirstName = b;
      //paramsReceived += 'FirstName=' + BillingInfo.BillingFirstName + ', ';
    } 
  
    String c = params.get('lastName'); // Retreive Last Name from URL
    if (c != null && c != '') {
      BillingInfo.BillingLastName = c;
      //paramsReceived += 'LastName=' + BillingInfo.BillingLastName + ', ';
    } 
  
    String d = params.get('email'); // Retreive Email from URL
    if (d != null && d != '') {
      BillingInfo.BillingEmail = d;
      //paramsReceived += 'Email=' + BillingInfo.BillingEmail + ', ';
    } 
  
    String e = params.get('phone'); // Retreive Phone from URL
    if (e != null && e != '') {
      BillingInfo.BillingPhone = e;
      //paramsReceived += 'Phone=' + BillingInfo.BillingEmail + ', ';
    } 
    
    String f = params.get('address'); // Retreive Address from URL
    if (f != null && f != '') {
      BillingAddress.BillingAddress1 = f;
      //paramsReceived += 'Address=' + BillingInfo.BillingEmail + ', ';
    }
     
    String g = params.get('city'); // Retreive Address from URL
    if (g != null && g != '') {
      BillingAddress.BillingCity = g;
      //paramsReceived += 'City=' + BillingInfo.BillingEmail + ', ';
    } 
        
    String h = params.get('zip'); // Retreive Zip Code from URL
    if (h != null && h != '') {
      BillingAddress.BillingPostalCode = h;
      //paramsReceived += 'ZipCode=' + BillingAddress.BillingPostalCode + ', ';
    } 
  
    String i = params.get('state'); // Retreive Address from URL
    if (i != null && i != '') {
      BillingAddress.BillingStateProvince = i;
      system.debug('State::'+BillingAddress.BillingStateProvince);
      //paramsReceived += 'State=' + BillingInfo.BillingEmail + ', ';
    } 
    
    String j = params.get('country'); // Retreive Address from URL
    if (j != null && j != '') {
        if(j == 'United States' || j == 'US' || j == 'USA') BillingAddress.BillingCountryCode = '840';
        if(j == 'Canada') BillingAddress.BillingCountryCode = '124';
      //paramsReceived += 'Country=' + BillingInfo.BillingEmail + ', ';
    }       
  
    String k = params.get('amount'); // Retreive Amount from URL
    if (k != null && k != '') {
        URLAmount = k;
        item.unitprice = Decimal.valueOf(k);
        //paramsReceived += 'Amount=' + URLAmount + ', ';
    }    
    
    String l = params.get('sku'); // Retreive SKU from URL
    if (l != null && l != '') {
        URLSKU = l;
        //paramsReceived += 'SKU=' + URLSKU + ', ';
    }    
    //system.debug(paramsReceived);
  
  }
/************* Constructor ends ***********/
    
   

    //new method for return all initial value
    @RemoteAction
    public static WrapperOnlineDonation getInitialValues()
    {
            WrapperOnlineDonation newWrapperOnlineDonation = new WrapperOnlineDonation();
            
            newWrapperOnlineDonation.DedicationRequest = false;
            // Instantiate C&P objects
            newWrapperOnlineDonation.BillingInfo = new CnP_IaaS.PaaS_Class.BillingInformation();
            newWrapperOnlineDonation.BillingAddress = new CnP_IaaS.PaaS_Class.BillingAddress();
            newWrapperOnlineDonation.PaymentMethod = new CnP_IaaS.PaaS_Class.PaymentMethod();
            //Receipt=new CnP_IaaS.PaaS_Class.Receipt();
            //Receipt.SendReceipt = false; // Do not send C&P default receipt
            newWrapperOnlineDonation.cnpxmlstring = new CnP_IaaS.PaaS_Class();
            //CnPTransaction=new CnP_IaaS.PaaS_Class.cnpTransaction();
            list<CnP_IaaS.PaaS_Class.Items> Itemlist=new list<CnP_IaaS.PaaS_Class.Items>();
            newWrapperOnlineDonation.item = new CnP_IaaS.PaaS_Class.Items();
         
            // Using only one item in this transaction. Set non-price details
            newWrapperOnlineDonation.item.SKU = 'NONE'; // Default Sku 
            newWrapperOnlineDonation.item.ItemName = 'Donation'; // Can get renamed in C&P settings
            newWrapperOnlineDonation.item.UnitPrice = 100;
            ItemList.add(newWrapperOnlineDonation.item);
            
            // Set Default DedicationType (not back to VF)
            newWrapperOnlineDonation.DedicationType = 'No Dedication';
        
            // Set Default Newletter sign up to True
            newWrapperOnlineDonation.chkNewsLetter = True;
             // Changing the Format for date
            String todaysDate = datetime.now().format('MM/dd/yyyy');
            String month = todaysDate.substring(0,2);
            String year = todaysDate.substring(8,10);
            newWrapperOnlineDonation.PaymentMethod.ExpirationMonth = month;
            newWrapperOnlineDonation.PaymentMethod.ExpirationYear = year;
            newWrapperOnlineDonation.ExpireMonth = newWrapperOnlineDonation.cnpxmlstring.getExpireMonth();
            newWrapperOnlineDonation.ExpireYear = newWrapperOnlineDonation.cnpxmlstring.getExpireYear();
           
            return newWrapperOnlineDonation;   
          
    
    }
    
    
    public class WrapperOnlineDonation
    {
        public CnP_IaaS.PaaS_Class.Items item { get; set; }
         // Designation
        String DesignationType = '';  
        String DesignationDefault;
        public void setDesignationType(String S){this.DesignationType = S;}
        public String getDesignationType(){return DesignationType;}
          // Dedication
        public String DedicationType = 'No Dedication'; 
        public void setDedicationType(String S){this.DedicationType = S;}
        public String getDedicationType(){return DedicationType;}
        public String DedicatedToName{ get;set; }
        public CnP_IaaS.PaaS_Class.PaymentMethod PaymentMethod{get;set;}
        public CnP_IaaS.PaaS_Class cnpxmlstring{get;set;}
        public List<SelectOption> ExpireMonth{get;set;}
        public List<SelectOption> ExpireYear{get;set;}
        public CnP_IaaS.PaaS_Class.BillingInformation BillingInfo{get;set;}
        public CnP_IaaS.PaaS_Class.BillingAddress BillingAddress{get;set;}
        // Lead Source
        public String LeadSource{ get; set; }
        // Extra Select List 
        public String ExtraSelect{ get; set; }
         // Extra Text Input
        public String ExtraText{ get; set; }
        // Newsletter
        public boolean chkNewsLetter { get; set;} // Check box for the general donation form
        public String Newsletter { get; set; } // String because we can only pass text
         //For checking dedication ackowledgment  
        public boolean DedicationRequest{get;set;}
        // Fields from Donation Form Custom Settings
        public Donation_Form_Settings__c formSettings {get;set;}
        
    }
}