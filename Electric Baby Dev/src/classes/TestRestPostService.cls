@istest

public class TestRestPostService {
    static testMethod void  testPostRestService(){
        List<contact> lstcon=new List<contact>();
        
        for(integer  i=0;i<=10;i++){
            Contact c=new Contact();
            c.lastname='Test+i';
            insert c;
            lstcon.add(c);
        }
        
        MyRestResourcedemo.RequestWrapper reqst = new MyRestResourcedemo.RequestWrapper();
        reqst.con=lstcon;
        String JsonMsg = JSON.serialize(reqst);
        Test.startTest();
            //As Per Best Practice it is important to instantiate the Rest Context
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.httpMethod = 'POST';//HTTP Request Type
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;
            MyRestResourcedemo.ResponseWrapper resp = new MyRestResourcedemo.ResponseWrapper();
            resp = MyRestResourcedemo.doPost(reqst); //Call the Method of the Class with Proper       Constructor
            System.assert(resp.statusMessage.contains('Test success message'));//Assert the response has message as expected
            System.assert(resp.statusCode.contains('Done'));
        Test.stopTest();
        
    }
}