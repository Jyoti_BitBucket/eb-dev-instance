@isTest
public class EventAttendControllerTest 
{
 static testmethod void TestEventAttendence() 
 {
     // Custom setting values
	insert new Volunteer_Roll_Call_Settings__c(Refresh_Interval__c = 2,Hours_Worked_Time_Interval__c = 2);
    // Create new Affiliates	
	    list<Contact> lstObjContact = new List<Contact>();
        Contact c= new Contact(FirstName='MyTest',LastName='Platoon');
        lstObjContact.add(c);
     insert lstObjContact;
		//system.assertNotEquals(lstObjAffiliate,null,'Affiliate list is null');
		//system.assertEquals(lstObjAffiliate.isEmpty(),false);
		
		
		// Inactivate one of these affiliates to use for testing.
		lstObjContact[0].Active__c = false;
		lstObjContact[0].Status__c = null;
		database.update(lstObjContact[0]);
		//update lstObjContact[0];
     
     lstObjContact = [SELECT id,Email FROM Contact WHERE id IN :lstObjContact];
     
     // Create new Registration
		list<Contact> lstContact = new list<Contact>();
     lstContact = [select id,Email from Contact where id = :lstObjContact[0].id];
		//list<Registration__c> lstObjRegistration = TMC_TestDataGenerator.createTestOccasionWithRegistrations(lstAffiliate[0].Service_Platoon__c,lstAffiliate);
	  Campaign objCampaign =new Campaign(Name='TestCampaign');
       insert objCampaign;
     
     GW_Volunteers__Volunteer_Job__c objJob = new GW_Volunteers__Volunteer_Job__c(Name='Test Job',GW_Volunteers__Campaign__c=objCampaign.Id);  
        insert objJob;
     
     GW_Volunteers__Volunteer_Shift__c objShift =new GW_Volunteers__Volunteer_Shift__c(GW_Volunteers__Volunteer_Job__c=objJob.Id,
                                                GW_Volunteers__Start_Date_Time__c=Date.today(),GW_Volunteers__Duration__c=4);
        insert objShift;
     
   List<GW_Volunteers__Volunteer_Hours__c> lstObjRegistration =new List<GW_Volunteers__Volunteer_Hours__c>();
     GW_Volunteers__Volunteer_Hours__c objRegistration =new GW_Volunteers__Volunteer_Hours__c(GW_Volunteers__Contact__c =lstObjContact[0].id,
                                                        GW_Volunteers__Volunteer_Job__c=objJob.Id,GW_Volunteers__Status__c='Confirmed',
                                                        GW_Volunteers__Number_of_Volunteers__c=2,GW_Volunteers__Start_Date__c=Date.today(),
                                                       GW_Volunteers__Volunteer_Shift__c=objShift.Id);
    lstObjRegistration.add(objRegistration);
      insert lstObjRegistration;
     //system.assertNotEquals(lstObjRegistration,null,'Registration list is null');
		//system.assertEquals(lstObjRegistration.isEmpty(),false);
		//system.debug('lstObjRegistration ::'+lstObjRegistration[0].Affiliate_Number__c );

     list<GW_Volunteers__Volunteer_Shift__c> lstShift = new list<GW_Volunteers__Volunteer_Shift__c>([Select ID,GW_Volunteers__Volunteer_Job__c From GW_Volunteers__Volunteer_Shift__c
                                                       Where GW_Volunteers__Volunteer_Job__c= :objJob.Id]);
      
		//system.assertNotEquals(lstOccasion,null,'Occasion list is null');
		//system.assertEquals(lstOccasion.isEmpty(),false);
     Profile testProfile = [SELECT Id FROM profile WHERE Name = 'System Administrator'LIMIT 1];
      User testUser = new User(LastName = 'test user 1',Username = 'test.user.1@example.com',Email = 'test.1@example.com', 
                             Alias = 'testu1',TimeZoneSidKey = 'GMT',LocaleSidKey = 'en_GB',EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = testProfile.Id,LanguageLocaleKey = 'en_US');
        GW_Volunteers__Volunteer_Shift__c shift = new GW_Volunteers__Volunteer_Shift__c();
		shift = lstShift[0];
     

        Test.startTest();
        System.runas(testUser)
        {
            // Pass EventId as parameter
			ApexPages.StandardController stdOccasion = new ApexPages.StandardController(shift);
			ApexPages.currentPage().getParameters().put('eventId',objRegistration.GW_Volunteers__Volunteer_Shift__c);
			EventAttendController objEventAttendanceController = new EventAttendController(stdOccasion );
			objEventAttendanceController.init();
            
          //  objEventAttendanceController.setAffiliatesList();
			//system.assertEquals(objEventAttendanceController.strEventId , lstObjRegistration[0].Occasion__c);
			//system.assertNotEquals(objEventAttendanceController.lstRegisterdAffiliates,null);
			
			
           //test UpdateAffiliate method with lstObjAffiliate i.e Occasion attended = false
			objEventAttendanceController.affiliateIdToUpdate = lstObjContact[0].id;
			//objEventAttendanceController.UpdateAffiliate();
			lstObjRegistration[0].In_Time__c = system.now().addMinutes(-1);
			update lstObjRegistration[0];
			objEventAttendanceController.UpdateVolunteer();
            
            
			list<GW_Volunteers__Volunteer_Hours__c> lstRegistrations = new list<GW_Volunteers__Volunteer_Hours__c>();
			lstRegistrations = [SELECT id,In_Time__c,Attended__c FROM GW_Volunteers__Volunteer_Hours__c WHERE id =: lstObjRegistration[0].id LIMIT 1];

		/*	system.assertNotEquals( lstRegistrations,null,'Registration list is null' );
			system.assertNotEquals( lstRegistrations.isEmpty(),true,'Registration list is not present' );
			system.assertEquals(lstRegistrations[0].Attended__c,true);*/
            
            // for test UpdateAffiliate method with lstObjAffiliate i.e Occasion attended = true
			lstObjRegistration[0].In_Time__c = system.now().addMinutes(-20);
			lstObjRegistration[0].Attended__c = true;
			lstObjRegistration[0].GW_Volunteers__Hours_Worked__c = 1;
            
            
			Double L_affiliateInTime = (lstObjRegistration[0].In_Time__c).getTime();
			Double milliseconds = (DateTime.now().getTime()) - L_affiliateInTime;
			Double hours = (milliseconds / 1000)/(60*60);
            
            objEventAttendanceController.affiliateIdToUpdate = lstObjContact[0].id;
			update lstObjRegistration[0];
			objEventAttendanceController.UpdateVolunteer();
            
            lstRegistrations = [SELECT id,In_Time__c,Attended__c,GW_Volunteers__Hours_Worked__c FROM
                              GW_Volunteers__Volunteer_Hours__c WHERE id =: lstObjRegistration[0].id LIMIT 1];
			/*system.debug('lstRegistrations::'+lstRegistrations[0]);
			system.assertEquals(lstRegistrations[0].In_Time__c,null);*/
            
            Double Hours_Contributed =(lstObjRegistration[0].GW_Volunteers__Hours_Worked__c = lstObjRegistration[0].GW_Volunteers__Hours_Worked__c + hours).setscale(3);
			//system.assertEquals(true,(testRegistration[0].Hours_Contributed__c).setscale(3) == Hours_Contributed);
			/*system.debug('Hours_Contributed :::' + Hours_Contributed + '::testRegistration[0].Hours_Contributed__c::'+(lstRegistrations[0].Hours_Contributed__c).setscale(3));
			system.assertEquals(false,lstObjRegistration[0].Attended__c == false);*/

            // code is for test SearchAffiliate() method i.e register new affiliate
			//objEventAttendanceController.strEventId = lstObjRegistration[0].GW_Volunteers__Volunteer_Shift__c;
			//system.debug('Affiliates queried :: '+[SELECT id,Email__C FROM Affiliates__c WHERE id = :lstObjAffiliate[1].id]);
			objEventAttendanceController.strEmailId = lstObjContact[1].Email;
			//system.debug(' lstObjAffiliate 1 ==> '+lstObjContact);
			objEventAttendanceController.SearchAffiliate();
            
            lstRegistrations = [SELECT id,In_Time__c,Attended__c,GW_Volunteers__Number_of_Volunteers__c FROM GW_Volunteers__Volunteer_Hours__c WHERE id = :objEventAttendanceController.affiliateIdToUpdate LIMIT 1];
			//system.assertNotEquals(lstRegistrations[0],null);

			//test for already registered affiliate
			objEventAttendanceController.strEmailId = lstObjContact[0].Email;
			system.debug('objEventAttendanceController.strEmailId::::'+objEventAttendanceController.strEmailId);
			objEventAttendanceController.SearchAffiliate();
			//system.assertEquals(false,objEventAttendanceController.b_searchFlag);


             
			//Test when email id incorrect or not exist
			objEventAttendanceController.strEmailId = '';
			objEventAttendanceController.SearchAffiliate();

			shift.GW_Volunteers__Start_Date_Time__c = Date.Today();
			update shift;

			system.assertequals(Date.Today(),shift.GW_Volunteers__Start_Date_Time__c);
			stdOccasion = new ApexPages.StandardController(shift);
			EventAttendController objEventAttendanceController1 = new EventAttendController(stdOccasion);
			shift.GW_Volunteers__Start_Date_Time__c = Date.Today()-2;
			update shift;
            
           // system.assertnotequals(Date.Today(),oc.Occasion_Date__c);
			/*stdOccasion = new ApexPages.StandardController(shift);
			EventAttendController objEventAttendanceController2 = new EventAttendController(stdOccasion);
			shift.Status__c = 'Closed';
			update shift;

			system.assertNotEquals(shift.Status__c,'Open');
			stdOccasion = new ApexPages.StandardController(shift);
		    EventAttendController objEventAttendanceController3 = new EventAttendController(stdOccasion);*/
            Test.stopTest();

        }
 }
 
}