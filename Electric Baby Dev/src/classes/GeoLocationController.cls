public with sharing class GeoLocationController
{
   @RemoteAction
   public static List<Account> getNearByAccounts(Double Latitude,Double Longitude,Integer distance)
   {
        List<Account> lstAcc = new List<Account>();
        lstAcc = Database.query('select Name,BillingPostalCode,BillingStreet,BillingCity,BillingState,BillingCountry from Account where DISTANCE(Location__c, GEOLOCATION('+latitude+','+longitude+'),\'mi\')< '+distance+' ORDER BY Name');
        return lstAcc;
   }
   @RemoteAction
   public static void saveAccounts(Map<String,String> mapAccount)
   {
        Id accountId = mapAccount.get('Id');
        List<Account> lstAccount = new List<Account>();
        lstAccount = [Select Name,BillingPostalCode,BillingStreet,BillingCity,BillingState,BillingCountry from Account where Id =: accountId];
        lstAccount[0].Name = mapAccount.get('Name');
        lstAccount[0].BillingStreet = mapAccount.get('BillingStreet');
        lstAccount[0].BillingState = mapAccount.get('BillingState');
        lstAccount[0].BillingCountry = mapAccount.get('BillingCountry');
        lstAccount[0].BillingPostalCode = mapAccount.get('BillingPostalCode');
        update(lstAccount);
   }
   @RemoteAction
   public static void deleteAccounts(Id accountId)
   {
        
        List<Account> lstAccount = new List<Account>();
        lstAccount = [Select Name,BillingPostalCode,BillingStreet,BillingCity,BillingState,BillingCountry from Account where Id =: accountId];
        delete(lstAccount);
   }
   @RemoteAction
   public static string demoMethod(){
     return 'Success';
   }
}