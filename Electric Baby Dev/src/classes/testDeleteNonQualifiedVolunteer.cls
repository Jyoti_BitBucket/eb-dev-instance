/*
 * Summary    : Test method for delete nonqualifiedvolunteer Trigger also allocating the floating shifts
 * 
 *
 */

@isTest(SeeAllData = True)
public Class testDeleteNonQualifiedVolunteer 
{
    public static testMethod void testNonQualifiedVolunteer()
    {
          Triggers__c  TriggerSettingshift  = Triggers__c.getValues('OnNewVolunteerShift');
          Triggers__c  TriggerSetting  = Triggers__c.getValues('DeleteNonQualifiedVolunteer');
        
      System.runAs ( new User(Id = UserInfo.getUserId()) ) 
      {
         
        ////custom setting for delete non qualified volunteer  
        
        if( TriggerSetting == null )
        {
             TriggerSetting = new Triggers__c( Name = 'DeleteNonQualifiedVolunteer');
             insert TriggerSetting;
             System.assertNotEquals( TriggerSetting.Id, null);
        }
        else
        {
          if( TriggerSetting.Active__c != true )
          {
            TriggerSetting.Active__c = true;
            update TriggerSetting;
          }
        }
      }
        List<GW_Volunteers__Volunteer_Hours__c> lstVolHrs;
        
       // HOMEtracker__Service_File__c NewServiceFile = HelpertestDeleteNonQualifiedVolunteer.CreateServiceFile('Test Service File 2013','In Progress');
      //  insert(NewServiceFile);
      //  System.AssertNotEquals(NewServiceFile.Id,Null);
        
        
        
        Contact NewContact = HelpertestDeleteNonQualifiedVolunteer.CreateContact('TestLastName','10/07/2013',false,'Homeowner');
        Date myDateValue = Date.Today();
    myDateValue = myDateValue.addYears(-15);
      //NewContact.hh_HomeKeeper_Role__c = 'Dependent';
      NewContact.Birthdate = myDateValue;
        insert NewContact;
        System.AssertNotEquals(NewContact.Id,Null);
        
        List<Contact> lstCont = new List<Contact>([SELECT id,Name FROM  Contact where id = : NewContact.Id ]);
        System.debug('lstOf Contact is '+lstCont);
        Volunteer_Registration__c VolRegistration = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerRegistration('TestWaiver Name',NewContact.Id);
        insert VolRegistration;
        System.AssertNotEquals(VolRegistration.Id,Null);
        
        Campaign NewCampaign = HelpertestDeleteNonQualifiedVolunteer.CreateCampaign('Homeowner Volunteers','01/07/2013',true);
        insert(NewCampaign);
        System.AssertNotEquals(NewCampaign.Id,Null);
        
        GW_Volunteers__Volunteer_Job__c NewVolunteerJob = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerJob('Homeowner Evanston',NewCampaign.Id,'Homeowner','Construction Job',8,4,5,4,'All ages');
        insert(NewVolunteerJob );
        System.AssertNotEquals(NewVolunteerJob.Id,Null);
        
        Datetime myDate = datetime.newInstance(2013, 07, 20, 9, 00, 0);
        
        GW_Volunteers__Volunteer_Shift__c VolShiftGeneral = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerShift(NewVolunteerJob.Id,Date.today() + 2,4,'General',2,8,1);
        insert(VolShiftGeneral );
        System.AssertNotEquals(VolShiftGeneral.Id,Null);
        
        List<GW_Volunteers__Volunteer_Shift__c> lstShift = [select id,hh_Type__c,Name,GW_Volunteers__Description__c from GW_Volunteers__Volunteer_Shift__c Where GW_Volunteers__Volunteer_Job__c =: NewVolunteerJob.Id ];
        System.AssertNotEquals(lstShift,Null);
        
        GW_Volunteers__Volunteer_Shift__c shiftHomeowner = new GW_Volunteers__Volunteer_Shift__c();
        GW_Volunteers__Volunteer_Shift__c shiftConsVolunteer = new GW_Volunteers__Volunteer_Shift__c();
        GW_Volunteers__Volunteer_Shift__c shiftCrewLeader = new GW_Volunteers__Volunteer_Shift__c();
        
        for( GW_Volunteers__Volunteer_Shift__c shift : lstShift )
        {
            if( shift.hh_Type__c == 'Homeowners' )
                shiftHomeowner = shift;
                
            if( shift.hh_Type__c == 'Construction Volunteers' )
                shiftConsVolunteer = shift;
                
            if( shift.hh_Type__c == 'Crew Leaders' )
                shiftCrewLeader = shift;
        }
        //system.debug('NewServiceFile.hh_Family_Id__cis '+NewServiceFile.hh_Family_Id__c);
        /////custom setting for shift trigger controller
      
        
       // HOMEtracker__Service_File__c  sFileNew = [select Id, Name,hh_Family_Id__c  from HOMEtracker__Service_File__c where id = : NewServiceFile.id];  
      //  system.debug('sFileNew.hh_Family_Id__cis '+sFileNew.hh_Family_Id__c);
        // Inserted volunteer hours with 5 homeowners where minimum needed are 4 so one floating space is used
        GW_Volunteers__Volunteer_Hours__c NewVolunteerHoursHomeowners = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerHours(NewContact.Id,NewVolunteerJob.Id,'Confirmed',5,date.today(),date.today()+30,shiftHomeowner.Id );
        insert NewVolunteerHoursHomeowners;
        System.AssertNotEquals(NewVolunteerHoursHomeowners.Id,Null);
        
        
        //Another volunteer hour record is added with the homeowner is 1
        GW_Volunteers__Volunteer_Hours__c NewVolunteerHoursHomeownersNew = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerHours(NewContact.Id,NewVolunteerJob.Id,'Confirmed',1,date.today(),date.today()+30,shiftHomeowner.Id);
        insert NewVolunteerHoursHomeownersNew;
        System.AssertNotEquals(NewVolunteerHoursHomeownersNew.Id,Null);
        
         // updated the volunteer hours for homeowner from 1 to 2
        NewVolunteerHoursHomeownersNew.GW_Volunteers__Status__c = 'Completed';
        NewVolunteerHoursHomeownersNew.GW_Volunteers__Number_of_Volunteers__c = 2;
        NewVolunteerHoursHomeownersNew.GW_Volunteers__Start_Date__c = date.today() - 10;
        NewVolunteerHoursHomeownersNew.GW_Volunteers__End_Date__c = date.today() + 10;
        update NewVolunteerHoursHomeownersNew;
        
        
        // decrease the volunteer hours
        NewVolunteerHoursHomeownersNew.GW_Volunteers__Number_of_Volunteers__c = 1;
        update NewVolunteerHoursHomeownersNew;
        
         
        //decrese volunteer hours from 2 to 5 
        NewVolunteerHoursHomeowners.GW_Volunteers__Number_of_Volunteers__c = 5;
        update NewVolunteerHoursHomeowners;
        
        test.starttest();
        // delete homeowner shift volunteer hours
        delete NewVolunteerHoursHomeowners;
        
        // undelete operation on the same volunteer hours
        undelete NewVolunteerHoursHomeowners;
      // Helper_CNTRL_ONInsertTrackVoleerHours.resetValue();
        Contact NewContactConstruction = HelpertestDeleteNonQualifiedVolunteer.CreateContact('TestLastName','10/07/2013',false,'Homeowner');
        insert(NewContactConstruction);
        System.AssertNotEquals(NewContactConstruction.Id,Null);
       
        Volunteer_Registration__c VolRegistrationConstruction = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerRegistration('TestWaiver Name',NewContactConstruction.Id);
        insert VolRegistrationConstruction;
        System.AssertNotEquals(VolRegistrationConstruction.Id,Null);
        
        GW_Volunteers__Volunteer_Hours__c NewVolunteerHoursConstructionVol = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerHours(NewContactConstruction.Id,NewVolunteerJob.Id,'Confirmed',5,date.today(),date.today()+30,shiftConsVolunteer.Id);
        insert(NewVolunteerHoursConstructionVol );
        System.AssertNotEquals(NewVolunteerHoursConstructionVol.Id,Null);
       
        GW_Volunteers__Volunteer_Hours__c NewVolunteerHoursConstructionVolExceed = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerHours(NewContactConstruction.Id,NewVolunteerJob.Id,'Confirmed',2,date.today(),date.today()+30,shiftConsVolunteer.Id);
        insert(NewVolunteerHoursConstructionVolExceed );
        System.AssertNotEquals(NewVolunteerHoursConstructionVolExceed.Id,Null);
       // Helper_CNTRL_ONInsertTrackVoleerHours.resetValue();
        //volunteer hours are incremented by 2
        NewVolunteerHoursConstructionVolExceed.GW_Volunteers__Status__c = 'Completed';
        NewVolunteerHoursConstructionVolExceed.GW_Volunteers__Start_Date__c = date.today() - 10;
        NewVolunteerHoursConstructionVolExceed.GW_Volunteers__End_Date__c = date.today() + 10;
        NewVolunteerHoursConstructionVolExceed.GW_Volunteers__Number_of_Volunteers__c = 4;
        update NewVolunteerHoursConstructionVolExceed;
       // Helper_CNTRL_ONInsertTrackVoleerHours.resetValue();
        //volunteer hours are incremented by 1
        NewVolunteerHoursConstructionVolExceed.GW_Volunteers__Number_of_Volunteers__c = 5;
        update NewVolunteerHoursConstructionVolExceed;
      
        //delete construction volunteer shifts hours
        delete NewVolunteerHoursConstructionVolExceed;
        //undelete construction volunteer shifts hours
        undelete NewVolunteerHoursConstructionVolExceed;
        //volunteer hours are decremented by 1
        NewVolunteerHoursConstructionVolExceed.GW_Volunteers__Number_of_Volunteers__c = 4;
        update NewVolunteerHoursConstructionVolExceed;
         //volunteer hours are decremented by 3
        NewVolunteerHoursConstructionVolExceed.GW_Volunteers__Number_of_Volunteers__c = 1;
        update NewVolunteerHoursConstructionVolExceed;
        
        test.stoptest();
        Contact NewContactNotQualified = HelpertestDeleteNonQualifiedVolunteer.CreateContact('TestLastName','10/07/2013',false,'Homeowner');
        insert( NewContactNotQualified );
        System.AssertNotEquals(NewContactNotQualified.Id,Null);
        
        //invalid volunteer for the shift
        
        GW_Volunteers__Volunteer_Hours__c NewVolunteerHoursTobeDeleted = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerHours(NewContactNotQualified.Id,NewVolunteerJob.Id,'Confirmed',1,date.today(),date.today()+30,shiftConsVolunteer.Id);
        insert(NewVolunteerHoursTobeDeleted );
        System.AssertNotEquals(NewVolunteerHoursTobeDeleted.Id,Null);
        
        Contact NewContactQualified = HelpertestDeleteNonQualifiedVolunteer.CreateContact('TestLastName','20/08/2013',false,'');
        insert(NewContactQualified);
        System.AssertNotEquals(NewContactQualified.Id,Null);
        
        myDate = datetime.newInstance(2013, 07, 29, 9, 00, 0);
        GW_Volunteers__Volunteer_Shift__c VolShiftCrewleader = HelpertestDeleteNonQualifiedVolunteer.CreateVolunteerShift(NewVolunteerJob.Id,Date.Today() + 2,4,'Crew Leaders',2,8,1);
        insert( VolShiftCrewleader );
        System.AssertNotEquals(VolShiftCrewleader.Id,Null);
        System.runAs ( new User(Id = UserInfo.getUserId()) ) 
      {
          TriggerSetting.Active__c = false;
      update TriggerSetting;
      }
    }
}