public with sharing class Utility 
{
	// function accept the string in the format field=val; and create map from the same
	public static map<string,string> fieldMapGenerator( string strsObjectFieldMappingString ) 
	{
		list<string> lstMappedFlields = new list<string>();
		lstMappedFlields = strsObjectFieldMappingString.split(';');
		map<string,string> mapFieldMap = new map<string,string>();
		system.debug('lstMappedFields');
		for(string fld : lstMappedFlields )
		{
		    fld = fld.trim();
		    if(fld != null && fld != '')
		    {
		         list<string> lstMapFields= new list<string>();
		        lstMapFields = fld.split('=');
		        if( lstMapFields != null && lstMapFields.size() > 0 )
		        {
		            mapFieldMap.put(lstMapFields[0].trim(),lstMapFields[1].trim());
		        }
		    }
		}
		
		return mapFieldMap;
	}
	
	// function to create the query from list of fields and type of object and return the list of sObject
	
	public static string createQuery(list<string> lstFields,string sObjName,string filterCriteria)
	{
		
		string strQuery = 'SELECT ';
		for( string fld : lstFields )
		{
			strQuery = strQuery + fld + ', ';
		}
		strQuery = strQuery.trim();
		strQuery = strQuery.removeEnd(',');
		system.debug('Removed End Semicolon : '+strQuery);
		strQuery = strQuery + ' FROM ' + sObjName;
		strQuery = strQuery + ' '+filterCriteria;
		return strQuery;
	}
	
	// function to create the query from set of fields and type of object and return the list of sObject
	
	public static string createQuery(set<string> setFields,string sObjName,string filterCriteria)
	{
		
		string strQuery = '';
		
		for( string fld : setFields )
		{
			strQuery = strQuery + fld + ', ';
		}
		strQuery = strQuery.removeEnd(',');
		strQuery = strQuery + ' FROM ' + sObjName;
		strQuery = strQuery + ' '+filterCriteria;
		return strQuery;
	}
}