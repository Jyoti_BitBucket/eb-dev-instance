/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestJobResource {

    static testMethod void jobRestResourceTest() 
    {
        // create campaign 
        
        Campaign camp = new Campaign();
        camp.Name = 'Test Campaign';
        camp.Description = 'This is the test campaign for the testing purpose';
        insert camp;
        system.assertNotEquals(camp.id,null);
        
       // create volunteer jobs
       
       GW_Volunteers__Volunteer_Job__c constructionJob = new GW_Volunteers__Volunteer_Job__c();
       constructionJob.Name = 'Test Job A';
       constructionJob.GW_Volunteers__Campaign__c = camp.id;
       constructionJob.GW_Volunteers__Description__c = 'This is the test description';
       insert constructionJob;
       system.assertNotEquals(constructionJob.id,null);
       
       // create volunteer jobs
       
       GW_Volunteers__Volunteer_Job__c otherJob = new GW_Volunteers__Volunteer_Job__c();
       otherJob.Name = 'Test Job A';
       otherJob.GW_Volunteers__Campaign__c = camp.id;
       otherJob.GW_Volunteers__Description__c = 'This is the test description';
       //otherJob.Featured__c = true;
       insert otherJob;
       system.assertNotEquals(otherJob.id,null);
       
       Test.startTest();      
       
       RestRequest req = new RestRequest();
       RestResponse res = new RestResponse();
       req.requestURI = 'https://voldev-futurefounders.cs21.force.com/services/apexrest/jobs/.json?fetured=true';
       req.httpMethod = 'GET';
       RestContext.request = req;
       RestContext.response = res;
       GW_Volunteers__Volunteer_Job__c[] results = JobResource.doGet();
       
       system.debug('results '+results);
       Test.stopTest();
    }
}