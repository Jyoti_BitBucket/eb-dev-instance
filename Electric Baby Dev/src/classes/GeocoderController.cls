public with sharing class GeocoderController 
{
	@future(callout=true)
	public static void updateGeocoderData(list<id> lstsObjectIds,string sObjectName)
	{
		string lat,longi;
        string baseURL,authkey,reqURL;
        list<sObject> lstsObjTobeUpdated = new list<sObject>();
        set<Id> setsObjectIds = new set<Id>();
        setsObjectIds.addAll(lstsObjectIds);
		map<string,string> mapFieldsMap = new map<string,string>();
		
		Geocoder_Object_Configuration__c geoCodeCodeConfig = Geocoder_Object_Configuration__c.getInstance('Contact');
		string strsObjectFieldMappingString;
		strsObjectFieldMappingString = geoCodeCodeConfig.Geocoder_Field_mapping_1__c;
		mapFieldsMap = Utility.fieldMapGenerator(strsObjectFieldMappingString);
		system.debug('We got the following map mapFieldsMap: '+mapFieldsMap);
 		list<string> lstFieldsToBeQuried = new list<string>();
 		lstFieldsToBeQuried.addAll(mapFieldsMap.values());
 		lstFieldsToBeQuried.add(geoCodeCodeConfig.Postal_Code__c);
 		lstFieldsToBeQuried.add(geoCodeCodeConfig.City__c);
 		lstFieldsToBeQuried.add(geoCodeCodeConfig.Country__c);
 		lstFieldsToBeQuried.add(geoCodeCodeConfig.State__c);
 		lstFieldsToBeQuried.add(geoCodeCodeConfig.Street__c);
 		string strQuery = Utility.createQuery(lstFieldsToBeQuried,sObjectName,' WHERE id IN :lstsObjectIds ');
 		system.debug('String query created is : '+strQuery);
 		list<sObject> lstsObject = Database.Query(strQuery);
 		
 		US_Geocoder_Settings__c GeocoderSetting = US_Geocoder_Settings__c.getValues('GeoCoder Settings');
        if( GeocoderSetting == null )
        {
            throw new CongressionalDistrictException('Please Update Custom Setting Values');
        }
        else
        {
            baseURL = GeocoderSetting.End_Point_URL__c;
            authkey = GeocoderSetting.API_Key__c;
        }
        
        map<string,string> mapGeoCoderResponse = new map<string,string>();
        // Create a map of "State" and the "Congressional District ==> id of the record"
        map<string,map<string,string>> mapStateWiseCongDist = new map<string,map<string,string>>();
        
           //  Comment No Congressional_District__c
        for( Congressional_District__c CD : [SELECT id,Name,Party__c,Representative__c,Congressional_District__c,State__c FROM Congressional_District__c LIMIT 5000] )
        {
            if( mapStateWiseCongDist.containsKey(CD.State__c) )
            {
                map<string,string> mapCongressionalRepresentatives = new map<string,string>();
                mapCongressionalRepresentatives = mapStateWiseCongDist.get(CD.State__c);
                mapCongressionalRepresentatives.put(CD.Congressional_District__c,CD.id);
                mapStateWiseCongDist.put(CD.State__c,mapCongressionalRepresentatives);
            }
            else
            {
                map<string,string> mapCongressionalRepresentatives = new map<string,string>();
                mapCongressionalRepresentatives.put(CD.Congressional_District__c,CD.id);
                mapStateWiseCongDist.put(CD.State__c,mapCongressionalRepresentatives);
            }
        }
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////
        for( sObject sObj : lstsObject )
        {
            try
            {
            	system.debug('Postal Code field is : '+geoCodeCodeConfig.Postal_Code__c);
            	
            	string PostalCodeField = geoCodeCodeConfig.Postal_Code__c;
            	string StreetField = geoCodeCodeConfig.Street__c;
                system.debug('Postal Code is :: '+sObj.get(PostalCodeField));
                if( PostalCodeField != null && PostalCodeField != '' && StreetField != null && StreetField != '' && sObj.get(PostalCodeField) != null && sObj.get(StreetField) != null )
                {
                    
                    String Street = (string)sObj.get(StreetField);  // Trim and white spaces
                    Street = Street.trim();
                    //Street = Street.substring(0, 5);  // Take only first 5 digits
                    
                    Integer LastComma = 0;  // to track the last occurrence of a comma in address string
                   
                    // removes the last comma from the street
                    if(Street != null && Street != '')
                    {
                        Integer index = Street.indexOfAny(',');
                        if( index != -1 )
                        Street = Street.substring(0, index);
                        Street = Street.removeEnd(',');
                    }
                    
                    String PostalCode = (string)sObj.get(PostalCodeField);
                    PostalCode = PostalCode.trim(); // Trim and white spaces
                    if( PostalCode.length() == 5 || PostalCode.length() == 10 )
                    {
                        PostalCode = PostalCode.substring(0, 5); // Take only first 5 digits
                        reqURL ='zipcode=' + EncodingUtil.urlEncode(PostalCode, 'UTF-8') + '&address=' + EncodingUtil.urlEncode(Street, 'UTF-8')+ '&authkey=' + authkey;
                        system.debug('Request URL is :: '+reqURL);
                    }
                    else
                    {
                        // Can not submit API request without Zipcode
                        // Update Request Date and Status
                       sObj.put('USG_Update_Requested__c', Datetime.now());
                       sObj.put('USG_Update_Status__c', 'Invalid Zip Code');
                        throw new CongressionalDistrictException('Invalid Zip Code');
                    }
                    
                }
                else if( sObj.get(PostalCodeField) == null  )
                {
                    // Can not submit API request without Zipcode
                    // Update Request Date and Status
                    sObj.put('USG_Update_Requested__c', Datetime.now());
                    sObj.put('USG_Update_Status__c', 'No Zip Code provided');
                    //con.addError('Please Enter Postal Code');
                    throw new CongressionalDistrictException('Please Enter Postal Code');
                }
                else if(  sObj.get(PostalCodeField) != null && sObj.get(StreetField) == null )
                {
                    String PostalCode = (string)sObj.get(PostalCodeField);
                    PostalCode = PostalCode.trim(); // Trim and white spaces
                    if(PostalCode.length() != 5 && PostalCode.length() != 10)
                    {
                        // Can not submit API request without Zipcode
                        // Update Request Date and Status
                        sObj.put('USG_Update_Requested__c', Datetime.now());
                        sObj.put('USG_Update_Status__c', 'Invalid Zip Code');
                        throw new CongressionalDistrictException('Invalid Zip Code');
                    }
                    
                    
                    PostalCode = PostalCode.substring(0, 5); // Take only first 9 digits
                    
                    reqURL = 'zipcode=' +  EncodingUtil.urlEncode(PostalCode, 'UTF-8') +  '&address=' + EncodingUtil.urlEncode('123 No Address St', 'UTF-8') + '&authkey=' + authkey;
                }
                
                // Update Request Date and Status
                sObj.put('USG_Update_Requested__c' , Datetime.now());
                sObj.put('USG_Update_Status__c', 'Request Submitted ');
                
                HttpRequest req = new HttpRequest();
                HttpResponse res = new HttpResponse();
                Http http = new Http();
                
                req.setEndpoint(baseURL);
                req.setMethod('GET');
                req.setBody(reqURL);
                req.setTimeout(120000); // timeout in milliseconds
                //req.setCompressed(true); // otherwise we hit a limit of 32000
                XmlStreamReader reader;
                if(!Test.isRunningTest())
                {
                    // Do the request to the geoCoder and parse the response to get the necessary values
                    
                    try
                    {
                        res = http.send(req);
                    }
                    catch(System.CalloutException e)
                    {
                        System.debug('Callout error: '+ e);
                        System.debug(res.toString());
                        sObj.put('USG_Update_Status__c',  e.getMessage());
                    }
                    
                    // Generate the HTTP response as an XML stream
                    reader = res.getXmlStreamReader();
                    
                   
                }
                else
                {
                    string response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><usgeocoder><request_status><request_address>PO Box 94, 80443</request_address><request_status_code>Success</request_status_code><request_status_code_description>Both street address and zip code were found</request_status_code_description><request_status_version>05.0313</request_status_version></request_status><geo_info><geo_status>Match Found</geo_status><latitude>39.5753244891886</latitude><longitude>-106.103095470429</longitude></geo_info><zipcode_carrierroute><zip9><zip9_status>Match Found</zip9_status><zip9_value>80443-9997</zip9_value></zip9></zipcode_carrierroute><jurisdictions_info><congressional_legislators><official_state>Colorado</official_state><congressional_district><congressional_district_status>Match Found</congressional_district_status><congressional_district_name>Congressional District 2</congressional_district_name></congressional_district></congressional_legislators></jurisdictions_info><census_info><msa><msa_status>Match Found</msa_status><msa_name>Silverthorne, CO Micro Area</msa_name></msa></census_info></usgeocoder>';
                    reader = new XmlStreamReader(response);
                }
                string key,val;
                while(reader.hasNext())
                {
                    if( reader.getEventType() == XmlTag.START_ELEMENT )
                    key = (string)reader.getLocalName();
                    
                    if (reader.getEventType() == XmlTag.CHARACTERS)
                    {
                        val = reader.getText();
                        mapGeoCoderResponse.put(key,val);
                        system.debug('KEY ==>> '+key +'VALUE ==>> '+val);
                    }
                    reader.next();
                }
                
                
                // Update Request Date and Status
                sObj.put('USG_Update_Requested__c',Datetime.now());
                
                sObj.put('USG_Update_Status__c', mapGeoCoderResponse.get('request_status_code'));
                if( mapGeoCoderResponse.get('request_status_code') == 'NoMatch' )
                sObj.put('USG_Update_Status__c', 'No Match');
                
                //system.debug(mapGeoCoderResponse);
                system.debug(' mapGeoCoderResponse.get(request_status_code) ==> '+ mapGeoCoderResponse.get('request_status_code'));
                if( mapGeoCoderResponse.get('request_status_code') == 'Success' || mapGeoCoderResponse.get('request_status_code') == 'ZipMatch' )
                {
                    // Update Zip 9
                    system.debug('mapGeoCoderResponse.get(zip9_status)'+mapGeoCoderResponse.get('zip9_status'));
                    if( mapGeoCoderResponse.get('zip9_status') == 'Match Found' )
                    {
                        sObj.put(mapFieldsMap.get('zip9_value') , mapGeoCoderResponse.get('zip9_value'));
                    }
                    else
                    {
                        sObj.put('USG_Update_Status__c', sObj.get('USG_Update_Status__c') + ', Zip9 not found');
                        sObj.put(mapFieldsMap.get('zip9_value'), '');
                    }
                    
                    // Update Geocodes
                    system.debug('mapGeoCoderResponse.get(geo_status) ==> '+mapGeoCoderResponse.get('geo_status'));
                    if(mapGeoCoderResponse.get('geo_status') == 'Match Found')
                    {
                    	system.debug('Values are : '+(string)mapGeoCoderResponse.get('latitude'));
                    	
                        sObj.put(mapFieldsMap.get('latitude'), (string)mapGeoCoderResponse.get('latitude'));
                        sObj.put(mapFieldsMap.get('longitude'), (string)mapGeoCoderResponse.get('longitude'));
                    }
                    else
                    {
                    	system.debug('I am in else : ');
                        sObj.put('USG_Update_Status__c',sObj.get('USG_Update_Status__c') + ', Geocodes not found');
                        sObj.put(mapFieldsMap.get('latitude'), '');
                        sObj.put(mapFieldsMap.get('longitude'), '');
                    }
                    
                    // Update Metropolitan Statistical Area
                    if (mapGeoCoderResponse.get('msa_status') == 'Match Found')
                    {
                        sObj.put(mapFieldsMap.get('msa_name'), mapGeoCoderResponse.get('msa_name'));
                    } else
                    {
                        sObj.put('USG_Update_Status__c',sObj.get('USG_Update_Status__c') + ', MSA not found');
                        system.debug('=======> '+mapFieldsMap.get('msa_name'));
                        sObj.put(mapFieldsMap.get('msa_name'), '');
                    }
                    
                    // Update Congressional District
                    if (mapGeoCoderResponse.get('congressional_district_status') == 'Match Found')
                    {
                        map<string,string> mapCongressionalRepresentatives = new map<string,string>();
                        mapCongressionalRepresentatives = mapStateWiseCongDist.get(mapGeoCoderResponse.get('official_state'));
                        string DistrictId = mapCongressionalRepresentatives.get( mapGeoCoderResponse.get('congressional_district_name') );
                       // system.debug('setCongDistrict ==> '+DistrictId);
                        
                     /*   if( DistrictId != null)
                        {
                            con.Congressional_District__c = DistrictId;  //  Comment No Congressional_District__c Update the Congressional District Look up
                        }
                        else
                        {
                            con.USG_Update_Status__c += ', District ' +  mapGeoCoderResponse.get('congressional_district_name') +' does not exist in SalesForce';
                            con.Congressional_District__c = null;  //  Comment No Congressional_District__c
                        }*/
                    }
                    else
                    {
                        sObj.put('USG_Update_Status__c', sObj.get('USG_Update_Status__c') + ', District not found');
                        sObj.put('Congressional_District__c', null);  //  Comment No Congressional_District__c
                    }
                    
                }
                
                sObj.put('USG_Trigger_Update__c', false);
                lstsObjTobeUpdated.add(sObj);
                system.debug('Contact is :: '+sObj);
            }
            catch(Exception ExPostal)
            {
                sObj.put('USG_Trigger_Update__c', false);
                sObj.put(mapFieldsMap.get('latitude'), '');
                sObj.put(mapFieldsMap.get('longitude'), '');
                
                sObj.put(mapFieldsMap.get('msa_name'), '');
                lstsObjTobeUpdated.add(sObj);
                if( lstsObjTobeUpdated != null && lstsObjTobeUpdated.size() > 0 )
                {
                    ProcessorControl.inContactFutureContext = true;
                    //update lstContUpdated;
                }
                system.debug('Exception Occured:: '+ExPostal);
            }
            
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        if( lstsObjTobeUpdated != null && lstsObjTobeUpdated.size() > 0 )
        {
            ProcessorControl.inContactFutureContext = true;
            update lstsObjTobeUpdated;
            system.debug('sObjects Updated');
        }
     
	}	
	
}