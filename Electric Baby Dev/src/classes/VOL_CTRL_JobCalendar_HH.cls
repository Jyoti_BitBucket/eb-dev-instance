/***************************************************************************************

****************
* @author David Habib
* @date 7/1/2011
* @description Page Controller class for the Job Calendar visualforce page.  Provides 

Javascript remoting
* method for getting Job Shift data. 
*
* Written by David Habib, copyright (c) 2011-2013 DJH Consulting, djhconsulting.com 
* This program is released under the GNU Affero General Public License, Version 3. 

http://www.gnu.org/licenses/
****************************************************************************************

****************/
global with sharing class VOL_CTRL_JobCalendar_HH {

    // constructor
    global VOL_CTRL_JobCalendar_HH() {
      
        // handle optional parameters (must use string, not ID, to handle null)
        
        // if Campaign passed in, set current state
        map<string, string> params = ApexPages.currentPage().getParameters();
        string p = params.get('campaignId');
        if (p != null && p != '') {
          list<Campaign> listCampaign = [select Id, StartDate from Campaign where Id = :p];
          if (listCampaign.size() > 0) {
            initialDate = Date.valueOf(listCampaign[0].StartDate);
            // because this is a GMT time, we should add some padding, so we'll stick
            // with the same day, even if the locale time zone is up to 12 hours before GMT.
            if (initialDate != null) initialDate = initialDate.addHours(12);  
            campaignId = p;            
          }
        }
        
        // if Job passed in, set current state
        p = params.get('volunteerJobId');
        if (p != null && p != '') {
          list<GW_Volunteers__Volunteer_Job__c> listJob = [select Id, GW_Volunteers__First_Shift__c, 
            GW_Volunteers__Campaign__c from GW_Volunteers__Volunteer_Job__c where Id = :p];
          if (listJob.size() > 0) {
            initialDate = Date.valueOf(listJob[0].GW_Volunteers__First_Shift__c);
            volunteerJobId = p;
            // let caller control whether to filter by campaign or not.
            //campaignId = listJob[0].Campaign__c;            
          }
        }
          
    // if shift passed in, set current state
        p = params.get('volunteerShiftId');
        if (p != null && p != '') {
          list<GW_Volunteers__Volunteer_Shift__c> listShift = [select Id, GW_Volunteers__Start_Date_Time__c, 
            GW_Volunteers__Volunteer_Job__c, GW_Volunteers__Volunteer_Job__r.GW_Volunteers__Campaign__c 
            from GW_Volunteers__Volunteer_Shift__c where Id = :p];
          if (listShift.size() > 0) {
            initialDate = Date.valueOf(listShift[0].GW_Volunteers__Start_Date_Time__c);
            // let caller control whether to filter by campaign & job or not.
            //volunteerJobId = listShift[0].Volunteer_Job__c;
            //campaignId = listShift[0].Volunteer_Job__r.Campaign__c;
          }
        }

//add shiftType in here if you want to work with Campaign and Job drop down
        string s = params.get('shiftType');
        if (s != null && s != '') {
        shiftType = s;
        } else {
        shiftType = '*';
        }
        
        System.debug('shiftType='+shiftType);
        
      fPrint = false; 
        p = params.get('Print');
        if (p == '1') fPrint = true;

      // we can derive whether we are within the Personal Site or not, so no need for parameter.
      fPersonalSite = ApexPages.currentPage().getUrl().contains('PersonalSite');

    // we can derive whether we are within Sites or not, so no need for parameter.
      fWeb = false; 
        fWeb = (Site.getName() != null);

      strLanguage = fWeb ? 'en-us' : null;  // don't set language if viewing in SF.  Let SF decide. 
        p = params.get('Language');
        if (p != null && p != '') strLanguage = p;
        
        p = params.get('initialDate');
    if (p != null && p != '') initialDate = DateTime.valueOf(p);
    
    strCalendarView = 'month';
        p = params.get('CalendarView');
        if (p != null && p != '') strCalendarView = p;

    // finally, keep all parameters for passing to VolunteersJobListingFS
    // (but remove the params that the page will explicitly add to the url)
    params.remove('Calendar');
    params.remove('volunteerShiftId');
    params.remove('jobId');
    params.remove('dtMonthFilter');
    strParams = '';
    string strAmp = '';
    for (string str : params.keySet()) {
      strParams += strAmp + str + '=' + params.get(str);
      strAmp = '&';
    }
    }
    
    global string strURLtoCSSFile { 
        get { 
          if (fPersonalSite) return null;  // just use whatever CSS the Site Template includes.
          
          // only specify the css file if in the web page scenario.
            if (strURLtoCSSFile == null && fWeb) {
                list<Document> listDocs = [SELECT Name, Id From Document WHERE Name = 'JobCalendarCSS.css' LIMIT 1 ];
                if (listDocs.size() > 0) {
                    Document doc = listDocs[0];
                    string imageid = doc.id;
                    imageid = imageid.substring(0,15);
                    strURLToCSSFile = '/servlet/servlet.FileDownload?file=' + imageid;
                }
            }
            return strURLtoCSSFile;
        }  
        
        set;
    }

    // public helper to get the Volunteers Campaign recordtype.
private class MyException extends Exception {}
    public static Id recordtypeIdVolunteersCampaign {
    get {
    if (recordtypeIdVolunteersCampaign == null) {
    list<RecordType> listRT = [SELECT Id FROM RecordType WHERE DeveloperName='Volunteers_Campaign'];
    if (listRT.size() == 0) {
throw (new MyException('The Volunteers Campaign Record Type is missing and must be restored.'));        
    }
    recordtypeIdVolunteersCampaign = listRT[0].Id;
    }
    return recordtypeIdVolunteersCampaign;
    }
    set;
    }
    
//    private VOL_SharedCode volSharedCode;
    
    // global properties
    global string strLanguage { get; set; }
    global boolean fPrint { get; set; }
    global boolean fWeb { get; set; }
    global string strCalendarView { get; set; }
    global string strParams { get; set; }
    global boolean fPersonalSite { get; set; }
      
    // holds the currently selected Campaign in the Campaign dropdown
    global ID campaignId { get; set; }

    // holds the currently selected shiftType 
    global String shiftType { get; set; }

    // the list of active Volunteer Campaigns 
    global list<SelectOption> listSOCampaigns {
        get {
            list<SelectOption> listSO = new list<SelectOption>();
             listSO.add(new SelectOption('', 'all active volunteer campaigns'));
            for (Campaign c : [select Name, Id, StartDate from Campaign 
              where RecordTypeId = :recordtypeIdVolunteersCampaign 
              and IsActive = true order by Name asc limit 999]) {
                listSO.add(new SelectOption(c.id, c.name));
            }       
            return listSO;
        }
        set;
    }
    
    // the user has changed the Campaign dropdown
    global virtual PageReference ChangeCampaign() {
        // clear out all state that is specific to the campaign
        listSOVolunteerJobs = null;
        volunteerJobId = null;
        ChangeVolunteerJob();
        return null;
    }

    // holds the currently select Job in the Job dropdown
    global ID volunteerJobId { get; set; }
    
    // hold the initial date for the calendar
    global DateTime initialDate { get; set; }
    
    // the list of Volunteer Jobs for the specified Campaign
    global list<SelectOption> listSOVolunteerJobs {
      get {
          list<SelectOption> listSO = new list<SelectOption>();
          listSO.add(new SelectOption('', 'all active jobs'));
          if (campaignId == null) {
            for (GW_Volunteers__Volunteer_Job__c vj : [select Name, Id from GW_Volunteers__Volunteer_Job__c 
              where GW_Volunteers__Campaign__r.IsActive = true order by name limit 999]) 

{
                listSO.add(new SelectOption(vj.id, vj.name));
            }          
          } else {
            for (GW_Volunteers__Volunteer_Job__c vj : [select Name, Id from GW_Volunteers__Volunteer_Job__c 
              where GW_Volunteers__Campaign__c = :campaignId order by name limit 999]) {
                listSO.add(new SelectOption(vj.id, vj.name));
            }
          }       
          return listSO;
      } 
      
      set;
    }    

    // the user has changed the Volunteer Job dropdown
    global virtual PageReference ChangeVolunteerJob() {
        // clear out all state that is specific to the Job
        return null;
    }
    
    
  

/***************************************************************************************

****************
  * @description Javascript Remoting method to return a list of Shifts, optionally 

filtered by strFilter.
  * &param strStartDateTime filter for Logs >= startDate
  * &param strEndDateTime filter for Logs <= endDate
  * @return list<Volunteer_Shift__c>, which will be turned into a Javascript collection.
  

****************************************************************************************

****************/
    @RemoteAction global static list<GW_Volunteers__Volunteer_Shift__c> getListShifts_HH(string strCampaignId, string strJobId, 
      string strStartDateTime, string strEndDateTime, string strShiftType) {
      return getListShiftsWeb_HH(strCampaignId, strJobId, strStartDateTime, strEndDateTime, strShiftType, false);    
    }
    
  

/***************************************************************************************

****************
  * @description Javascript Remoting method to return a list of Shifts, optionally filtered by strFilter.
  * &param strStartDateTime filter for Logs >= startDate
  * &param strEndDateTime filter for Logs <= endDate
  * &param fWeb whether to filter out job's who are not display on web.
  * @return list<Volunteer_Shift__c>, which will be turned into a Javascript collection.
  

****************************************************************************************

****************/
    @RemoteAction global static list<GW_Volunteers__Volunteer_Shift__c> getListShiftsWeb_HH(string strCampaignId, string strJobId, 
      string strStartDateTime, string strEndDateTime, string strShiftType, boolean fWeb) {
      DateTime dtStart = datetime.valueOf(strStartDateTime);
      DateTime dtEnd = datetime.valueOf(strEndDateTime);
      boolean fAllCampaign = (strCampaignId == '*');
      boolean fAllJob = (strJobId == '*');
      boolean fAllShifts = (strShiftType == '*');
      string shiftQuery = '';
       
      list<GW_Volunteers__Volunteer_Shift__c> listShifts;
      
shiftQuery = 'select Id, Name, GW_Volunteers__Volunteer_Job__c, GW_Volunteers__Volunteer_Job__r.Name,';
shiftQuery += ' GW_Volunteers__Volunteer_Job__r.GW_Volunteers__Campaign__c, GW_Volunteers__Start_Date_Time__c, GW_Volunteers__Duration__c,'; 
shiftQuery += ' GW_Volunteers__Total_Volunteers__c, GW_Volunteers__Number_of_Volunteers_Still_Needed__c, GW_Volunteers__Description__c,';
shiftQuery += ' hh_Type__c, hh_Group_Type__c, hh_Group_Name__c';
shiftQuery += ' from GW_Volunteers__Volunteer_Shift__c';

shiftQuery += ' where GW_Volunteers__Start_Date_Time__c >= ' + dtStart.format('yyyy-MM-dd\'T\'HH:mm:ss\'z\'') + ' and GW_Volunteers__Start_Date_Time__c <= ' + dtEnd.format('yyyy-MM-dd\'T\'HH:mm:ss\'z\'');
//shiftQuery += ' where GW_Volunteers__Start_Date_Time__c >= 2013-09-01T06:00:00z and GW_Volunteers__Start_Date_Time__c <= 2013-10-06T06:00:00z';


IF (!fAllShifts) {
shiftQuery += ' and GW_Volunteers__Volunteer_Shift__c.hh_Type__c = \'' + strShiftType +'\'';
}


IF (!fAllJob) {
shiftQuery += ' and GW_Volunteers__Volunteer_Job__c = \''+ strJobId +'\''; 
}

IF (fAllCampaign && fAllJob) {
shiftQuery += ' and GW_Volunteers__Volunteer_Job__r.GW_Volunteers__Campaign__r.IsActive = true'; 
}

IF (!fAllCampaign && fAllJob) {
shiftQuery += ' and GW_Volunteers__Volunteer_Job__r.GW_Volunteers__Campaign__c = \'' + strCampaignId + '\''; 
}

shiftQuery += ' and (GW_Volunteers__Volunteer_Job__r.GW_Volunteers__Display_On_Website__c = true or GW_Volunteers__Volunteer_Job__r.GW_Volunteers__Display_On_Website__c = ' + fWeb + ')';
//shiftQuery += ' order by GW_Volunteers__Start_Date_Time__c ASC, GW_Volunteers__Volunteer_Job__c ASC, GW_Volunteers__Volunteer_Shift__c.hh_Type__c DESC';        

shiftQuery += ' order by GW_Volunteers__Volunteer_Shift__c.hh_Type__c DESC';        


listShifts = Database.query(shiftQuery);
        
    return listShifts;
    }     
          
}