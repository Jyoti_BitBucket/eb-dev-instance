/**
 * Google API GeoUtilities Look Up
 */
public class GeoUtilities {
   
@future(callout=true)

public static void updateGeocodes(List<ID> ContactIDList) {
    // Query address fields and status fields from Contacts
    List <Contact> locations = [select Id, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry from Contact where Id in :ContactIDList ];

    // Itterate through Contacts to request coordinates
    for(Contact theLocation : locations) {

        // Build address string for request
        String address = theLocation.MailingStreet + ',' + theLocation.MailingCity + ',' + theLocation.MailingState + ',' + theLocation.MailingPostalCode + ',' + theLocation.MailingCountry;

        address = EncodingUtil.urlEncode(address,'UTF-8');  // Encode address

        List<String> coordinates = new List<String>();  // List of Strings to store coordinates

        system.debug('Sending address to get coords='+address);

        coordinates = GeoUtilities.getCoordinates(address);  

        if(coordinates != null) {
           Decimal pos1 = Decimal.valueOf(coordinates[0]);
           theLocation.Mailing_Location__latitude__s = pos1;

           Decimal pos2 = Decimal.valueOf(coordinates[1]);
           theLocation.Mailing_Location__longitude__s = pos2;

           theLocation.Geocode_Update_Status__c = 'Updated';

           system.debug('GeoUtilities coordinates ' + pos1 + ' ' + pos2 );

        } else {

           system.debug(Logginglevel.ERROR,'GeoUtilities no coordinates!!! for address' ); 

           // Reset coordinates and set Status = Requested
           theLocation.Mailing_Location__latitude__s = 0;
           theLocation.Mailing_Location__longitude__s =0;
           theLocation.Geocode_Update_Status__c = 'Failed';

        }

    }

    update locations;

}

    /*     Input list of address parts: street,  city,  state,  country.     Output: list of coordinates: latitude, longitude    */
public static List<String> getCoordinates(String address /*street,  city,  state,  country */) {

    system.debug('getCoords address='+address);

    List<String> coordinates = new List<String>();

    if(address == Null) {
        system.debug(Logginglevel.ERROR,'GeoUtilities getCoordinates no address provided. Return null');
        return null;
    }

    String url = 'https://maps.googleapis.com/maps/api/geocode/xml?';

	// Sampple Call
	// https://maps.googleapis.com/maps/api/geocode/xml?address=490B+Hammerstone+Ln%2CFrisco%2CColorado%2C80443%2CUnited+States&key=AIzaSyBxOvS_khb7SfP1Wkfkkq0OU50zO7awbKM

    url += 'address=' + address;
    url += '&key=AIzaSyBxOvS_khb7SfP1Wkfkkq0OU50zO7awbKM';
    
//    Http h = new Http();
//    HttpRequest req = new HttpRequest();

//     req.setHeader('Content-type', 'application/x-www-form-urlencoded');
//     req.setHeader('Content-length', '0');
//     req.setEndpoint(url);
//     req.setMethod('POST');


	Http h = new Http();
    HttpRequest req = new HttpRequest();
    
    // url that returns the XML in the response body
    req.setEndpoint(url);
    req.setMethod('GET');

    //String responseBody;
    // Methods defined as TestMethod do not support Web service callouts
 //   if (!Test.isRunningTest()){
//         HttpResponse res = h.send(req);
//         responseBody = res.getBody();
    HttpResponse res = h.send(req);
    Dom.Document doc = res.getBodyDocument();


    //} else {
        // dummy data
   //     responseBody = '200,4,48.5,-123.67';
 //   }
    //system.debug('responseBody='+responseBody);

//	Dom.Document docx = new Dom.Document();
 //    docx.load(responseBody);

	dom.XmlNode xmlroot = doc.getRootElement();
	String xmlStatus = xmlroot.getChildElement('status', null).getText();
    system.debug('xmlStatus=' + xmlStatus);

	dom.XmlNode xmlLatitude = xmlroot.getChildElement('result', null)
	    .getChildElement('geometry', null)
	    .getChildElement('location', null)
	    .getChildElement('lat', null)
	    ;

    system.debug('lattitude=' + xmlLatitude);

	dom.XmlNode xmlLongitude = xmlroot.getChildElement('result', null)
	    .getChildElement('geometry', null)
	    .getChildElement('location', null)
	    .getChildElement('lng', null)
	    ;

    system.debug('longitude=' + xmlLongitude);

    Coordinates = new String[2];
    Coordinates[0] = xmlLatitude.gettext();
    Coordinates[1] = xmlLongitude.gettext();

    return Coordinates;
}

}