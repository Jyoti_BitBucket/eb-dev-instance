public class CampaignSignUpWidget
{
    public ID CampaignId{get;set;}
    public string strFirstName{get;set;}
    public string strLastName{get;set;}
    public string strEmail{get;set;}
    public List<Campaign> listOfCampaign{get;set;}
    public CampaignWrapperClass objCampaignWrapperClass{get;set;}
    public CampaignSignUpWidget()
    {
        String strURLParameters;
        map<string,string> UrlParams = ApexPages.currentPage().getParameters();
        system.debug('URLPARAMS::::'+UrlParams);
        strURLParameters = UrlParams.get('campaignid');
        CampaignId = strURLParameters; 
        if(strURLParameters == '' || strURLParameters == null){
            system.debug('::Null Url Parameter::');
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'CampaignId is missing');
            ApexPages.addMessage(myMsg);
        }
        else{
            try{   
                strURLParameters = UrlParams.get('fname');
                if(strURLParameters != '' || strURLParameters != null)
                strFirstName = strURLParameters;
                strURLParameters = UrlParams.get('lname');
                if(strURLParameters != '' || strURLParameters != null)
                strLastName = strURLParameters;
                strURLParameters = UrlParams.get('email');
                if(strURLParameters != '' || strURLParameters != null)
                strEmail = strURLParameters;
                Date TodayDate = System.today();
                listOfCampaign = [SELECT Id, Name, Chapter__c, Description, Time__c, StartDate, Location_Name__c, Location_Street__c, Location_City__c, Open_Spaces__c, Capacity__C From Campaign Where Id =: CampaignId ];
                
                system.debug('listOfCampaign:::'+listOfCampaign);
                if(listOfCampaign == null || listOfCampaign.size() < 0){
                    system.debug('List Null::');
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Campaign not exist');
                    ApexPages.addMessage(myMsg);
                }
                String strChapter = listOfCampaign[0].Chapter__c;
                String strDescription = listOfCampaign[0].Description;
                String strTime = listOfCampaign[0].Time__c;
                String strLocation = listOfCampaign[0].Location_Name__c;
                DateTime d = listOfCampaign[0].StartDate;
                String monthName= d.format('MMM');
                String dateName = d.Format('dd');
                String strMonthDate = monthName+' '+dateName;
                String strLocationStreet = listOfCampaign[0].Location_Street__c;
                String strLocationCity = listOfCampaign[0].Location_City__c;
                Integer intOpenSpaces = listOfCampaign[0].Open_Spaces__c.intValue();
                Integer intCapacity = listOfCampaign[0].Capacity__c.intValue();
                system.debug('listOfCampaign:::'+listOfCampaign+'::listOfCampaignid'+listOfCampaign[0].id+'::strFirstName::'+strFirstName+'::strLastName::'+strLastName+'::email::'+strEmail);
               
                if(listOfCampaign[0].id == CampaignId){
                    system.debug('::in::'+objCampaignWrapperClass);
                    objCampaignWrapperClass = new CampaignWrapperClass(CampaignId,strFirstName,strLastName,strEmail,strChapter,strMonthDate,strTime,strDescription,strLocation,strLocationStreet,strLocationCity,intCapacity,intOpenSpaces);
                    system.debug(objCampaignWrapperClass);
                }
                
            }
            catch(Exception ex){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Campaign not exist'+ex);
                ApexPages.addMessage(myMsg);
            }
        }
       
    }
    
    public class CampaignWrapperClass{
        public Id CampaignId{get;set;}
        public String strFirstName{get;set;}
        public String strLastName{get;set;}
        public String strEmail{get;set;}
        public String strChapter{get;set;}
        public String strMonthDate{get;set;}
        public String strTime{get;set;}
        public String strDescription{get;set;}
        public String strLocation{get;set;}
        public String strLocationStreet{get;set;}
        public String strLocationCity{get;set;}
        public Integer intCapacity{get;set;}
        public Integer intOpenSpaces{get;set;}
        
        public CampaignWrapperClass(Id CampaignId, String strFirstName, String strLastName, String strEmail,  String strChapter, String strMonthDate, String strTime, String strDescription, String strLocation, String strLocationStreet, String strLocationCity, Integer intCapacity, Integer intOpenSpaces )
        {
            this.CampaignId = CampaignId;
            this.strFirstName = strFirstName;
            this.strLastName = strLastName;
            this.strEmail = strEmail;
            this.strChapter = strChapter;
            this.strMonthDate = strMonthDate;
            this.strTime = strTime;
            this.strDescription = strDescription;
            this.strLocation = strLocation;
            this.strLocationStreet = strLocationStreet;
            this.strLocationCity = strLocationCity;
            this.intCapacity = intCapacity;
            this.intOpenSpaces = intOpenSpaces;
        }
    }
}