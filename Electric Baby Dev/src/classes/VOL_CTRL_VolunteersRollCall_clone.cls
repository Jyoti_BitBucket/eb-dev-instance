public with sharing class VOL_CTRL_VolunteersRollCall_clone {

/* Summary : Controller for the  page VolunteerRollCall which will work as time logging tool for Volunteer 
 *           for a particualar shift. Each volunteer can sign in and sign out multiple time in a day by drag and drop. Each time user 
 *           sign out the hours worked are updated in the related VolunteerHour.
 *           When Volunteer Sign In by drag and drop we set the field 'In Time' on Volunteer_Hours. On Sign Out the Hours_Worked are 
 *           updated in Volunteer_Hours and 'In Time' field is cleared.
 *Created by:
 */
 
	//Using for Vounteer SignUp Contact
    public Contact volunteerHourContact {get;set;}
    //URL shiftId
    public string strShiftId { get; set; }
    public string strJobId { get; set; }
    //list for VolunteerHours Object
    public list<GW_Volunteers__Volunteer_Hours__c> lstVolunteerHours { get; set; }
    public GW_Volunteers__Volunteer_Shift__c objVolunteerShift { get; set; }
    public string VolunteerNameValue { get; set; }
    //list for showing Volunteer for SignOut and SignIn
    public list<VolunteerTimeLog> lstVolunteerSignOut { get; set; }
    public list<VolunteerTimeLog> lstVolunteerSignIn { get; set; }
    //Using for new/existing Volunteer Hour Contact
    public boolean contactFlag { get;set;}
    //map of Contact id,VolunteerHours 
    map<string,GW_Volunteers__Volunteer_Hours__c> mapVolunteerHoursContact = new map<string,GW_Volunteers__Volunteer_Hours__c> ();
   //Custom Settings
    public Volunteer_Roll_Call_Settings__c timeIntervalSetting{get;set;}
    public set<id> setContactId {get; set;}
    public String refreshTimeInterval{get;set;}
    //Exception
    public String strError {get;set;}

    // Fields to hold latest volunteer sign up
    public String signUpVolunteerID {get;set;}
    public String signUpVolunteerName {get;set;}
    //Hold VolunteerName
    String strVolunteerName;
    public list<Contact> lstContact{get;set;}
    Set<ID> shiftID = new Set<ID>();
    //Constructor
    public VOL_CTRL_VolunteersRollCall_clone()
    {
        setVolunteerTimeLog();
        signUpVolunteerID = '0';
        signUpVolunteerName = '0';
    }
    
/*this method is used for getting Volunteer Hours according to thier status
*and set thier value in VolunteerTimeLog for processing
*thismethod called at the time of constructor calling
*/
    public void setVolunteerTimeLog()
    { 
         timeIntervalSetting = Volunteer_Roll_Call_Settings__c.getInstance();
         system.debug('TimeInterval::'+timeIntervalSetting+'::refreshInterval::'+timeIntervalSetting.Refresh_Interval__c+'::LegalInterval::'+timeIntervalSetting.Hours_Worked_Time_Interval__c);
         if( timeIntervalSetting != null && timeIntervalSetting.Refresh_Interval__c != null && timeIntervalSetting.Hours_Worked_Time_Interval__c != null){
         	system.debug('timeIntervalSetting.Refresh_Interval__c::'+string.valueOf(timeIntervalSetting.Refresh_Interval__c*60000));
            refreshTimeInterval = string.valueOf(timeIntervalSetting.Refresh_Interval__c*60000);  
            system.debug('refreshTimeInterval::'+refreshTimeInterval);
         }
         else
            refreshTimeInterval =  string.valueOf(60*60000);//default refresh time
        
         volunteerHourContact = new Contact();
         setContactId = new set<id>();
         setContactId.clear();
         mapVolunteerHoursContact.clear();
         // Get the shift id from the url
        // strShiftId = ApexPages.currentPage().getParameters().get('shiftid');
         strJobId = ApexPages.currentPage().getParameters().get('jobid');
         objVolunteerShift = new GW_Volunteers__Volunteer_Shift__c();
         
     	/*if( (strShiftId == null || strShiftId == '') )
         {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Input.');
            ApexPages.addMessage(myMsg);
         } */
        // System.debug(strJobId);
        /*  if( (strJobId == null || strJobId == '') )
         {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Input.');
            ApexPages.addMessage(myMsg);
						
         }
         else
         {*/
             try
             {
                 /***Retrive all the volunteer Hours for the particualr shift and stored in the map so that hours can be updated on sign out*****/
                 //GW_Volunteers__Volunteer_Shift__c TempShift = new GW_Volunteers__Volunteer_Shift__c();
                 //TempShift  = [SELECT id,Name,GW_Volunteers__Number_of_Volunteers_Still_Needed__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Total_Volunteers__c,GW_Volunteers__Desired_Number_of_Volunteers__c FROM GW_Volunteers__Volunteer_Shift__c WHERE id =: strShiftId LIMIT 1];
                   
              //   if( TempShift.GW_Volunteers__Desired_Number_of_Volunteers__c == null ) // this field is set to -ve value so if the field is blank then shift full msg will not be shown
              //       TempShift.GW_Volunteers__Desired_Number_of_Volunteers__c = -10;
                 
                list<GW_Volunteers__Volunteer_Shift__c> lstTempShift = new list<GW_Volunteers__Volunteer_Shift__c>();
                lstTempShift  = [SELECT id,Name,GW_Volunteers__Number_of_Volunteers_Still_Needed__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Total_Volunteers__c,GW_Volunteers__Desired_Number_of_Volunteers__c FROM GW_Volunteers__Volunteer_Shift__c WHERE hh_Job_Id__c =: strJobId];
               
               
               
                for(GW_Volunteers__Volunteer_Shift__c lstID : lstTempShift) // retrive shift id under perticular jobid
   				 {
        			if(!shiftID.contains(lstID.ID))
       				 {
         			    shiftID.add(lstID.ID);           
      				 }
   				 }
                 
                 
                // objVolunteerShift =  TempShift;
                 
                 lstVolunteerSignOut = new list<VolunteerTimeLog>();
                 lstVolunteerSignIn = new list<VolunteerTimeLog>();
                 lstVolunteerHours = new list<GW_Volunteers__Volunteer_Hours__c>();
                 String strConfirmed = 'Confirmed'; 
                 String strCompleted = 'Completed';
                 lstVolunteerHours = [SELECT id,Name,GW_Volunteers__End_Date__c,GW_Volunteers__Full_Name__c,GW_Volunteers__Contact__c,
                 GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,In_Time__c,GW_Volunteers__Volunteer_Shift__c,
                 GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,
                 GW_Volunteers__Contact__r.FirstName,GW_Volunteers__Contact__r.LastName,
                 Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c WHERE  GW_Volunteers__Volunteer_Shift__c IN: shiftID  order by CreatedDate desc ];
                
                 if( lstVolunteerHours != null && lstVolunteerHours.size() > 0 )
                 {
                  	 //***** this block add all volunteer hour in processing list when status is confirmed and signintime is  null*****************//
                     for( GW_Volunteers__Volunteer_Hours__c volunteerHour : lstVolunteerHours )
                     {
                        if( (volunteerHour.GW_Volunteers__Status__c == strConfirmed || volunteerHour.GW_Volunteers__Status__c == strCompleted )&& volunteerHour.In_Time__c == NULL )
                        {
                            if( setContactId.contains(volunteerHour.GW_Volunteers__Contact__c ) == false )
                            {
                                strVolunteerName = volunteerHour.GW_Volunteers__Contact__r.FirstName+ '<br/>'+volunteerHour.GW_Volunteers__Contact__r.LastName;
                                mapVolunteerHoursContact.put(volunteerHour.GW_Volunteers__Contact__c,volunteerHour);
                                setContactId.add(volunteerHour.GW_Volunteers__Contact__c);
                                lstVolunteerSignOut.add( new VolunteerTimeLog(strVolunteerName,volunteerHour.GW_Volunteers__Contact__c,volunteerHour));//,volunteerHour.In_Time__c));//,LoggedIn,volunteerHour.GW_Volunteers__Status__c) );
                            }
                        }
                 
                 		//***** this block add all volunteer hour in processing list when status is confirmed and signintime is not null**********//
                        if( volunteerHour.GW_Volunteers__Status__c == 'Confirmed' && volunteerHour.In_Time__c != NULL )
                        { 
                            if( setContactId.contains(volunteerHour.GW_Volunteers__Contact__c ) == false )
                            {
                                strVolunteerName = volunteerHour.GW_Volunteers__Contact__r.FirstName+ '<br/>'+volunteerHour.GW_Volunteers__Contact__r.LastName;
                                mapVolunteerHoursContact.put(volunteerHour.GW_Volunteers__Contact__c,volunteerHour);
                                setContactId.add(volunteerHour.GW_Volunteers__Contact__c);
                                lstVolunteerSignIn.add( new VolunteerTimeLog(strVolunteerName,volunteerHour.GW_Volunteers__Contact__c,volunteerHour));//,volunteerHour.In_Time__c));//,LoggedIn,volunteerHour.GW_Volunteers__Status__c) );
                            }
                        }
                  		//***** this block add all volunteer hour in processing list when status is completed and signintime is not null***********//
                        if ( volunteerHour.GW_Volunteers__Status__c == strCompleted && volunteerHour.In_Time__c != NULL )
                        {
                            if( setContactId.contains(volunteerHour.GW_Volunteers__Contact__c ) == false )
                            {
                                strVolunteerName = volunteerHour.GW_Volunteers__Contact__r.FirstName+ '<br/>'+volunteerHour.GW_Volunteers__Contact__r.LastName;
                                mapVolunteerHoursContact.put(volunteerHour.GW_Volunteers__Contact__c,volunteerHour);
                                setContactId.add(volunteerHour.GW_Volunteers__Contact__c);
                                lstVolunteerSignOut.add( new VolunteerTimeLog(strVolunteerName,volunteerHour.GW_Volunteers__Contact__c,volunteerHour));//,volunteerHour.In_Time__c));//,LoggedIn,volunteerHour.GW_Volunteers__Status__c) );
                            }
                        }
                        
                     }
                }
             }
             catch( Exception ex )
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Shift.'+ex);
                 ApexPages.addMessage(myMsg);
             }
            
        // }
     }
     
 /** when volunteer sign in the In time field on volunteer hour is updated with the current time
  * this field is used for time calculation on sign out
  */
   public void SetInTime()
    {
        GW_Volunteers__Volunteer_Hours__c volunteerHour = new GW_Volunteers__Volunteer_Hours__c();
        GW_Volunteers__Volunteer_Hours__c oldVolunteerHour = new GW_Volunteers__Volunteer_Hours__c();
        if( mapVolunteerHoursContact.containsKey( VolunteerNameValue ))
        {
            oldVolunteerHour = mapVolunteerHoursContact.get(VolunteerNameValue);
            oldVolunteerHour = [SELECT id,Name,GW_Volunteers__End_Date__c,GW_Volunteers__Full_Name__c,GW_Volunteers__Contact__c,
            GW_Volunteers__Contact__r.FirstName,GW_Volunteers__Contact__r.LastName,
            GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,In_Time__c,GW_Volunteers__Volunteer_Shift__c,
            GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c WHERE id =: oldVolunteerHour.Id];
        }
        //***** This block will show the Volunteer  signintime when Volunteer signin by drag volunteer hour from Out to In 
        if( oldVolunteerHour.GW_Volunteers__Status__c != 'Completed' )
        {
            
            if( mapVolunteerHoursContact.containsKey( VolunteerNameValue ))
                VolunteerHour =  mapVolunteerHoursContact.get( VolunteerNameValue);
            VolunteerHour.GW_Volunteers__Status__c = 'Confirmed';  
            VolunteerHour.In_Time__c = datetime.now();
            if( VolunteerHour != null )
                update VolunteerHour;
            strVolunteerName = oldVolunteerHour.GW_Volunteers__Contact__r.FirstName+ '<br/>'+oldVolunteerHour.GW_Volunteers__Contact__r.LastName;
            lstVolunteerSignIn.add( new VolunteerTimeLog(strVolunteerName,volunteerHour.GW_Volunteers__Contact__c,volunteerHour));//,volunteerHour.In_Time__c));
        }
        else 
        {   
            //***** If Volunteer Again Drop In In Section After Status Completed 
            
            if(oldVolunteerHour != null)
            {
                oldVolunteerHour.In_Time__c = datetime.now();
                oldVolunteerHour.GW_Volunteers__Status__c = 'Confirmed';
                if(oldVolunteerHour != null)
                    update oldVolunteerHour;
                setContactId.remove(oldVolunteerHour.GW_Volunteers__Contact__c );
            }
            list<GW_Volunteers__Volunteer_Hours__c > lstNewVolHours = [SELECT id,Name,GW_Volunteers__End_Date__c,GW_Volunteers__Full_Name__c,
            GW_Volunteers__Contact__c,GW_Volunteers__Contact__r.FirstName,GW_Volunteers__Contact__r.LastName,
            GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,In_Time__c,
            GW_Volunteers__Volunteer_Shift__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c Where Id =: oldVolunteerHour.Id order by CreatedDate desc ];
            if( lstNewVolHours != null && lstNewVolHours.size() > 0 )
            {
                for( GW_Volunteers__Volunteer_Hours__c volunteerHours : lstNewVolHours )
                {
                        strVolunteerName = volunteerHours.GW_Volunteers__Contact__r.FirstName+ '<br/>'+volunteerHours.GW_Volunteers__Contact__r.LastName;
                        mapVolunteerHoursContact.put(volunteerHours.GW_Volunteers__Contact__c,volunteerHours);
                        setContactId.add(volunteerHours.GW_Volunteers__Contact__c);
                        lstVolunteerSignIn.add( new VolunteerTimeLog(strVolunteerName,volunteerHours.GW_Volunteers__Contact__c,volunteerHours));//,volunteerHours.In_Time__c));
                }
                
            }
            
            List<VolunteerTimeLog> lstnewLog = new List<VolunteerTimeLog>();
            for(VolunteerTimeLog log : lstVolunteerSignIn)
            {
                if(log.VolunteerHour.Id != oldVolunteerHour.id)
                {
                    lstnewLog.add(log);
                }
            }
            lstVolunteerSignIn = new List<VolunteerTimeLog>();
            for(VolunteerTimeLog log : lstnewLog){
                lstVolunteerSignIn.add(log);
            }
        }
         
    }
    
    
    /*When sign out is clicked update the volunteer hour with the elapsed time value
    * this method is called from the page and use the value of contact id in an volunteer hour
    * mapped previously and make new Object with same Volunteer Hours information and show it on VisualForce Page.
    */
  
   public  void UpdateOutTime()
   {
        DateTime logInTime;
        GW_Volunteers__Volunteer_Hours__c volunteerHour = new GW_Volunteers__Volunteer_Hours__c();
        timeIntervalSetting = Volunteer_Roll_Call_Settings__c.getInstance();
        if( mapVolunteerHoursContact.containsKey( VolunteerNameValue ))
            volunteerHour =  mapVolunteerHoursContact.get( VolunteerNameValue );
      
        if( volunteerHour.In_Time__c != null )
        {
            logInTime = volunteerHour.In_Time__c;
            volunteerHour.Out_Time__c= DateTime.now();
            Double HoursWorked = Math.roundToLong((DateTime.now().getTime() - logInTime.getTime())/ (60.0*1000.0));  
            if( volunteerHour.GW_Volunteers__Hours_Worked__c == null )
                volunteerHour.GW_Volunteers__Hours_Worked__c = 0;
            system.debug('HoursWordked::'+HoursWorked+'CustomSettings::'+timeIntervalSetting.Hours_Worked_Time_Interval__c);
            if( HoursWorked != null && HoursWorked >= (timeIntervalSetting.Hours_Worked_Time_Interval__c/2))
            {
                if(HoursWorked <= timeIntervalSetting.Hours_Worked_Time_Interval__c)
                    HoursWorked = timeIntervalSetting.Hours_Worked_Time_Interval__c/60;
                decimal TimeCounter = 0;
                Long modValue;
                if(HoursWorked > timeIntervalSetting.Hours_Worked_Time_Interval__c){
                    TimeCounter = HoursWorked/timeIntervalSetting.Hours_Worked_Time_Interval__c;
                    modValue = Math.mod(Math.round(HoursWorked),Math.round(timeIntervalSetting.Hours_Worked_Time_Interval__c));
                    if(modValue >= timeIntervalSetting.Hours_Worked_Time_Interval__c/2)
                        HoursWorked = (((integer)TimeCounter*timeIntervalSetting.Hours_Worked_Time_Interval__c)+timeIntervalSetting.Hours_Worked_Time_Interval__c)/60;
                    else
                        HoursWorked = (timeIntervalSetting.Hours_Worked_Time_Interval__c*(integer)TimeCounter)/60;
                }
                
                volunteerHour.GW_Volunteers__Hours_Worked__c =  volunteerHour.GW_Volunteers__Hours_Worked__c + HoursWorked;
                volunteerHour.GW_Volunteers__Status__c = 'Completed';
                volunteerHour.In_Time__c = null;
                volunteerHour.Out_Time__c= null;
            }
            else if(volunteerHour.GW_Volunteers__Hours_Worked__c != null && (Math.Ceil(volunteerHour.GW_Volunteers__Hours_Worked__c) >= (timeIntervalSetting.Hours_Worked_Time_Interval__c/60))){
                volunteerHour.GW_Volunteers__Status__c = 'Completed';
                volunteerHour.In_Time__c = null;
                volunteerHour.Out_Time__c= null;
            }
            else
            {
                volunteerHour.GW_Volunteers__Status__c = 'Confirmed';
                volunteerHour.In_Time__c = null;
                volunteerHour.Out_Time__c= null;
            }
            if( volunteerHour != null )
                update volunteerHour;
            
                
        }
     
    }
/*This method is used for SignUp.When a new volunteer fill his firstname,lastname and emailand click on SignUp
*This method is called and search for contact,if contact does not exist,it creates a new contact and a new volunteer hour and show this VolunteerHour in out section
*/ 
/*
*When SignUp Button is clicked then this method will check the existing contact with their Email,FirstName and LastName.
*This method is called form the page and use the contact id and email.
*check the contact id,if not exist in Out Section then mapped,and make a new Volunteer Hours and show it on Out Section if exist and donot in in or out section,will make a new VolunteerHour.
*/ 
    public void SignUp() { 
        contactFlag = true;
        String query;

        // Clear New Sign up Fields
        signUpVolunteerID = '0';
        signUpVolunteerName ='0';

        GW_Volunteers__Volunteer_Shift__c TempShift = new GW_Volunteers__Volunteer_Shift__c();
        try{
            list<GW_Volunteers__Volunteer_Shift__c> listShift = [select GW_Volunteers__Number_of_Volunteers_Still_Needed__c, GW_Volunteers__Desired_Number_of_Volunteers__c from GW_Volunteers__Volunteer_Shift__c where Id = :strShiftId];
            TempShift = [SELECT id,Name,GW_Volunteers__Number_of_Volunteers_Still_Needed__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Total_Volunteers__c,GW_Volunteers__Desired_Number_of_Volunteers__c FROM GW_Volunteers__Volunteer_Shift__c WHERE id =: strShiftId LIMIT 1]; 
            
            //if( TempShift.Total_Number_Of_Volunteers__c == null )
               // TempShift.Total_Number_Of_Volunteers__c = 0;
            
            if( TempShift.GW_Volunteers__Desired_Number_of_Volunteers__c == null ) // this field is set to -ve value so if the field is blank then shift full msg will not be shown
                TempShift.GW_Volunteers__Desired_Number_of_Volunteers__c = -10;
                             
            objVolunteerShift = TempShift;
            
            query = 'SELECT Id, FirstName,LastName,Email FROM Contact where Email <>  NULL limit 50000';
           
            lstContact = new list<Contact>();
            lstContact = (list<Contact>)Database.query(query);
            Contact newVolunteerContact;
            GW_Volunteers__Volunteer_Hours__c signinVolunteerHour;
            GW_Volunteers__Volunteer_Hours__c contactExistVolHour;
            
            boolean flag = true;
            if ( objVolunteerShift != null ) 
            {
                if (objVolunteerShift.GW_Volunteers__Total_Volunteers__c == objVolunteerShift.GW_Volunteers__Desired_Number_of_Volunteers__c) 
                {
                    strError = String.Format('Too many volunteers for Shift',new string[] { string.valueOf(objVolunteerShift.GW_Volunteers__Total_Volunteers__c) }); 
                    throw (new MyException(strError));                  
                }
            }
           // if (shift.Total_Number_Of_Volunteers__c <= shift.GW_Volunteers__Desired_Number_of_Volunteers__c) // commented as if Desired volunteer has blank. Shift can have any number of volunteer (i.e. no restriction and shift will never be full) 
            
            if( lstContact != null && lstContact.size() > 0 )
            {
                for( Contact conDetail : lstContact )
                {
                    if( volunteerHourContact.Email == conDetail.Email && volunteerHourContact.FirstName == conDetail.FirstName  && volunteerHourContact.LastName == conDetail.LastName )
                    {
                        flag = false;
                        //if contact exist add volunteerhour with same contact details 
                        if( !setContactId.contains( conDetail.id ) )
                        {
                            setContactId.add(conDetail.id);
                            contactExistVolHour  = new GW_Volunteers__Volunteer_Hours__c();
                            contactExistVolHour.GW_Volunteers__Status__c = 'Confirmed';
                            contactExistVolHour.In_Time__c = datetime.now();
                            contactExistVolHour.GW_Volunteers__Volunteer_Shift__c = strShiftId;
                            contactExistVolHour.GW_Volunteers__Contact__c =  conDetail.id;
                            contactExistVolHour.GW_Volunteers__Hours_Worked__c = 0;
                            contactExistVolHour.GW_Volunteers__Number_of_Volunteers__c = 1;
                            contactExistVolHour.GW_Volunteers__Volunteer_Job__c = objVolunteerShift.GW_Volunteers__Volunteer_Job__c;
                            contactExistVolHour.GW_Volunteers__Start_Date__c = Date.today();
                            insert contactExistVolHour;

                            // New Volunteer Hour Name and ID accessed by VF page
                            signUpVolunteerID = conDetail.id;
                            signUpVolunteerName =conDetail.FirstName + '<br/>' + conDetail.LastName;
                        }
                        else
                        {
                            contactFlag = false;
                        }
                        
                    }
                }
                // add new volunteer hour with new contact if contact is not exist
                if( flag == true && volunteerHourContact.FirstName != null && volunteerHourContact.LastName != null && volunteerHourContact.Email != null ) 
                {
                        newVolunteerContact = new Contact();
                        newVolunteerContact.FirstName = volunteerHourContact.FirstName;
                        newVolunteerContact.LastName = volunteerHourContact.LastName;
                        newVolunteerContact.Email = volunteerHourContact.Email;
                        insert newVolunteerContact;
                        signinVolunteerHour  = new GW_Volunteers__Volunteer_Hours__c();
                        signinVolunteerHour.GW_Volunteers__Status__c = 'Confirmed';
                        signinVolunteerHour.GW_Volunteers__Volunteer_Shift__c = strShiftId;
                        signinVolunteerHour.GW_Volunteers__Contact__c = newVolunteerContact.Id;
                        signinVolunteerHour.GW_Volunteers__Hours_Worked__c = 0;
                        signinVolunteerHour.GW_Volunteers__Number_of_Volunteers__c = 1;
                        signinVolunteerHour.In_Time__c = datetime.now();
                        signinVolunteerHour.GW_Volunteers__Volunteer_Job__c = objVolunteerShift.GW_Volunteers__Volunteer_Job__c;
                        signinVolunteerHour.GW_Volunteers__Start_Date__c = Date.today();
                        insert signinVolunteerHour;
                        contactFlag = true;

                       // New Volunteer Hour Name and ID accessed by VF page
                        signUpVolunteerID = newVolunteerContact.Id; 
                        signUpVolunteerName= newVolunteerContact.FirstName + '<br/>'+newVolunteerContact.LastName;
                }
            }
            else
            {
                newVolunteerContact = new Contact();
                newVolunteerContact.FirstName = volunteerHourContact.FirstName;
                newVolunteerContact.LastName = volunteerHourContact.LastName;
                newVolunteerContact.Email = volunteerHourContact.Email;
                insert newVolunteerContact;
                signinVolunteerHour  = new GW_Volunteers__Volunteer_Hours__c();
                signinVolunteerHour.GW_Volunteers__Status__c = 'Confirmed';
                signinVolunteerHour.GW_Volunteers__Volunteer_Shift__c = strShiftId;
                signinVolunteerHour.GW_Volunteers__Contact__c = newVolunteerContact.Id;
                signinVolunteerHour.GW_Volunteers__Hours_Worked__c = 0;
                signinVolunteerHour.GW_Volunteers__Number_of_Volunteers__c = 1;
                signinVolunteerHour.In_Time__c = datetime.now();
                signinVolunteerHour.GW_Volunteers__Volunteer_Job__c = objVolunteerShift.GW_Volunteers__Volunteer_Job__c;
                signinVolunteerHour.GW_Volunteers__Start_Date__c = Date.today();
                insert signinVolunteerHour;
                contactFlag = true;

               // New Volunteer Hour Name and ID accessed by VF page
                signUpVolunteerID = newVolunteerContact.Id; 
                signUpVolunteerName= newVolunteerContact.FirstName + '<br/>'+newVolunteerContact.LastName;
            }
            set<GW_Volunteers__Volunteer_Hours__c> setVolHourId = new set<GW_Volunteers__Volunteer_Hours__c>();
            setVolHourId.add(signinVolunteerHour); 
            setVolHourId.add(contactExistVolHour);
            list<GW_Volunteers__Volunteer_Hours__c > lstSignInVolHours = [SELECT id,Name,GW_Volunteers__End_Date__c,GW_Volunteers__Full_Name__c,
            GW_Volunteers__Contact__c,GW_Volunteers__Contact__r.FirstName,GW_Volunteers__Contact__r.LastName,
            GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,In_Time__c,GW_Volunteers__Volunteer_Shift__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c Where id IN : setVolHourId order by CreatedDate desc ];
            if( lstSignInVolHours != null && lstSignInVolHours.size() > 0 )
            {
                for( GW_Volunteers__Volunteer_Hours__c volunteerHours : lstSignInVolHours )
                {
                    if( setContactId.contains(volunteerHours.GW_Volunteers__Contact__c ) == false )
                    {
                        strVolunteerName = volunteerHours.GW_Volunteers__Contact__r.FirstName+ '<br/>'+volunteerHours.GW_Volunteers__Contact__r.LastName;
                        mapVolunteerHoursContact.put(volunteerHours.GW_Volunteers__Contact__c,volunteerHours);
                        setContactId.add(volunteerHours.GW_Volunteers__Contact__c);
                        lstVolunteerSignIn.add( new VolunteerTimeLog(strVolunteerName,volunteerHours.GW_Volunteers__Contact__c,volunteerHours));//,volunteerHours.In_Time__c));
                    }
                }
            } 
            lstSignInVolHours.clear();
            setVolunteerTimeLog();
        }
        catch (exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));         
        }


    }
    
    /******* class is used to store the necessary information for processing ************/
    class VolunteerTimeLog
    {
        public string VolunteerName { get; set; }
        public string VolunteerContactId { get; set; }
        public GW_Volunteers__Volunteer_Hours__c VolunteerHour { get; set; }
        //public DateTime InTime { get; set; }
        public VolunteerTimeLog( string VolunteerName,string VolunteerContactId,GW_Volunteers__Volunteer_Hours__c VolunteerHour)//,DateTime InTime)
        {
            this.VolunteerName = VolunteerName;
            this.VolunteerContactId = VolunteerContactId;
            this.VolunteerHour = VolunteerHour; 
            //this.InTime = InTime;   
        }
    
    }
    private class MyException extends Exception {}
    
}