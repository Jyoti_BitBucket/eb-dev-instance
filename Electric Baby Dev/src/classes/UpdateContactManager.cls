@RestResource(urlMapping='/updateContact/*')
global with sharing class UpdateContactManager{
    @HttpPut
    global static String updateContact(String FirstName,String Id) {
        Contact con = new Contact();
        con.FirstName = FirstName;
        con.Id = Id;
        update con;
        return con.Id;
    }
}