/** 
* @author Franklin Joyce, Idealist Consulting
* @date 03/15/2015
*
* @description :  This apex class maintain occasions(Event) Attendance of affiliates. 
* - The TMC_EventAttendanceController - provides and interface for easily signing 
*	in/out Registrants on a Platoon Event while also logging Registrant's Hours 
*	Contributed
*
* @unit-test: TMC_EventAttendanceControllerTest 
*
* @modifications:
* - M.Smith, 05/26/2015: Bug - always create new Affiliates with Role=Attendee
*	not 'Participant'
* - M.Smith, 05/26/2015: Don't set the Active__c field to True when creating a
*	new Affiliate. Also, don't re-activate an Affiliate if the InactiveStatus
*	value is set to 'Pending Event Participation'.
* - M.Smith, 01/22/2015: Minor changes related to a new custom page that allows
*	for mass attendance update AFTER the event has already been completed. This
*	was required because this page was built specifically to handle check in/out
*	with time tracking for DAY-OF attendance entry. It doesn't work well for
*	marking people as attended AFTER the event has been completed.
*/
public with sharing class EventAttendanceControllerExt{

	public GW_Volunteers__Volunteer_Shift__c shift {get; private set;}
	
    public ApexPages.StandardController stdController{get;set;}
	
	public List<GW_Volunteers__Volunteer_Hours__c> lstVolunteerHours {get;set;}  // List of Platoon Affiliates to match Drop-In emails against
	public List<AffiliatesDetails> lstVolunteerHoursAffiliates { get; set;}  // List of Resgistrants to display
	public Set<id> setAffiliateId {get; set;}  // Set of Affiliate IDs
	public Roll_Call_Custom_Setting__c timeIntervalSetting{get;set;}  // Custom settings peratining to Event Attendance
	public String strEventId;  // Occasion SalesForce ID
	public String strEmailId{get;set;}  // Get EmailId from Visualforce page to registeration of affiliate for Occation
	public String refreshTimeInterval{get;set;}  // Custom Setting for refresh interval 
	public String strPlatoonName{get;set;}  // Platoon Name
	public String strPlatoonID{get;set;}  // Platoon ID   
	public String strOccasionName{get;set;}  // Occasion Name   
	public String errorMessage{get;set;}  // Error Message 
	public String successMessage{get;set;}  // Success Message
	public String affiliateName;  // Affiliate Name
	public Id affiliateIdToUpdate{get; set;}    // Get Affiliate id to update occassion Attendance status 
	public Date Today { get { return Date.today(); }}
	public Boolean b_searchFlag{get;set;}
	public Boolean b_IsBeforeAfterEvent{get;set;}
	public String strStartDateTime{get;set;}
   
	/*
	* Page Constructor
	*/	
	public EventAttendanceControllerExt(ApexPages.StandardController stdController) {
     this.shift = (GW_Volunteers__Volunteer_Shift__c)stdController.getRecord();
		//Id shiftId = stdController.getId();
		Id shiftId =this.shift.Id;
		b_IsBeforeAfterEvent = TRUE;
		try {
			this.shift = [SELECT Id, GW_Volunteers__Start_Date_Time__c, Status__c, 
				Name, Event_End_Time__c, End_Time__c 
				FROM GW_Volunteers__Volunteer_Shift__c WHERE ID = :shiftId];
	  
			//strStartDateTime = (occasion.Occasion_Date__c).format() + ' ' + occasion.Start_Time__c;
			//strShiftName = shift.Name;

			if(shift.Status__c != 'Open') {
				// If the Occasion is not Open
				b_IsBeforeAfterEvent = FALSE;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
					'The Platoon Event is already closed. Please contact your SPS if you have questions.')); 

			} else if(shift.GW_Volunteers__Start_Date_Time__c > Date.Today()) { 
				// If the Occasion is before today
				b_IsBeforeAfterEvent = FALSE;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
					'You are early. This Platoon Event has not yet occurred.')); 

			} else if(shift.GW_Volunteers__Start_Date_Time__c < Date.Today()) {
				b_IsBeforeAfterEvent = FALSE;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
					'This page cannot be used after the event has been completed.'));

			} else {
				lstVolunteerHoursAffiliates = new list<AffiliatesDetails>();
				setAffiliateId = new set<id>();
				setAffiliatesList();
				b_searchFlag=false;
			}
		
		} catch(Exception ex) { 
			system.debug('Error Occurred');           
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
				'UnExpected Error : ' + ex.getMessage()));
		}
	}

	/**
	* This method is called when the page loads
	* - This will check the whether the date of the event is before today
	*	and redirect the user if necessary.
	*/  
	public pagereference init() {
		if (this.shift.GW_Volunteers__Start_Date_Time__c < Date.Today()) {
			// If the event is before TODAY, then redirect the user to the
			// PlatoonEventAttendance page to handle post event entry
			PageReference pg = Page.DummyPage1;
			pg.getParameters().put('id', shift.Id);
			pg.setRedirect(true);
			return pg;
		}
		return null;    
	}

	/*
	* Method to initialize lstVolunteerHoursAffiliates of affiliate details for 
	* Platoon Events with Registrations affiliates 
	*/
	public void setAffiliatesList() {
		// Set refresh interval from custom setting
		timeIntervalSetting = Roll_Call_Custom_Setting__c.getInstance();
		lstVolunteerHoursAffiliates = new list<AffiliatesDetails>();
		setAffiliateId = new set<id>();
		if( timeIntervalSetting != null && timeIntervalSetting.Refresh_Interval__c != null && timeIntervalSetting.Hours_Worked_Time_Interval__c != null) {
			refreshTimeInterval = string.valueOf(timeIntervalSetting.Refresh_Interval__c*60000);  
		} else {
			refreshTimeInterval =  string.valueOf(60*60000);//default refresh time
		}
 
	   
		// Query all Registrations of Platoon Occasion
		GW_Volunteers__Volunteer_Shift__c objshift = new GW_Volunteers__Volunteer_Shift__c();
		try
		{
			lstVolunteerHours = [SELECT Affiliate_Name__c, GW_Volunteers__Contact__c, 
					Attended__c, In_Time__c, Out_Time__c, GW_Volunteers__Hours_Worked__c 
				FROM GW_Volunteers__Volunteer_Hours__c WHERE GW_Volunteers__Volunteer_Shift__c =: shift.Id];
			system.debug('::lstVolunteerHours ::'+lstVolunteerHours );
			// Query all Occasion details              
			objshift = [SELECT id, Name, GW_Volunteers__Volunteer_Job__c, GW_Volunteers__Volunteer_Job__r.Name, 
				GW_Volunteers__Start_Date_Time__c, End_Time__c, Event_End_Time__c  
				FROM GW_Volunteers__Volunteer_Shift__c WHERE id =: shift.Id LIMIT 1];
			strPlatoonName = objshift.GW_Volunteers__Volunteer_Job__r.Name;
			strPlatoonID = objshift.GW_Volunteers__Volunteer_Job__c;
			strOccasionName = objshift.Name;
		 }
		 
		catch(DMLException ex)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,
				'Platoon Event has not been specified. Please access check ins from the Occasion Record. ');
			ApexPages.addMessage(myMsg);
		}
			

		if(lstVolunteerHours != null && !lstVolunteerHours.isEmpty()) {    
			
			for(GW_Volunteers__Volunteer_Hours__c vhours : lstVolunteerHours ) {
				lstVolunteerHoursAffiliates.add(new AffiliatesDetails(vhours));
				if(!setAffiliateId.contains(vhours.GW_Volunteers__Contact__c)) {
					setAffiliateId.add(vhours.GW_Volunteers__Contact__c);
				}
			}
		}       
		
		lstVolunteerHoursAffiliates.sort();      
	}
	
	/*
	* This UpdateAffiliate() method to update affiliates InTime for occasion 
	* and to update their hours contribution for particular occasion.
	*/
	public void UpdateAffiliate() {
		List<GW_Volunteers__Volunteer_Hours__c> volunteerHoursAffiliate = new List<GW_Volunteers__Volunteer_Hours__c>();
		GW_Volunteers__Volunteer_Hours__c objVolunteerHoursAffiliate = new GW_Volunteers__Volunteer_Hours__c();
				system.debug('#0 objRegistrAffiliate.In_time__c=' +objVolunteerHoursAffiliate.In_time__c);
		timeIntervalSetting = Roll_Call_Custom_Setting__c.getInstance();
		Boolean b_CheckInterval = true;
		String style;  // Used to set initial style for Registered Affiliate in interface
		String status;  // Used to set initial status for Registered Affiliate in interface

		// Registered Affiliate to update occasion attended status
		volunteerHoursAffiliate = [SELECT Affiliate_Name__c,GW_Volunteers__Contact__c,
				GW_Volunteers__Hours_Worked__c,Attended__c,In_time__c,Out_time__c 
			FROM GW_Volunteers__Volunteer_Hours__c 
			WHERE GW_Volunteers__Contact__c =: affiliateIdToUpdate 
			AND GW_Volunteers__Volunteer_Shift__c =: shift.ID LIMIT 1]; 

	//	TMC_PlatoonEventServices.updateEventEndTime(this.shift);

		System.debug('registerdAffiliate:test:'+volunteerHoursAffiliate);
		if( volunteerHoursAffiliate != null && volunteerHoursAffiliate.size() > 0 ) {
				
			objVolunteerHoursAffiliate = volunteerHoursAffiliate[0];
			if(objVolunteerHoursAffiliate.In_time__c != null) {  // Sign-out Registrant
			 
				objVolunteerHoursAffiliate.Out_time__c = DateTime.now();  // Set Out Time to now

				// M.Smith, 12/06/2015:
				// If the current date/time is AFTER the event has ended,
				// use the Event End Time instead
			 	if (this.shift.Event_End_Time__c != null 
			 			&& objVolunteerHoursAffiliate.Out_time__c > this.shift.Event_End_Time__c) {
			 		objVolunteerHoursAffiliate.Out_time__c = this.shift.Event_End_Time__c;
			 	}
				
				// Calculate the difference between the In Time an Out Time in hours
				Double l_affiliateInTime = (objVolunteerHoursAffiliate.In_time__c).getTime();
				Double milliseconds = (DateTime.now().getTime()) - l_affiliateInTime;
				Double hours = (milliseconds / 1000)/(60*60);
				system.debug(':hours:'+hours+':milliseconds:'+milliseconds+':seconds:'+ (milliseconds/1000));

				// Set blank hours to 0
				if( objVolunteerHoursAffiliate.GW_Volunteers__Hours_Worked__c == null ) {
					objVolunteerHoursAffiliate.GW_Volunteers__Hours_Worked__c = 0; 
				}
				
				if(hours != null && hours > 0) {  // Have hours to increment Hours_Countributed if longer than 1/2 the Hours Worked Time Interval
					if((milliseconds / 1000) >= (timeIntervalSetting.Hours_Worked_Time_Interval__c*60/2)) {
						objVolunteerHoursAffiliate.GW_Volunteers__Hours_Worked__c  =  objVolunteerHoursAffiliate.GW_Volunteers__Hours_Worked__c + hours;
					}
				}

				objVolunteerHoursAffiliate.In_time__c = null;  // Clear InTime
				objVolunteerHoursAffiliate.Out_time__c = null;  //Clear OutTime

				if(objVolunteerHoursAffiliate.GW_Volunteers__Hours_Worked__c == 0) {
					objVolunteerHoursAffiliate.Attended__c = false;  // Reset Registrant
					style = 'Attendees Absent';   
					status = 'Absent';                  
				} else {
					objVolunteerHoursAffiliate.Attended__c = true;  // Reset Attendee
					style = 'Attendees SignedOut'; 
					status = 'SignedOut';
				}             
			} else {  // Registrant has just signed in
				objVolunteerHoursAffiliate.In_time__c = DateTime.now();  // Set InTime to now
				objVolunteerHoursAffiliate.Attended__c = true ;  // Set as Attended
				objVolunteerHoursAffiliate.Out_time__c = null;  // Clear OutTime
				style = 'Attendees SignedIn';  // Set stye to SignedIn
				status = 'SignedIn';
			}

			// To reflect affiliate color to visualforce page after update
			for(AffiliatesDetails Affiliate : lstVolunteerHoursAffiliates) {
				if(Affiliate.AffiliateId == affiliateIdToUpdate) {
					Affiliate.style = style;                 
					Affiliate.status = status;
				}
			}

			try {
				update objVolunteerHoursAffiliate;
			}
			catch(DMLException ex) {
				System.debug('Error: '+ex.getMessage()+ '  at line: '+ex.getLineNumber());
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,
					'Error: ' + ex.getMessage()) );
			}
		
			lstVolunteerHoursAffiliates.sort();
		
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
				'Error: Affiliate not found.'));
		}
	}
	
	
	/* 
	* Method SearchAffiliate() invoke on drop button action. Method search 
	* for affiliates by email address and register for Occasion.
	*/
	public void SearchAffiliate() { 
		list<Contact> lstSearchAffiliates = new list<Contact>();
		GW_Volunteers__Volunteer_Hours__c objVolunteerHoursAffiliate = new GW_Volunteers__Volunteer_Hours__c();
		GW_Volunteers__Volunteer_Shift__c tempshift = new GW_Volunteers__Volunteer_Shift__c();
		try{
			//Search for Affiliate whose emailId belonges to Platoon of the Platoon Event 
			system.debug('strEmailId::::'+strEmailId);

			// Affiliate_Name__c is contact id add back in Service_Platoon__c,    
			lstSearchAffiliates = [SELECT id,Name, Status__c,
				Active__c, Email, Volunteer_Job__c
				FROM Contact 
				WHERE Email =: strEmailId 
				AND Volunteer_Job__c =: strPlatoonID 
				AND Eligible_for_Registration__c = true LIMIT 1];
			//system.debug('shift.Id:'+occasion.Id+' lstSearchAffiliates[0].Volunteer_Job__c:'+lstSearchAffiliates[0].Volunteer_Job__c);

			tempshift = [SELECT id,Name FROM GW_Volunteers__Volunteer_Shift__c WHERE id =: shift.Id  LIMIT 1]; // Check to see if Affiliate already on Platoon Event

			if (tempshift!= null && !setAffiliateId.contains(lstSearchAffiliates[0].id)) { 

				objVolunteerHoursAffiliate.GW_Volunteers__Volunteer_Shift__c =  tempshift.id;
				objVolunteerHoursAffiliate.GW_Volunteers__Contact__c = lstSearchAffiliates[0].id;
				objVolunteerHoursAffiliate.Attended__c = true; 
				objVolunteerHoursAffiliate.In_time__c = DateTime.now();
				objVolunteerHoursAffiliate.GW_Volunteers__Hours_Worked__c = 0;
				//objVolunteerHoursAffiliate.Attendee_Role__c = TMC_Definitions.REGISTRATION_ROLE_PARTIC;

				if(lstSearchAffiliates[0].Active__c == false){
					lstSearchAffiliates[0].Active__c = true;
					lstSearchAffiliates[0].Status__c = null;
					update lstSearchAffiliates[0];
				}                                 
				affiliateName = lstSearchAffiliates[0].Name;

				try{ 
					insert objVolunteerHoursAffiliate;
				}
				catch(DMLEXception ex){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
						'Error: Invalid EmailId.'+ex.getMessage()));
				}
				
				setAffiliateId.add(lstSearchAffiliates[0].id);
				lstVolunteerHoursAffiliates.add(new AffiliatesDetails(objVolunteerHoursAffiliate,affiliateName));
				strEmailId = '';
				b_searchFlag =true;
				successMessage = lstSearchAffiliates[0].Name + ' has been registered for ' + tempshift.Name;
				errorMessage ='';

			} 
			else {
				
				successMessage =  lstSearchAffiliates[0].Name +' is already registered for' + tempshift.Name + '. Please check in.' ;
				errorMessage ='';
				b_searchFlag =false; // for already exist
				if (lstSearchAffiliates[0].Active__c == false){
					lstSearchAffiliates[0].Active__c = true;
					lstSearchAffiliates[0].Status__c = null;
					update lstSearchAffiliates[0];
				}
				
			}
			   lstVolunteerHoursAffiliates.sort();
		}
		catch( Exception ex ) {
			b_searchFlag = false;
			errorMessage = strEmailId + ' is not a valid email associated with this Platoon or this Event. Please re-enter email address, or register for the Event on the registration website.';
			successMessage = '';
		}
	}
	
	/* 
	* Inner class AffiliatesDetails used to store the necessary information 
	* for processing VF page 
	*/
	public class AffiliatesDetails implements Comparable {
		public String AffiliateName { get; set; }
		public ID AffiliateId { get; set; }
		public String style { get; set; }
		public String status { get; set; }
		public Boolean hours { get; set; } 
		public Boolean inTime ;
	   
		public AffiliatesDetails(GW_Volunteers__Volunteer_Hours__c AffiliateObj) 
		{   
			this.AffiliateName = AffiliateObj.Affiliate_Name__c;
			this.AffiliateId = AffiliateObj.GW_Volunteers__Contact__c; 
		   
			if(AffiliateObj.In_time__c != null) {
				status = 'SignedIn';
			} else if(AffiliateObj.GW_Volunteers__Total_Hours_Worked__c == 0) {
				status = 'Absent';
			}else{
				status = 'SignedOut';
			}

			SetStyle(status);
		}
		
		public AffiliatesDetails(GW_Volunteers__Volunteer_Hours__c AffiliateObj,String affiliateName) {   
			this.AffiliateName = affiliateName;
			this.AffiliateId = AffiliateObj.GW_Volunteers__Contact__c;
			
			if(AffiliateObj.In_time__c != null) {
				status = 'SignedIn';
			} else if(AffiliateObj.GW_Volunteers__Total_Hours_Worked__c == 0) {
				status = 'Absent';
			}else{
				status = 'SignedOut';
			}

			SetStyle(status);
		}
		
		public void SetStyle(String status) {
			if(status == 'SignedIn') {
				style = 'Attendees SignedIn';
			} else if(status == 'SignedOut') {
				style = 'Attendees SignedOut';
			} else {
				style = 'Attendees Absent';
			}  
		}

		public Integer compareTo(Object ObjToCompare) {
			return AffiliateName.CompareTo(((AffiliatesDetails)ObjToCompare).AffiliateName);
		}
	}
}