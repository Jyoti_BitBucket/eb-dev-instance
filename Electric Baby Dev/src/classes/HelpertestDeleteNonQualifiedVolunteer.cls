/*
 * Summary    : Helper Class for testDeleteNonQualifiedVolunteer which create the test data
 * 
 *
 */

@isTest
public Class HelpertestDeleteNonQualifiedVolunteer 
{
    
    public static Volunteer_Registration__c CreateVolunteerRegistration (String WaiverName,String ContactId)
    {
        Volunteer_Registration__c VolRegistration = new Volunteer_Registration__c();
        VolRegistration.Name = WaiverName;
        VolRegistration.Contact__c = ContactId;
        return VolRegistration;
    }
    
    public static Contact CreateContact(String LastName,String RegistrationExpirtionDate,boolean IsCrewLeader,String ClientStatus)
    {
    
        Contact NewContact = new Contact();
       
        NewContact.LastName = LastName;
        //NewContact.hh_Crew_Leader__c = IsCrewLeader;
       // NewContact.hh_Service_File__c = ServiceFile;
        //NewContact.hh_Age_Group__c = AgeGroup;
        NewContact.Email = 'test@test.com';
        return NewContact;
    }
    
    public static GW_Volunteers__Volunteer_Hours__c CreateVolunteerHours(String VolunteerContact,String VolunteerJob,String VolunteerStatus,Integer NumberOfVolunteer,date StartDate,date EndDate,String VolShift)
    {
        GW_Volunteers__Volunteer_Hours__c NewVolunteerHours = New GW_Volunteers__Volunteer_Hours__c();
        NewVolunteerHours.GW_Volunteers__Contact__c = VolunteerContact;
        NewVolunteerHours.GW_Volunteers__Volunteer_Job__c = VolunteerJob;
        NewVolunteerHours.GW_Volunteers__Status__c = VolunteerStatus;
        NewVolunteerHours.GW_Volunteers__Number_of_Volunteers__c = NumberOfVolunteer;
        NewVolunteerHours.GW_Volunteers__Start_Date__c = StartDate;
        NewVolunteerHours.GW_Volunteers__End_Date__c = EndDate;
        NewVolunteerHours.GW_Volunteers__Volunteer_Shift__c = VolShift;
        //NewVolunteerHours.hh_Family_Id__c = FamilyId;
        NewVolunteerHours.GW_Volunteers__Hours_Worked__c = 1;
        //NumberOfValunteer;
        return NewVolunteerHours;       
    
    }
    
    public static GW_Volunteers__Volunteer_Job__c CreateVolunteerJob(String VolunteerJobName,String CampaignName,String JobType,String JobRecordType,Integer MinVolunteer,Integer MinHomeOwner,Integer CrewLeader,Integer FloatingSpace,String AgePermitted)
    {
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.GW_Volunteers__Volunteer_Job__c ; 
        Map<String,Schema.RecordTypeInfo> VolJobRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        
        GW_Volunteers__Volunteer_Job__c NewVolunteerJob = new GW_Volunteers__Volunteer_Job__c();
        NewVolunteerJob.Name = VolunteerJobName;
        //NewVolunteerJob.Job_Type__c = JobType;
        NewVolunteerJob.RecordTypeID = VolJobRecordTypeInfo.get(JobRecordType).getRecordTypeId();// '012L00000008bzC';
        System.debug('****NewVolunteerJob.RecordTypeID'+NewVolunteerJob.RecordTypeID+ ' JobRecordType '+JobRecordType);
        NewVolunteerJob.GW_Volunteers__Campaign__c = CampaignName;
        NewVolunteerJob.hh_Volunteer_Minimum__c =  MinVolunteer;
        NewVolunteerJob.hh_Homeowner_Minimum__c = MinHomeOwner;
       // NewVolunteerJob.hh_Crew_Leaders__c = CrewLeader;
        NewVolunteerJob.hh_Floating_Space__c = FloatingSpace;
        NewVolunteerJob.hh_Ages_Permitted__c = AgePermitted;
       // NewVolunteerJob.Group_Volunteers__c = 5;
        return NewVolunteerJob;
    }
    
    public static Campaign CreateCampaign(String CampaignName,String CampaignStartDate,boolean IsActive)
    {
        RecordType campRecordType =  [select Id from RecordType where Name = 'Volunteer Campaign' limit 1];
        Campaign NewCampaign = new Campaign();
        NewCampaign.Name = CampaignName;
        NewCampaign.StartDate = Date.parse(CampaignStartDate);
        NewCampaign.IsActive = IsActive;
        NewCampaign.RecordtypeId  = campRecordType.Id;
        return NewCampaign;
    }
    
    public static GW_Volunteers__Volunteer_Shift__c CreateVolunteerShift(String VolunteerJob,DateTime StartDateTime,Integer DesiredVolunteer,String Type,Integer TotalNumVolunteer,Integer Duration,Integer MinimumNeeded)
    {
        
        GW_Volunteers__Volunteer_Shift__c VolShift = new GW_Volunteers__Volunteer_Shift__c ();
        VolShift.GW_Volunteers__Volunteer_Job__c = VolunteerJob;
        VolShift.GW_Volunteers__Start_Date_Time__c = StartDateTime;
        VolShift.GW_Volunteers__Desired_Number_of_Volunteers__c = DesiredVolunteer;
        VolShift.GW_Volunteers__Total_Volunteers__c = TotalNumVolunteer;
        VolShift.hh_Type__c = Type;
        
        VolShift.GW_Volunteers__Duration__c = Duration;
        VolShift.hh_Minimum_Needed__c = MinimumNeeded;
        return VolShift;
    }
    
  /*  public static HOMEtracker__Service_File__c CreateServiceFile( String ServiceFileName,String SweatEquityStatus)
    {
        HOMEtracker__Service_File__c file = new HOMEtracker__Service_File__c ();
        file.hh_Sweat_Equity_Status__c = SweatEquityStatus;
        file.Name = ServiceFileName;
        file.hh_Total_Hour_Requirement__c = 200;
        return file;
    }*/
}