/*
*Summary : Controller for the trigger is used for the updation of the CongressionalDistrict in the account.
*      Trigger calls the future method and then do the web service call to the GeoCoder API
*      The return values from the Geocoder are extracted and updated in the Account. On the basis  of the
*      values of CongressionalDistrict it retrives the Congressional District look up and update it to Account
*
*Written By : Pravin Waykar
*
*Date : 28/07/2014
*/

public class CNTRL_AccountInsertUpdateAddCongDistrict
{
    
    @future(callout=true)
    public static void CNTRL_AccountInsertUpdateAddCongDistrict(list<id> lstAccIds)
    {
        string lat,longi;
        string baseURL,authkey,reqURL;
        list<Account> lstAcctUpdated = new list<Account>();
        set<id> setAcctIds = new set<Id>();
        setAcctIds.addAll(lstAccIds);
        list<Account> lstAccount = new list<Account>([SELECT id,Name,ShippingPostalCode,ShippingStreet,USG_Update_Requested__c,USG_Update_Status__c,Longitude__c,Latitude__c,Shipping_Zip9__c FROM Account WHERE id IN : setAcctIds]);
        
        
        US_Geocoder_Settings__c GeocoderSetting = US_Geocoder_Settings__c.getValues('GeoCoder Settings');
        if( GeocoderSetting == null )
        {
            throw new CongressionalDistrictException('Please Update Custom Setting Values');
        }
        else
        {
            baseURL = GeocoderSetting.End_Point_URL__c;
            authkey = GeocoderSetting.API_Key__c;
        }
        
        map<string,string> mapGeoCoderResponse = new map<string,string>();
        
        // Create a map of "State" and the "Congressional District ==> id of the record"
        map<string,map<string,string>> mapStateWiseCongDist = new map<string,map<string,string>>();
   
        for( Congressional_District__c CD : [SELECT id,Name,Party__c,Representative__c,Congressional_District__c,State__c FROM Congressional_District__c LIMIT 5000] )
        {
            
            if( mapStateWiseCongDist.containsKey(CD.State__c) )
            {
                map<string,string> mapCongressionalRepresentatives = new map<string,string>();
                mapCongressionalRepresentatives = mapStateWiseCongDist.get(CD.State__c);
                mapCongressionalRepresentatives.put(CD.Congressional_District__c,CD.id);// Commented bcz NO Object 
                mapStateWiseCongDist.put(CD.State__c,mapCongressionalRepresentatives);
            }
            else
            {
                map<string,string> mapCongressionalRepresentatives = new map<string,string>();
                mapCongressionalRepresentatives.put(CD.Congressional_District__c,CD.id); // Commented bcz NO Object 
                mapStateWiseCongDist.put(CD.State__c,mapCongressionalRepresentatives);
            }
        }
        
        for( Account Acc : lstAccount )
        {
            
            try
            {
                system.debug('Account Postal Code is :: '+Acc.ShippingPostalCode);
                if( Acc.ShippingPostalCode != null && Acc.ShippingStreet != null )
                {
                    
                    String Street = Acc.ShippingStreet;  // Trim and white spaces
                    Street = Street.trim();
                    //Street = Street.substring(0, 5);  // Take only first 5 digits
                    
                    //Integer LastComma = 0;  // to track the last occurrence of a comma in address string
                    /* While ( Street.indexOf(',',LastComma) != -1) {  // Loop through street address to find last ','
                    LastComma = Street.indexOf(',',LastComma);
                    }*/
                    
                    // removes the last comma from the street
                    if(Street != null && Street != '')
                    {
                        Integer index = Street.indexOfAny(',');
                        if( index != -1 )
                        Street = Street.substring(0, index);
                        Street = Street.removeEnd(',');
                    }
                    
                    /*If (LastComma >0) {
                    Street = Street.substring(0, LastComma);  // Trim off the last comma and everything behind it
                    }*/
                    
                    String PostalCode = Acc.ShippingPostalCode;
                    PostalCode = PostalCode.trim(); // Trim and white spaces
                    system.debug('PostalCode.length() '+PostalCode.length());
                    
                    if( PostalCode.length() == 5 || PostalCode.length() == 10 )
                    {
                        PostalCode = PostalCode.substring(0, 5); // Take only first 5 digits
                        reqURL ='zipcode=' + EncodingUtil.urlEncode(PostalCode, 'UTF-8') + '&address=' + EncodingUtil.urlEncode(Street, 'UTF-8')+ '&authkey=' + authkey;
                        system.debug('Request URL is :: '+reqURL);
                    }
                    else
                    {
                        // Can not submit API request without Zipcode
                        // Update Request Date and Status
                        Acc.USG_Update_Requested__c = Datetime.now();
                        Acc.USG_Update_Status__c = 'Invalid Zip Code';
                        throw new CongressionalDistrictException('Invalid Zip Code');
                    }
                }
                else if( Acc.ShippingPostalCode == null  )
                {
                    // Can not submit API request without Zipcode
                    // Update Request Date and Status
                    Acc.USG_Update_Requested__c = Datetime.now();
                    Acc.USG_Update_Status__c = 'No Zip Code provided';
                    //.addError('Please Enter Postal Code');
                    throw new CongressionalDistrictException('Please Enter Postal Code');
                }
                else if(  Acc.ShippingPostalCode != null && Acc.ShippingStreet == null )
                {
                    String PostalCode = Acc.ShippingPostalCode;
                    PostalCode = PostalCode.trim(); // Trim and white spaces
                    if(PostalCode.length() != 5 && PostalCode.length() != 10)
                    {
                        // Can not submit API request without Zipcode
                        // Update Request Date and Status
                        Acc.USG_Update_Requested__c = Datetime.now();
                        Acc.USG_Update_Status__c = 'Invalid Zip Code';
                        throw new CongressionalDistrictException('Invalid Zip Code');
                    }
                    
                    PostalCode = PostalCode.substring(0, 5); // Take only first 5 digits
                    
                    
                    reqURL = 'zipcode=' +  EncodingUtil.urlEncode(PostalCode, 'UTF-8') +  '&address=' + EncodingUtil.urlEncode('123 No Address St', 'UTF-8') + '&authkey=' + authkey;
                }
                
                // Update Request Date and Status
                Acc.USG_Update_Requested__c = Datetime.now();
                Acc.USG_Update_Status__c = 'Request Submitted ';
                
                HttpRequest req = new HttpRequest();
                HttpResponse res = new HttpResponse();
                Http http = new Http();
                
                req.setEndpoint(baseURL);
                req.setMethod('GET');
                req.setBody(reqURL);
                req.setTimeout(120000); // timeout in milliseconds
                //req.setCompressed(true); // otherwise we hit a limit of 32000
                XmlStreamReader reader;
                if(!Test.isRunningTest())
                {
                    // Do the request to the geoCoder and parse the response to get the necessary values
                    
                    try
                    {
                        res = http.send(req);
                        system.debug('Response:: '+res);
                    }
                    catch(System.CalloutException e)
                    {
                        System.debug('Callout error: '+ e);
                        System.debug(res.toString());
                        Acc.USG_Update_Status__c =  e.getMessage();
                    }
                    
                    // Generate the HTTP response as an XML stream
                    reader = res.getXmlStreamReader();
                }
                /*   string key,val;
                while(reader.hasNext())
                {
                if( reader.getEventType() == XmlTag.START_ELEMENT )
                key = (string)reader.getLocalName();
                
                if (reader.getEventType() == XmlTag.CHARACTERS)
                {
                val = reader.getText();
                mapGeoCoderResponse.put(key,val);
                system.debug('KEY ==>> '+key +'VALUE ==>> '+val);
                }
                reader.next();
                }*/
                
                else
                {
                    string response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><usgeocoder><request_status><request_address>PO Box 94, 80443</request_address><request_status_code>Success</request_status_code><request_status_code_description>Both street address and zip code were found</request_status_code_description><request_status_version>05.0313</request_status_version></request_status><geo_info><geo_status>Match Found</geo_status><latitude>39.5753244891886</latitude><longitude>-106.103095470429</longitude></geo_info><zipcode_carrierroute><zip9><zip9_status>Match Found</zip9_status><zip9_value>80443-9997</zip9_value></zip9></zipcode_carrierroute><jurisdictions_info><congressional_legislators><official_state>Colorado</official_state><congressional_district><congressional_district_status>Match Found</congressional_district_status><congressional_district_name>Congressional District 2</congressional_district_name></congressional_district></congressional_legislators></jurisdictions_info><census_info><msa><msa_status>Match Found</msa_status><msa_name>Silverthorne, CO Micro Area</msa_name></msa></census_info></usgeocoder>';
                    reader = new XmlStreamReader(response);
                }
                
                string key,val;
                while(reader.hasNext())
                {
                    if( reader.getEventType() == XmlTag.START_ELEMENT )
                    key = (string)reader.getLocalName();
                    
                    if (reader.getEventType() == XmlTag.CHARACTERS)
                    {
                        val = reader.getText();
                        mapGeoCoderResponse.put(key,val);
                        system.debug('KEY ==>> '+key +'VALUE ==>> '+val);
                    }
                    reader.next();
                }
                // Update Request Date and Status
                Acc.USG_Update_Requested__c = Datetime.now();
                Acc.USG_Update_Status__c = mapGeoCoderResponse.get('request_status_code');
                
                if( mapGeoCoderResponse.get('request_status_code') == 'NoMatch' )
                Acc.USG_Update_Status__c = 'No Match';
                
                system.debug(mapGeoCoderResponse);
                system.debug(' mapGeoCoderResponse.get(request_status_code) ==> '+ mapGeoCoderResponse.get('request_status_code'));
                if( mapGeoCoderResponse.get('request_status_code') == 'Success' || mapGeoCoderResponse.get('request_status_code') == 'ZipMatch' )
                {
                    // Update Zip 9
                    system.debug('mapGeoCoderResponse.get(zip9_status)'+mapGeoCoderResponse.get('zip9_status'));
                    if( mapGeoCoderResponse.get('zip9_status') == 'Match Found' )
                    {
                        Acc.Shipping_Zip9__c = mapGeoCoderResponse.get('zip9_value');
                    }
                    else
                    {
                        Acc.USG_Update_Status__c += ', Zip9 not found';
                        Acc.Shipping_Zip9__c = '';
                    }
                    
                    // Update Geocodes
                    system.debug('mapGeoCoderResponse.get(geo_status) ==> '+mapGeoCoderResponse.get('geo_status'));
                    if(mapGeoCoderResponse.get('geo_status') == 'Match Found')
                    {
                        Acc.Latitude__c = (string)mapGeoCoderResponse.get('latitude');
                        Acc.Longitude__c = (string)mapGeoCoderResponse.get('longitude');
                    }
                    else
                    {
                        Acc.USG_Update_Status__c += ', Geocodes not found';
                        Acc.Latitude__c = '';
                        Acc.Longitude__c = '';
                    }
                    
                    // Update Metropolitan Statistical Area
                    if (mapGeoCoderResponse.get('msa_status') == 'Match Found')
                    {
                        Acc.Metropolitan_Statistical_Area__c = mapGeoCoderResponse.get('msa_name');
                    } else
                    {
                        Acc.USG_Update_Status__c += ', MSA not found';
                        Acc.Metropolitan_Statistical_Area__c = '';
                    }
                    
                    // Update congressional District
                    if (mapGeoCoderResponse.get('congressional_district_status') == 'Match Found')
                    {
                        map<string,string> mapCongressionalRepresentatives = new map<string,string>();
                        mapCongressionalRepresentatives = mapStateWiseCongDist.get(mapGeoCoderResponse.get('official_state'));
                        string DistrictId = mapCongressionalRepresentatives.get( mapGeoCoderResponse.get('congressional_district_name') ); //get the congressional Representatives custom object where District = congressional_district_name
                        system.debug('setCongDistrict ==> '+DistrictId);
                        
                        if( DistrictId != null)
                        {
                            Acc.Congressional_District__c = DistrictId;  //Update the Congressional District Look up
                        }
                        else
                        {
                            Acc.USG_Update_Status__c += ', District ' +  mapGeoCoderResponse.get('congressional_district_name') +' does not exist in SalesForce';
                            Acc.Congressional_District__c = null; 
                        }
                    }
                    else
                    {
                        Acc.USG_Update_Status__c += ', District not found';
                        Acc.Congressional_District__c = null;
                    }
                    
                }
                
                Acc.USG_Trigger_Update__c = false;
                lstAcctUpdated.add(Acc);
                system.debug('Account is :: '+Acc);
            }
            catch(Exception ExPostal)
            {
                Acc.USG_Trigger_Update__c = false;
                Acc.Longitude__c = '';
                Acc.Latitude__c = '';
                Acc.Congressional_District__c = null; // Commented bcz NO Object 
                Acc.Metropolitan_Statistical_Area__c = '';
                lstAcctUpdated.add(Acc);
                if( lstAcctUpdated != null && lstAcctUpdated.size() > 0 )
                {
                    ProcessorControl.inAccountFutureContext = true;
                    // update lstAcctUpdated;
                    system.debug('Account Updated');
                }
                system.debug('Account is in catch block :: '+Acc);
                system.debug('Exception Occured:: '+ExPostal);
            }
            
        }
        
        if( lstAcctUpdated != null && lstAcctUpdated.size() > 0 )
        {
            ProcessorControl.inAccountFutureContext = true;
            update lstAcctUpdated;
            system.debug('Account Updated');
        }
    }
    
}