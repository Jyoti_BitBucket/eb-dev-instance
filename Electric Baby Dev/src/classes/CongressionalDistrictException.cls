/* 
 *Summary : This is the class used for the creation of the custom exception
 *          used in the Congressional distrct triggers on the Account and
 *          Contact
 *Written By : Pravin Waykar
 *
 *Date : 28/07/2014
 */
public class CongressionalDistrictException extends Exception{

}