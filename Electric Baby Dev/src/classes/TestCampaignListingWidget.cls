@isTest
class TestCampaignListingWidget
{
    public static testmethod void testCampaignListingWidget(){
        
        String strRecordType = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Family Services Sessions').getRecordTypeId();
       
        //Set Current Page
        PageReference pageRef = Page.CampaignListingWidget;
        Test.setCurrentPage(pageRef);
        //CampaignObject
        Campaign newCamp1 = new Campaign();
        newCamp1.Recordtypeid = strRecordType;
        newCamp1.Description = 'Test Description';
        newCamp1.Chapter__c = 'Calgary';
        newCamp1.Name = 'TestCampaign1';
        newCamp1.StartDate = Date.Today()+2;
        newCamp1.Location_Name__c = 'habitat test Center';
        newCamp1.Location_Street__c = 'Wall Street West End';
        newCamp1.Location_City__c = 'North carolina';
        newCamp1.Time__c = '5:00PM-7:00PM';
        newCamp1.Capacity__c = 10;
        newCamp1.Registration_Deadline__c = Date.Today()+5;
        newCamp1.Display_on_Web__c = true;
        newCamp1.IsActive= true;
        insert newCamp1;
        system.assertNotEquals(newCamp1,null);
        ApexPages.currentPage().getParameters().put('campaignid',newCamp1.id);
        ApexPages.currentPage().getParameters().put('recordtype',strRecordType);
        ApexPages.currentPage().getParameters().put('monthstToDisplay','4');
        //Controller
        CampaignListingWidget newCampaignListingWidget  = new CampaignListingWidget ();
      
      
       
        //CampaignID not pass to url
        PageReference pageRef3 = Page.CampaignListingWidget;
        Test.setCurrentPage(pageRef3);
         //CampaignObject
        Campaign newCamp3 = new Campaign();
        newCamp3.Recordtypeid = strRecordType;
        newCamp3.Description = 'Test Description';
        newCamp3.Chapter__c = 'Calgary';
        newCamp3.Name = 'TestCampaign3';
        newCamp3.StartDate = Date.Today()+2;
        newCamp3.Location_Name__c = 'habitat test Center';
        newCamp3.Location_Street__c = 'Wall Street West End';
        newCamp3.Location_City__c = 'North carolina';
        newCamp3.Time__c = '5:00PM-7:00PM';
        newCamp3.Capacity__c = 10;
        newCamp3.Registration_Deadline__c = Date.Today()+5;
        newCamp3.Display_on_Web__c = true;
        newCamp3.IsActive= true;
        insert newCamp3 ;
        system.assertNotEquals(newCamp3 ,null);
        ApexPages.currentPage().getParameters().put('recordtype',strRecordType);
        //Controller
        CampaignListingWidget newCampaignListingWidget3  = new CampaignListingWidget ();
        
         //CampaignID pass to url for IndividualCampaign
        PageReference pageRef1 = Page.IndividualCampaignSignUpWidget;
        Test.setCurrentPage(pageRef1);
        //CampaignObject
        Campaign newCamp2 = new Campaign();
        newCamp2.Recordtypeid = strRecordType;
        newCamp2.Description = 'Test Description';
        newCamp2.Chapter__c = 'Calgary';
        newCamp2.Name = 'TestCampaign2';
        newCamp2.StartDate = Date.Today()+2;
        newCamp2.Location_Name__c = 'habitat test Center';
        newCamp2.Location_Street__c = 'Wall Street West End';
        newCamp2.Location_City__c = 'North carolina';
        newCamp2.Time__c = '5:00PM-7:00PM';
        newCamp2.Capacity__c = 10;
        newCamp2.Registration_Deadline__c = Date.Today()+5;
        newCamp2.Display_on_Web__c = true;
        newCamp2.IsActive= true;
        insert newCamp2;
        system.assertNotEquals(newCamp2,null);
        ApexPages.currentPage().getParameters().put('campaignid',newCamp2.id);
        //Controller
        CampaignListingWidget newCampaignListingWidget1  = new CampaignListingWidget ();
        ApexPages.currentPage().getParameters().put('campaignid','');
        CampaignListingWidget newCampaignListingWidget2  = new CampaignListingWidget ();
        
    }
   
  
}