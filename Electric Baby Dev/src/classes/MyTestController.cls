public class MyTestController {

 public contact c { get;set; }
    public MyTestController(ApexPages.StandardController controller) {
    c = new Contact();
    c = (Contact)controller.getRecord();
    system.debug('Contact is '+c);

    }


public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Active','Active'));
        options.add(new SelectOption('InActive','InActive'));
        options.add(new SelectOption('New Sign Up','New Sign Up'));
        options.add(new SelectOption('Prospective','Prospective'));

        return options;
    }
    
    public pagereference save()
    {
        System.debug('Contact before update is');
        update c;
        return null;
    }
}