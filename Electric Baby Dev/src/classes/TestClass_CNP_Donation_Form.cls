@isTest
public class TestClass_CNP_Donation_Form {
    public static testMethod void testOnlineDonations() {
        
        // Use the PageReference Apex class to instantiate a GeneralDonation page        
        PageReference pageRef = Page.GeneralDonation;
        ApexPages.currentPage().getParameters().put('cString','Test String'); 
        ApexPages.currentPage().getParameters().put('firstName','Test');
        ApexPages.currentPage().getParameters().put('lastName','Person');
        ApexPages.currentPage().getParameters().put('email','test@test.com');  
        ApexPages.currentPage().getParameters().put('phone','123-456-7890');
        ApexPages.currentPage().getParameters().put('address','123 Main St');
        ApexPages.currentPage().getParameters().put('city','Frisco');
        ApexPages.currentPage().getParameters().put('state','Colorado');        
        ApexPages.currentPage().getParameters().put('zip','11111');  
        ApexPages.currentPage().getParameters().put('country','USA');
        ApexPages.currentPage().getParameters().put('amount','2500');  
        ApexPages.currentPage().getParameters().put('sku','TEST');
        // Set Hidden Parameters
        Apexpages.currentPage().getParameters().put('customSKU','TEST');
        Apexpages.currentPage().getParameters().put('extraHidden','Extra Hidden');
       
        // Set GeneralDonation as the starting point of this test method
        //Test.setCurrentPage(pageRef);
        
        //Instantiate the OnlineDonations custom controller
        OnlineDonations e=new OnlineDonations();
        
        // Set URL parameters 
        e.URLAmount = '2500';
        e.URLSKU = 'TEST';
        e.CustomString = 'Test String'; 
        
        // Set Amount
        e.item.UnitPrice=11.11;
        e.item.SKU= 'TEST';

        // Set Recurring 
        e.Cnptransaction.recurring = True;
        e.cnptransaction.Periodicity='Month';
        
        // Set the Dedication Type
       /* String TestDedication = e.getDedicationType(); // Fire get method
        e.setDedicationType('In Honor of');
        e.DedicatedToName = 'Honor Test'; 
        e.DedicationType = 'In Honor of';*/
        
        // Set the Designation Type
        String TestDesignation = e.getDesignationType(); // Fire get method
        e.setDesignationType('TEST');

        // Set the Recognition  
        e.RecognitionAnonymous='False';
        e.RecognitionName = 'Recognize Me';
        
        // Set the Newsletter check box
        e.chkNewsLetter = True;
        e.NewsLetter = 'True';
        
        // Set the Lead Source select
        e.LeadSource = 'Web';

        // Set the Extra Select field
        e.ExtraSelect = 'ExtraSelect';
        
        // Set the Extra Text field
        e.ExtraText = 'ExtraText';
        
        // Set the Extra Hidden field
        e.ExtraHidden = 'ExtraHidden';

        // Set Contact Info
        e.BillingInfo.BillingFirstName = 'Test';
        e.BillingInfo.BillingLastName = 'Person';
        e.BillingInfo.BillingEmail = 'test@test.com';
        e.BillingInfo.BillingPhone = '123-456-7890'; 
        e.BillingAddress.BillingAddress1 = '123 Main St';
        e.BillingAddress.BillingCity = 'Frisco';
        e.BillingAddress.BillingPostalCode = '80443';
        e.BillingAddress.BillingStateProvince = 'Colorado';

        // Set Payment Details        
        e.PaymentMethod.CardNumber = '4111111111111111';
        e.PaymentMethod.Cvv2 = '123';
        e.PaymentMethod.ExpirationMonth = '01';
        e.PaymentMethod.ExpirationYear = '2020';
        
        // Instantiate Operation
        //CnP_IaaS.PaaS_Class.Operation Op= new CnP_IaaS.PaaS_Class.Operation();
        //e.Operation=Op;
        //list<CnP_IaaS.Paas_Class.Items> TempList=new list<CnP_IaaS.Paas_Class.Items>();
        //CnP_IaaS.Paas_Class.Items tempitem=new CnP_IaaS.Paas_Class.Items();
        //tempitem.ItemName='Test';
        //TempList.add(tempitem);
        
        //  e.cnpxmlstring.ItemsList=TempList;
        
        //  CnPAccountSetting__c rec=new CnPAccountSetting__c();
        
        //  rec.C_P_AccountGUID__c='Test';
        
        //  rec.C_P_Account_Number__c='Test';
        
        //  insert rec;
        
        // Test send email on failed transaction code
        e.SendEmail();
        
        Test.startTest();
        // Simulate the button click
        e.Submit(); // Is this ever going to get through our validation??
        system.debug(e.CustomString);
      //  system.assertEquals( 'Test String', e.CustomString);
        
        // Create sample date
        //    DateTime dt=System.today();
        //    date dToday = Date.newInstance(dt.year(),dt.month(),dt.day());
        
        
        // Query to find results
       //     Account acct0 = [ select Name, Phone from Account where Name = 'Test Person' ];
//            system.assertEquals( '123-456-7890', acct0.Phone);
        
      //      Contact cont0 = [ select LastName, FirstName from Contact where LastName = 'Person' ];
      //      system.assertEquals( 'Test', cont0.FirstName);
        
       //     Opportunity oppt0 = [ select StageName from Opportunity where AccountID =: acct0.Id AND CloseDate = TODAY AND Amount = 11.11];
        
        //    system.assertEquals( 'Posted', oppt0.StageName);
        
       //     system.assertEquals( 25, oppt0.Amount);
        
        
        
        Test.stopTest();
    }

}