public class EB_OnlineDonations{
    // C&P Getters and Setters
    public CnP_IaaS.PaaS_Class cnpxmlstring{get;set;}
    public CnP_IaaS.PaaS_Class.Operation Operation{get;set;}
    public CnP_IaaS.PaaS_Class.Order Order{get;set;}
    public CnP_IaaS.PaaS_Class.Authentication Authentication{get;set;}
    public CnP_IaaS.PaaS_Class.BillingInformation BillingInfo{get;set;}
    public CnP_IaaS.PaaS_Class.BillingAddress BillingAddress{get;set;}
    public CnP_IaaS.PaaS_Class.PaymentMethod PaymentMethod{get;set;}
    public CnP_IaaS.PaaS_Class.Receipt Receipt{get;set;}
    public boolean cnpTransactionPeriodicity { get; set;} // Check box for the general donation form
    public CnP_IaaS.PaaS_Class.cnpTransaction cnpTransaction{get;set;}
    private CnP_PaaS__CnP_API_Settings__c[] SettingsList { get; set; }
    public List<CnP_IaaS.PaaS_Class.Items> Itemlist{get;set;}
    public CnP_IaaS.PaaS_Class.Items item { get; set; }
    public Boolean settingsError{set;get;}
    
    // Fields from Donation Form Custom Settings
    public Donation_Form_Settings__c formSettings {get;set;}
    public String CnPAccountGUID { get; set; } // To show support email
    public String CnPAccountNumber { get; set; } // To show support email
    public String orderMode { get; set; } // To show hide sections in VF page if "Test" mode
    public String supportEmail { get; set; } // To show support email
    public String thankYouPage { get; set; } // For redirect upon succesful transaction
    public Decimal OtherAmount{ get;set; } // this is referred in communities donation page
    public String GivingDonation {get;set;} // this is referred in communities donation page
    public boolean DefaultStateFromSetting { get;set; } // used in vf page for showing correct input field for state
    public boolean DefaultCountryFromSetting { get;set;}//used in vf page for showing correct country
    public String MinimumAmount{get;set;}
    // Designation
    String DesignationType = '';
    String DesignationDefault;
    public void setDesignationType(String S){this.DesignationType = S;}  // We aren't setting but still required by select list in VF
    public String getDesignationType(){return DesignationType;}
    
    // Recognition
    public String RecognitionName{ get;set; } //Grab text question value
    public String RecognitionAnonymous{get;set;} //Grab text question value
    
    // Lead Source
    public String LeadSource{ get; set; }
    
    // Newsletter
    public boolean chkNewsLetter { get; set;} // Check box for the general donation form
    public String Newsletter { get; set; } // String because we can only pass text
    
    // Extra Select List
    public String ExtraSelect{ get; set; }
    
    // Extra Text Input
    public String ExtraText{ get; set; }
    
    // Extra Hidden Field
    public String ExtraHidden { get; set; }
    
    // SKU
    public String CustomSKU { get; set; }  // SKU passed from VisualForce page
    public String URLSKU { get; set; }  // SKU passed from URL
    
    // Campaign
    public String CustomCampaign { get; set; }  // Campaign passed from VisualForce page
    
    // Amount
    public String URLAmount { get; set; }  // Amount passed from URL
    
    // Custom String to be displayed in the VF page Optional
    public String CustomString { get; set; }
    
    // Fields for C&P Results
    public String transactionCode { get; set; }
    public String transactionResult { get; set; }
    public String transactionNumber { get; set; }
    
    // For checking if USA or Canada holds the country code
    public String country {get; set;}
    public String state {get;set;}
    
    // For checking if USA or Canada holds the country lable in string format
    public String countryLabel {get; set;}
    public String stateLabel {get;set;}
    
    // customSKU and extraHidden fields passed in from VisualForce Page
    public PageReference HiddenParams() {
        CustomSKU = Apexpages.currentPage().getParameters().get('customSKU');
        ExtraHidden = Apexpages.currentPage().getParameters().get('extraHidden');
        return null;
    }
    //For checking dedication ackowledgment
    public boolean DedicationRequest{get;set;}
    // For submitting the other state value other than US
    public string provOtherThanUS { get;set; }
    
    // For submitting the other state value from countries other than US and Canada
    public string provOtherThanUSCan { get;set; }
    //For Posted Letter and Email
    public boolean postLetterNotify{get;set;}
    public boolean emailNotify{get;set;}
    
    ///////////////////
    
    //Fields used to fetch dedication details
    public Opportunity DedicationDetails { get; set; }
    public string orderNo { get; set; }
    
    /*********** Constructor Starts **************/
    
    public EB_OnlineDonations(){
        DefaultStateFromSetting = true;
        DefaultCountryFromSetting = true;
        countryLabel = null;
        stateLabel = null;
        country = '000';
        DedicationRequest = false;
        emailNotify = false;
        postLetterNotify = false;
        // Instantiate C&P objects
        Operation=new CnP_IaaS.PaaS_Class.Operation();
        Order=new CnP_IaaS.PaaS_Class.Order();
        Authentication=new CnP_IaaS.PaaS_Class.Authentication();
        BillingInfo=new CnP_IaaS.PaaS_Class.BillingInformation();
        BillingAddress=new CnP_IaaS.PaaS_Class.BillingAddress();
        PaymentMethod=new CnP_IaaS.PaaS_Class.PaymentMethod();
        Receipt=new CnP_IaaS.PaaS_Class.Receipt();
        Receipt.SendReceipt = false; // Do not send C&P default receipt
        CnPXMLString=new CnP_IaaS.PaaS_Class();
        CnPTransaction=new CnP_IaaS.PaaS_Class.cnpTransaction();
        Itemlist=new list<CnP_IaaS.PaaS_Class.Items>();
        item = new CnP_IaaS.PaaS_Class.Items();
        
        // Using only one item in this transaction. Set non-price details
        item.SKU = 'NONE'; // Default Sku
        item.ItemName = 'Donation'; // Can get renamed in C&P settings
        ItemList.add(item);
        
        // Set Default Newletter sign up to True
        chkNewsLetter = True;
        
        //DedicationDetails initialization
        DedicationDetails = new Opportunity();
        DedicationDetails.Honor_Memorial_Country__c = 'United States';
        // get the order number from the url which is encoded in base64 format
        orderNo = ApexPages.currentPage().getParameters().get('order');
        
        if( orderNo != null )
        {/*
            // take the base64 encoded parameter and create base64 decoded Blob from it
            Blob oredrNoblob = EncodingUtil.base64Decode(orderNo);
            
            // Convert the blob back to a string and print it in the debug log
            orderNo = oredrNoblob.toString(); */
            DedicationDetails.Honor_Memorial_Order_Number__c = orderNo;
        }
        
        
        // Get Custom Settings
        orderMode = 'Test'; // Default to Test Mode if not specified
        formSettings = Donation_Form_Settings__c.getInstance();       // This is used in javascript processing() function
        settingsError = false;
        if(formSettings == null || formSettings.Order_Mode__c == null || formSettings.Thank_You_Page__c == null || formSettings.Support_Email__c == null || formSettings.CnP_Account_GUID__c == null || formSettings.CnP_Account_Number__c == null ) {
            settingsError = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.warning,'Please enter C&P Account details in Setup > Develop > Custom Settings > Donation Form Settings');
            ApexPages.addMessage(myMsg);
        }
        
        // check if default state and country is null
        if( settingsError == false && (formSettings.Default_State__c != null || formSettings.Default_Country__c != null) )
        {
            if( formSettings.Default_State__c != null )
            stateLabel =  formSettings.Default_State__c;
            
            if( formSettings.Default_Country__c != null )
            countryLabel = formSettings.Default_Country__c;
        }
        
        thankYouPage = formSettings.Thank_You_Page__c; // Assign page for redirect
        supportEmail = formSettings.Support_Email__c; // Assign email for support and for failed transactions
        MinimumAmount = string.valueof(formSettings.Minimum_Amount__c);
        Order.OrderMode = (formSettings.Order_Mode__c != null) ? formSettings.Order_Mode__c : 'Test';
        orderMode=Order.OrderMode; // for VF page flag
        CnPAccountGUID = formSettings.CnP_Account_GUID__c; // Set C&P Guid
        CnPAccountNumber = formSettings.CnP_Account_Number__c; // Set C&P Account Number
        if( formSettings != null && formSettings.Default_Amount__c != null )
        item.UnitPrice = (formSettings.Default_Amount__c).setScale(2);  // Assign default Amount
        else
            item.UnitPrice = 0.00;
        BillingAddress.BillingStateProvince = formSettings.Default_State__c;  // Assign Default State
        state = formSettings.Default_State__c;
        if( formSettings.Default_State__c != null )
        if( formSettings.Default_State__c == 'None')
        DefaultStateFromSetting = false;
        
        if( cnpxmlstring.getcountryCodes() != null && cnpxmlstring.getcountryCodes().size() > 0 )
        {
            for(SelectOption  option : cnpxmlstring.getcountryCodes())
            {
                if(formSettings.Default_Country__c == option.getLabel() )
                {
                    BillingAddress.BillingCountryCode = option.getValue();
                }
            }
        }
        if(cnpxmlstring.getUsStates() != null && cnpxmlstring.getUsStates().size() > 0)
        {
            for(SelectOption option : cnpxmlstring.getUsStates())
            {
                if( formSettings.Default_State__c == option.getLabel() )
                {
                    BillingAddress.BillingStateProvince = option.getValue();
                }
            }
        }
               
        if( formSettings.Default_Country__c == null || formSettings.Default_Country__c == null )
        DefaultCountryFromSetting = false;
        
        CnPTransaction.TransactionType = 'Payment'; // Important for donations to get charged and not just authorized
        CnPTransaction.Periodicity = ''; //Set default recurring periodicity as OneTime
        DesignationType='Undesignated';
        
        // Changing the Format for date
        String dd=datetime.now().format('MM/dd/yyyy');
        String mon=dd.substring(0,2);
        String year=dd.substring(7,10);
        PaymentMethod.ExpirationMonth=mon;
        PaymentMethod.ExpirationYear=year;
        
        // Add params from URL
        Map<String, String> params = ApexPages.currentPage().getParameters();
        String paramsReceived = 'Received:';
        system.debug('cnpxmlstring.VaultGUID:Constructor::'+cnpxmlstring.VaultGUID);
        String a = params.get('cString'); // Retreive Custom Output from URL
        if (a != null && a != '') {
            CustomString = a;
            //paramsReceived += 'CustomString=' + CustomString + ', ';
        }
        
        String b = params.get('firstName'); // Retreive First Name from URL
        if (b != null && b != '') {
            BillingInfo.BillingFirstName = b;
            //paramsReceived += 'FirstName=' + BillingInfo.BillingFirstName + ', ';
        }
        
        String c = params.get('lastName'); // Retreive Last Name from URL
        if (c != null && c != '') {
            BillingInfo.BillingLastName = c;
            //paramsReceived += 'LastName=' + BillingInfo.BillingLastName + ', ';
        }
        
        String d = params.get('email'); // Retreive Email from URL
        if (d != null && d != '') {
            BillingInfo.BillingEmail = d;
            //paramsReceived += 'Email=' + BillingInfo.BillingEmail + ', ';
        }
        
        String e = params.get('phone'); // Retreive Phone from URL
        if (e != null && e != '') {
            BillingInfo.BillingPhone = e;
            //paramsReceived += 'Phone=' + BillingInfo.BillingEmail + ', ';
        }
        
        String f = params.get('address'); // Retreive Address from URL
        if (f != null && f != '') {
            BillingAddress.BillingAddress1 = f;
            //paramsReceived += 'Address=' + BillingInfo.BillingEmail + ', ';
        }
        
        String g = params.get('city'); // Retreive Address from URL
        if (g != null && g != '') {
            BillingAddress.BillingCity = g;
            //paramsReceived += 'City=' + BillingInfo.BillingEmail + ', ';
        }
        
        String h = params.get('zip'); // Retreive Zip Code from URL
        if (h != null && h != '') {
            BillingAddress.BillingPostalCode = h;
            //paramsReceived += 'ZipCode=' + BillingAddress.BillingPostalCode + ', ';
        }
        
        String i = params.get('state'); // Retreive Address from URL
        if (i != null && i != '') {
            BillingAddress.BillingStateProvince = i;
            system.debug('State::'+BillingAddress.BillingStateProvince);
            //paramsReceived += 'State=' + BillingInfo.BillingEmail + ', ';
        }
        
        String j = params.get('country'); // Retreive Address from URL
        if (j != null && j != '') {
            if(j == 'United States' || j == 'US' || j == 'USA') BillingAddress.BillingCountryCode = '840';
            if(j == 'Canada') BillingAddress.BillingCountryCode = '124';
            //paramsReceived += 'Country=' + BillingInfo.BillingEmail + ', ';
        }
        
        String k = params.get('amount'); // Retreive Amount from URL
        if (k != null && k != '') {
            URLAmount = k;
            item.unitprice = Decimal.valueOf(k);
            //paramsReceived += 'Amount=' + URLAmount + ', ';
        }
        
        String l = params.get('sku'); // Retreive SKU from URL
        if (l != null && l != '') {
            URLSKU = l;
            //paramsReceived += 'SKU=' + URLSKU + ', ';
        }
        
    }
    /************* Constructor ends ***********/
    
    
    /******** Process button Action goes here *********/
    Public void Submit(){
        
        system.debug('::Cnp Periodicity::'+ CnPTransaction.Periodicity);
        
        Authentication.AccountGuid=CnPAccountGUID; //inserting value of Account no getting from C & P Account
        Authentication.AccountID=CnPAccountNumber;  //inserting value of Account GUID getting from C & P Account
        
        /******** PASSING CUSTOM INFO HERE AND AT BOTTOM LINE *********/
        
        // Check to see if a SKU has been passed in via URL, if not use the SKU from the VF page
        if(URLSKU != null){
            item.SKU = URLSKU;
        }else if(CustomSKU != null){
            item.SKU = CustomSKU;
        }
        system.debug('SKU=' + item.SKU);
        
        
        // Check to see if a Campaign has been passed in via URL, if not use the Campaign from the VF page
        if(CustomCampaign != null){
            Order.Campaign = CustomCampaign;
        }
        system.debug('Campaign=' + Order.Campaign);
        
        // Concat First name & Last name and passing value into "Name on card"
        PaymentMethod.Nameoncard = BillingInfo.BillingFirstName + ' ' + BillingInfo.BillingLastName ;
        
        
        // Recurring Info
        cnptransaction.RecurringMethod = 'Subscription';// Passing the Recurring Method as Subscription
        cnptransaction.Installment = '999';// Passing No. of installments as 999 by default
        
        
        // Custom Fields
        list<CnP_IaaS.PaaS_Class.Customfields> XmlCustomfieldlist_list = new list<CnP_IaaS.PaaS_Class.Customfields>();
        
        // Fund Designation
        if(DesignationType != ''){
            system.debug('DesignationType=' + DesignationType);
            XmlCustomfieldlist_list.add(createCustomField('Designation',DesignationType)); //adding Fieldname,Fieldvalue to the list
        }
        
        
        // Dedication
       // XmlCustomfieldlist_list.add(createCustomField('Dedication',DedicationType)); //adding Fieldname,Fieldvalue to the list
       // XmlCustomfieldlist_list.add(createCustomField('DedicatedTo',DedicatedToName)); //adding Fieldname,Fieldvalue to the list
        
        
        // Recognition
        String recognitionresult = 'Anonymous';
        system.debug('RecognitionAnonymous=' + RecognitionAnonymous);
        if(RecognitionAnonymous == 'false'){
            system.debug('RecognitionAnonymous=' + RecognitionAnonymous);
            recognitionresult = RecognitionName;
        }
        
        XmlCustomfieldlist_list.add(createCustomField('Recognition',recognitionresult)); //adding Fieldname,Fieldvalue to the list
        
        
        // Lead Source
        system.debug('LeadSource=' + LeadSource);
        XmlCustomfieldlist_list.add(createCustomField('LeadSource',LeadSource)); //adding Fieldname,Fieldvalue to the list
        if(cnpTransactionPeriodicity == true)
        {
            CnPTransaction.Periodicity = 'Month';
            CnPTransaction.recurring = true;
        }
        else
            CnPTransaction.Periodicity = ''; //Set default recurring periodicity as OneTime
        
        system.debug('Transaction is:: '+cnptransaction);
        
        // Newsletter
        If(chkNewsLetter == true )
            NewsLetter = 'TRUE';
        else
            NewsLetter = 'FALSE';
      
        system.debug('Newsletter=' + Newsletter);
        XmlCustomfieldlist_list.add(createCustomField('Newsletter',Newsletter)); //adding Fieldname,Fieldvalue to the list
        
        
        // Extra Select
        system.debug('ExtraSelect=' + ExtraSelect);
        XmlCustomfieldlist_list.add(createCustomField('ExtraSelectQuestion',ExtraSelect)); //adding Fieldname,Fieldvalue to the list
        
        
        // Extra Text Question
        system.debug('ExtraText=' + ExtraText);
        XmlCustomfieldlist_list.add(createCustomField('ExtraTextQuestion',ExtraText)); //adding Fieldname,Fieldvalue to the list
        
        
        // Extra Hidden Field
        XmlCustomfieldlist_list.add(createCustomField('ExtraHidden',ExtraHidden)); //adding Fieldname,Fieldvalue to the list
        
        
        // OnlineDonation Mark to distinguish online donations from internal
        XmlCustomfieldlist_list.add(createCustomField('OnlineDonation','ONLINE')); //adding Fieldname,Fieldvalue to the list
        system.debug('::DedicationDetails.Honor_Memorial_Type__c::'+DedicationDetails.Honor_Memorial_Type__c);
        DedicationDetails.Honor_Memorial_Country__c = 'United States';
        if(DedicationDetails.Honor_Memorial_State__c == 'State')
            DedicationDetails.Honor_Memorial_State__c = '';
        if(DedicationRequest == true){
            if(emailNotify == true){
                DedicationDetails.Honor_Memorial_Country__c = '';
                DedicationDetails.Honor_Memorial_State__c = '';
                DedicationDetails.Honor_Memorial_Street__c = '';
                DedicationDetails.Honor_Memorial_Postal_Code__c = '';
                DedicationDetails.Honor_Memorial_City__c = '';
            }
            else if(postLetterNotify == true){
                //DedicationDetails.Honor_Memorial_Country__c = 'United States';
                DedicationDetails.Honor_Memorial_Email__c = '';
            }
            else if(formSettings.Dedication_Delivery_Email__c == true){
                DedicationDetails.Honor_Memorial_State__c = '';
                DedicationDetails.Honor_Memorial_Country__c = '';
                DedicationDetails.Honor_Memorial_Street__c = '';
                DedicationDetails.Honor_Memorial_Postal_Code__c = '';
                DedicationDetails.Honor_Memorial_City__c = '';
            }
            else{
                //DedicationDetails.Honor_Memorial_Country__c = 'United States';
                DedicationDetails.Honor_Memorial_Email__c = '';
            }
        }
        else{
            DedicationDetails.Honor_Memorial_City__c = '';
            DedicationDetails.Honor_Memorial_Email__c = '';
            DedicationDetails.Honor_Memorial_State__c = '';
            DedicationDetails.Honor_Memorial_Country__c = '';
            DedicationDetails.Honor_Memorial_Street__c = '';
            DedicationDetails.Honor_Memorial_Postal_Code__c = '';
            DedicationDetails.Honor_Memorial_Type__c = 'No Dedication';
        }
        
        DedicationDetails.Online_Donation__c = true;
        String OnlineDonation;
        if(DedicationDetails.Online_Donation__c)
            OnlineDonation = 'true';
        //Adding Dedication Details fields to the list
        XmlCustomfieldlist_list.add(createCustomField('Online Donation',OnlineDonation));
        XmlCustomfieldlist_list.add(createCustomField('Honoree Name',DedicationDetails.Honoree_Name__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial to Notify',DedicationDetails.Honor_Memorial_to_Notify__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial Email',DedicationDetails.Honor_Memorial_Email__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial Street',DedicationDetails.Honor_Memorial_Street__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial City',DedicationDetails.Honor_Memorial_City__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial State',DedicationDetails.Honor_Memorial_State__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial Postal Code',DedicationDetails.Honor_Memorial_Postal_Code__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial Country',DedicationDetails.Honor_Memorial_Country__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial Message',DedicationDetails.Honor_Memorial_Message__c));
        XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial Type',DedicationDetails.Honor_Memorial_Type__c));
        
        if( formSettings.Dedication_Delivery_Email__c== true )
          if( emailNotify == true)
            XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial Notification Method','Email'));
        else
            if( formSettings.Dedication_Delivery_Print__c == true )
              if(postLetterNotify == true)
              XmlCustomfieldlist_list.add(createCustomField('Honor/Memorial Notification Method','Postal Letter'));
       
        
        
        if( BillingAddress.BillingCountryCode != '840' )
        BillingAddress.BillingStateProvince = provOtherThanUS;
        
        if( BillingAddress.BillingCountryCode != '840' && BillingAddress.BillingCountryCode != '124' )
        BillingAddress.BillingStateProvince = provOtherThanUSCan;
        
        System.debug('$$$$$$$$Billing State Provience ==>> '+BillingAddress.BillingStateProvince);
        
        /******* Getting the Signatures of related fields ********/
        cnpxmlstring.getOperation_node(Operation); // getting the values of operation node
        cnpxmlstring.getOrder_node(Order); //getting the values of Order node
        cnpxmlstring.getAuthentication_node(Authentication);  //getting the values of Authentication node
        cnpxmlstring.getBillingInformation_node(BillingInfo); //getting the values of Billing Information
        cnpxmlstring.getBillingAddress_node(BillingAddress); //getting the values of Billing Address node
        cnpxmlstring.getItemslist_node(Itemlist);
        cnpxmlstring.getTransactionDetails_node(cnpTransaction); //getting the values of Authentication node
        cnpxmlstring.setfieldslist(XmlCustomfieldlist_list); //setting the values of Customfield list
        cnpxmlstring.getCustomFieldList_node(cnpxmlstring); //getting the values of Customfield list
        cnpxmlstring.getPaymentMethod_node(PaymentMethod); //getting the values of payment node
        cnpxmlstring.getreceipt_node(Receipt); //getting the values of Receipt node
        cnpxmlstring.getgeneratexml(cnpxmlstring); // generating the Xml file
        // Methods defined as TestMethod do not support Web service callouts
        //code to send the email for the failed transactions
        
        
        if (!Test.isRunningTest()){
            cnpxmlstring.Send_Transaction_api(cnpxmlstring.xmlfile); //sending it to the C&P data
        } else {
            Account acct0 = new Account(Name='Franklin Tester', Phone = '1234567890', BillingStreet = '123 Main St', BillingCity = 'Frisco',  BillingState = 'CO');
            insert acct0;
            
            // Instances with NPSP
            //Contact cont0 = new Contact(AccountID=acct0.ID, FirstName='Franklin', LastName='Tester', npe01__Preferred_Email__c='Work', npe01__WorkEmail__c='franklin.joyce@idealistconsulting.com');
            // Instances without NPSP
            Contact cont0 = new Contact(AccountID=acct0.ID, FirstName='Franklin', LastName='Tester', Email='franklin.joyce@idealistconsulting.com');
            
            insert cont0;
            
            // Create sample date
            DateTime dt=System.now();
            date d = Date.newInstance(dt.year(),dt.month(),dt.day());
            
            Opportunity oppt0 = new Opportunity(AccountId=acct0.id, StageName='Posted', CloseDate=d, Amount=100, Name='Test Donation', Description='Test');
            insert oppt0;
            
        }
    }
    
    
    public void SendEmail(){
        system.debug('cnpxmlstring.VaultGUID:::::SendEmail::==>>'+cnpxmlstring.VaultGUID); 
        system.debug('in the send email function transactionCode ::'+transactionCode );
        system.debug('in the send email function transactionResult ::'+transactionResult );
        system.debug('in the send email function transactionNumber ::'+transactionNumber );
        if(transactionCode != '0'){
            String strEmailSub ='Failed Online Donation';
            String strEmailBody ='';
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String emailAddr = UserInfo.getUserEmail();
            
            Donation_Form_Settings__c formSettings = Donation_Form_Settings__c.getInstance();
            supportEmail  = formSettings.Support_Email__c;
            system.debug('formSetting is:: '+formSettings);
            system.debug('formSetting Email is:: '+formSettings.Support_Email__c);
            system.debug('supportEmail   '+supportEmail  );
            
            
            strEmailBody = '<p>A donation attempt in your online forms has failed.</p>' +
            '<h3>Donor Information</h3>' +
            '<p>' + BillingInfo.BillingFirstName + ' ' + BillingInfo.BillingLastName + '<br/>' +
            BillingInfo.BillingEmail + '<br/> '+
            BillingInfo.BillingPhone + '<br/>' +
            BillingAddress.BillingAddress1 + '<br/>' +
            BillingAddress.BillingCity + ', ' + BillingAddress.BillingStateProvince + '<br/>' +
            BillingAddress.BillingCountryCode + '  ' + BillingAddress.BillingPostalCode + '</p><br/>' +
            '<h3>Attempted Donation Details</h3>' +
            '<table>' +
            '<tr><td>Amount:</td><td>' + item.UnitPrice + '</td></tr>' +
            '<tr><td>Frequency:</td><td>' + cnptransaction.Periodicity + '</td></tr>' +
            '<tr><td>Designation:</td><td>' + DesignationType + '</td></tr>' +
            '</table><br/>' +
            '<h3>Processing Result</h3>' +
            '<p>Trasaction Status: ' + cnpxmlstring.ErrorData + '<br/>' +
            'Trasaction Result: ' + cnpxmlstring.TransactionResultCode + '<br/>' +
            'Trasaction XML:<br/>' + cnpxmlstring.xmlfile + '</p>';
            
            String[] toAddresses = new String[] {supportEmail};
            try{
                if( toAddresses != null && toAddresses.size() > 0  ){
                    mail.setToAddresses(toAddresses);
                    mail.setSubject(strEmailSub);
                    mail.setHtmlBody(strEmailBody);
                    system.debug('***mail Content'+mail);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
            catch(Exception ex){
                system.debug('Exception is:: '+ex);
            }
        }
    }
    /******* Create a C&P XML custom field entry ********/
    
    private static CnP_IaaS.PaaS_Class.Customfields createCustomField(String name, String value) {
        CnP_IaaS.PaaS_Class.Customfields cf = new CnP_IaaS.PaaS_Class.Customfields();
        cf.FieldName = name;
        cf.FieldValue = value;
        return cf;
        
    }
    
}