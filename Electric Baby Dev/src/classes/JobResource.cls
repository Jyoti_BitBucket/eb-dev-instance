@RestResource(urlMapping='/jobs/*')

// request url 
// https://cp-testorg-developer-edition.na15.force.com/demo/services/apexrest/jobs/.json
global class JobResource {
    @HttpGet
    global static list<GW_Volunteers__Volunteer_Job__c> doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        system.debug('Params are as follows : '+req.params);
        
        string strQuery = 'SELECT Ages_Permitted_Text__c, GW_Volunteers__Campaign__c, GW_Volunteers__Description__c, GW_Volunteers__Display_on_Website__c,'+
                          'GW_Volunteers__First_Shift__c, hh_Floating_Space__c, hh_Homeowner_Minimum__c, GW_Volunteers__Inactive__c, hh_Job_URL__c,'+
                          'hh_Last_Shift_Start_Date__c, GW_Volunteers__Location_City__c, GW_Volunteers__Location_Information__c, GW_Volunteers__Location__c,'+
                          'GW_Volunteers__Location_Street__c, GW_Volunteers__Location_Zip_Postal_Code__c, GW_Volunteers__Number_of_Completed_Hours__c,'+
                          'GW_Volunteers__Number_of_Shifts__c, GW_Volunteers__Number_of_Volunteers__c, GW_Volunteers__Ongoing__c, GW_Volunteers__Skills_Needed__c,'+
                          'hh_Volunteer_Minimum__c, GW_Volunteers__Volunteer_Website_Time_Zone__c From GW_Volunteers__Volunteer_Job__c';
         
         if(req.params != null)
         {          
            if( req.params.get('featured') == 'true' )
            {
                strQuery = strQuery + ' '+'WHERE Featured__c = true';
            }           
         }         
        GW_Volunteers__Volunteer_Job__c[] lstJobs = Database.query(strQuery);
        return lstJobs;
    }
}