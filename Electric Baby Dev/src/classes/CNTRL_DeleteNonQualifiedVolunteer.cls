public class CNTRL_DeleteNonQualifiedVolunteer 
{
    /* Smmary    : Delete Non qualified volunteer if check box on volunteer hours is checked delete the record 
     *             Also update the desired number of volunteer in sibling shift of volunteer hour shift if criteria is fullfilled
     *
     * Written By : Pravin Waykar
     */
    public static void DeleteNonQualifiedVolunteer( GW_Volunteers__Volunteer_Hours__c[] lstVolunteerHours )
    {
        List<GW_Volunteers__Volunteer_Hours__c> lstVolunteerHoursTobeDeleted = new List<GW_Volunteers__Volunteer_Hours__c>();
       
        if( lstVolunteerHours.size() > 0 && lstVolunteerHours != Null )
        { 
            
            for( GW_Volunteers__Volunteer_Hours__c  volunteerHours : lstVolunteerHours )
            {
                // delete non qualified volunteer hours
                if( volunteerHours.IsDelete__c == true && ! Trigger.isDelete )
                {
                    lstVolunteerHoursTobeDeleted.add(new GW_Volunteers__Volunteer_Hours__c( id = volunteerHours.id ));
                }
            }
        }
      
        if( lstVolunteerHoursTobeDeleted.size() > 0 && lstVolunteerHoursTobeDeleted != Null )
        {
            if( Trigger.isAfter )
            {  
            Database.delete(lstVolunteerHoursTobeDeleted);
            }
        }
   }
}