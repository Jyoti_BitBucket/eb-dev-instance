@isTest 
Public Class TestVOL_CTRL_JobCalendar_HH {

    public static testmethod void TestVOL_CTRL_JobCalendar_HH() {
      
    // create test data
        Campaign cmp = new Campaign(recordtypeid=GW_Volunteers.VOL_SharedCode.recordtypeIdVolunteersCampaign, 
          name='Job Calendar Test Campaign', IsActive=true);
        insert cmp;
        GW_Volunteers__Volunteer_Job__c job = new GW_Volunteers__Volunteer_Job__c(name='Job1', GW_Volunteers__campaign__c=cmp.Id);
        insert job;
        GW_Volunteers__Volunteer_Shift__c shift = new GW_Volunteers__Volunteer_Shift__c(GW_Volunteers__Volunteer_Job__c=job.Id, GW_Volunteers__Duration__c=1, GW_Volunteers__Start_Date_Time__c=System.now());
        insert shift;
      
        //point to our VF page
        PageReference p = new PageReference('Page.JobCalendar_HH');
        p.getParameters().put('campaignId', cmp.id); 
        p.getParameters().put('volunteerJobId', job.id);
        p.getParameters().put('volunteerShiftId', shift.id);
        Test.setCurrentPageReference(p);
      
      // start testing!
    list<GW_Volunteers__Volunteer_Shift__c> listS = VOL_CTRL_JobCalendar_HH.getListShifts_HH('*', '*', '2010-01-01 1:1:1', '2050-01-01 1:1:1', '*');
    system.assert(listS.size() > 0); 
    listS = VOL_CTRL_JobCalendar_HH.getListShifts_HH(cmp.Id, '*', '2014-01-01 1:1:1', '2050-01-01 1:1:1', '*');
    system.assert(listS.size() == 1); 
    listS = VOL_CTRL_JobCalendar_HH.getListShifts_HH(cmp.Id, job.Id, '2014-01-01 1:1:1', '2050-01-01 1:1:1', '*');
    system.assert(listS.size() == 1); 
    
        //instantiate the controller 
        VOL_CTRL_JobCalendar_HH ctrl = new VOL_CTRL_JobCalendar_HH();
    system.assert(ctrl.strURLtoCSSFile == null || ctrl.strURLtoCSSFile != null);
    system.assertEquals(null, ctrl.strLanguage);
    system.assertEquals(cmp.Id, ctrl.campaignId);
    system.assertEquals(job.Id, ctrl.volunteerJobId);
    system.assert(ctrl.listSOCampaigns.size() >= 2);
    system.assertEquals(shift.GW_Volunteers__Start_Date_Time__c, ctrl.initialDate);
    system.assert(ctrl.listSOVolunteerJobs.size() >= 2);
    ctrl.campaignId = null;    
    ctrl.ChangeVolunteerJob();
    ctrl.ChangeCampaign();     
    system.assert(ctrl.listSOVolunteerJobs.size() >= 2);

    }
}