/* Summary : Controller for the  page DevTestV4SSignIn which which work as time logging tool for volunteer 
 *           for a particualar shift. Each volunteer can sign in and sign out multiple time in a day by drag and drop. Each time user 
 *           sign out the hours worked are updated in the related volunteer hour.
 *           When volunteer Sign In by drag and drop we set the field 'In Time' on Volunteer_Hours. On sign out the Hours_Worked are 
 *           updated in Volunteer_Hours and 'In Time' field is cleared and a new Object will be created with same VolunteerHour object information.
 *Created by:
 */
public with sharing class CNTRL_OnDragDropSignIn 
{   
    public Contact contactDetails {get;set;}
    public string strShiftId { get; set; }
    public list<GW_Volunteers__Volunteer_Hours__c> lstVolHours { get; set; }
    public list<GW_Volunteers__Volunteer_Hours__c> lstInCompleteVolHours { get; set; }
    public GW_Volunteers__Volunteer_Shift__c shift { get; set; }
    public string VolunteerNameValue { get; set; }
    public list<VolunteerTimeLog> lstVolunteerTimeLog { get; set; }
    public list<VolunteerTimeLog> lstNewVolunteertTimeLog { get; set; }
    public boolean contactFlag { get;set;} 
    map<string,GW_Volunteers__Volunteer_Hours__c> mapContactVolHours = new map<string,GW_Volunteers__Volunteer_Hours__c> ();
    Volunteer_RoleCall_Custom_Settings__c timeIntervalSetting;
    set<id> setContactId = new set<id>();
    public GW_Volunteers__Volunteer_Hours__c vhours {
        get {
            if (vhours == null) vhours = new GW_Volunteers__Volunteer_Hours__c(GW_Volunteers__Number_of_Volunteers__c = 1);
            return vhours;      
        }
        set;
    }

    public CNTRL_OnDragDropSignIn ()
    {
        
        setVolunteerTimeLog();
    }
    
/*this method is used for getting Volunteer Hours according to thier status
* and set thier value in VolunteerTimeLog for processing
*/
    public void setVolunteerTimeLog()
    {        
        contactFlag = true;
         contactDetails = new Contact();
         shift = new GW_Volunteers__Volunteer_Shift__c();
         setContactId.clear();
         mapContactVolHours.clear();
         // Get the shift id from the url
         strShiftId = ApexPages.currentPage().getParameters().get('shiftid');
         if( (strShiftId == null || strShiftId == '') )
         {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Input.');
            ApexPages.addMessage(myMsg);
         
         }
         else
         {
             try
             {
                 /***Retrive all the volunteer Hours for the particualr shift and stored in the map so that hours can be updated on sign out*****/
                 shift = [SELECT id,Name,GW_Volunteers__Number_of_Volunteers_Still_Needed__c,GW_Volunteers__Volunteer_Job__c,hh_Type__c,GW_Volunteers__Total_Volunteers__c,GW_Volunteers__Job_Location_Street__c,GW_Volunteers__Job_Location_City__c,GW_Volunteers__Duration__c,GW_Volunteers__Start_Date_Time__c  FROM GW_Volunteers__Volunteer_Shift__c WHERE id =: strShiftId LIMIT 1]; 
                 lstVolunteerTimeLog = new list<VolunteerTimeLog>();
                 lstNewVolunteertTimeLog = new list<VolunteerTimeLog>();
                 lstVolHours = new list<GW_Volunteers__Volunteer_Hours__c>();
                 lstInCompleteVolHours = new list<GW_Volunteers__Volunteer_Hours__c>();
                 String strConfirmed = 'Confirmed'; 
                 lstVolHours = [SELECT id,Name,GW_Volunteers__End_Date__c,hh_Family_Id__c,GW_Volunteers__Full_Name__c,GW_Volunteers__Contact__c,GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,hh_In_Time__c,GW_Volunteers__Volunteer_Shift__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,hh_Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c WHERE GW_Volunteers__Volunteer_Shift__c =: strShiftId  order by CreatedDate desc ];
                
                 if( lstVolHours != null && lstVolHours.size() > 0 )
                 {
                     
                     for( GW_Volunteers__Volunteer_Hours__c volunteerHour : lstVolHours )
                     {
                        if( volunteerHour.GW_Volunteers__Status__c == strConfirmed && volunteerHour.hh_In_Time__c == NULL ){
                            boolean LoggedIn = false;
                            if( volunteerHour.hh_In_Time__c != null )
                                LoggedIn = true;
                            if( setContactId.contains(volunteerHour.GW_Volunteers__Contact__c ) == false )
                            {
                                mapContactVolHours.put(volunteerHour.GW_Volunteers__Contact__c,volunteerHour);
                                setContactId.add(volunteerHour.GW_Volunteers__Contact__c);
                                lstVolunteerTimeLog.add( new VolunteerTimeLog(volunteerHour.hh_Volunteer_Name__c,volunteerHour.GW_Volunteers__Contact__c,volunteerHour,volunteerHour.hh_In_Time__c,LoggedIn,volunteerHour.GW_Volunteers__Status__c) );
                                
                            }
                        }
                     }
                    
                 //}
                 //if( lstVolHours != null && lstVolHours.size() > 0 )
                 //{
                     for( GW_Volunteers__Volunteer_Hours__c volunteerHour : lstVolHours )
                     {
                        if( volunteerHour.GW_Volunteers__Status__c == strConfirmed && volunteerHour.hh_In_Time__c != NULL ){
                            boolean LoggedIn = false;
                            if( volunteerHour.hh_In_Time__c != null )
                                LoggedIn = true;
                            if( setContactId.contains(volunteerHour.GW_Volunteers__Contact__c ) == false )
                            {
                                mapContactVolHours.put(volunteerHour.GW_Volunteers__Contact__c,volunteerHour);
                                setContactId.add(volunteerHour.GW_Volunteers__Contact__c);
                                lstNewVolunteertTimeLog.add( new VolunteerTimeLog(volunteerHour.hh_Volunteer_Name__c,volunteerHour.GW_Volunteers__Contact__c,volunteerHour,volunteerHour.hh_In_Time__c,LoggedIn,volunteerHour.GW_Volunteers__Status__c) );
                                system.debug(':: List OF InComplete Volunteer Time Log Hours :: '+lstNewVolunteertTimeLog);
                            }
                        }
                     }
                     for( GW_Volunteers__Volunteer_Hours__c volunteerHour : lstVolHours )
                     {
                        if ( volunteerHour.GW_Volunteers__Status__c == 'Completed'  )
                        {
                            boolean LoggedIn = false;
                            if( volunteerHour.hh_In_Time__c != null )
                                LoggedIn = true;
                            if( setContactId.contains(volunteerHour.GW_Volunteers__Contact__c ) == false )
                            {
                                mapContactVolHours.put(volunteerHour.GW_Volunteers__Contact__c,volunteerHour);
                                setContactId.add(volunteerHour.GW_Volunteers__Contact__c);
                                lstVolunteerTimeLog.add( new VolunteerTimeLog(volunteerHour.hh_Volunteer_Name__c,volunteerHour.GW_Volunteers__Contact__c,volunteerHour,volunteerHour.hh_In_Time__c,LoggedIn,volunteerHour.GW_Volunteers__Status__c) );
                                system.debug(':: List OF Fresh Volunteer Time Log Hours :: '+lstVolunteerTimeLog);
                            }
                        }
                        
                     }
                }
             }
             catch( Exception ex )
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Shift.'+ex);
                 ApexPages.addMessage(myMsg);
             }
            
         }
     }
     
 /** when volunteer sign in the In time field on volunteer hour is updated with the current time
  * this field is used for time calculation on sign out
  */
   public void SetInTime()
    {
        system.debug('In is clicked and volunteer Name is '+VolunteerNameValue);
        GW_Volunteers__Volunteer_Hours__c volunteerHour = new GW_Volunteers__Volunteer_Hours__c();
        GW_Volunteers__Volunteer_Hours__c oldVolunteerHour = new GW_Volunteers__Volunteer_Hours__c();
        GW_Volunteers__Volunteer_Hours__c newVolunteerHour;
        if( mapContactVolHours.containsKey( VolunteerNameValue ))
            {
                oldVolunteerHour = mapContactVolHours.get(VolunteerNameValue);
                oldVolunteerHour = [SELECT id,Name,GW_Volunteers__End_Date__c,hh_Family_Id__c,GW_Volunteers__Full_Name__c,GW_Volunteers__Contact__c,GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,hh_In_Time__c,GW_Volunteers__Volunteer_Shift__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,hh_Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c WHERE id =: oldVolunteerHour.Id];
            }
            
        if( oldVolunteerHour.GW_Volunteers__Status__c != 'Completed' ){
            system.debug(':::set in time');
            if( mapContactVolHours.containsKey( VolunteerNameValue ))
            {
               volunteerHour =  mapContactVolHours.get( VolunteerNameValue );
            }
            volunteerHour.hh_In_Time__c = datetime.now();
            volunteerHour.GW_Volunteers__Hours_Worked__c = 0;
            if( volunteerHour != null )
            {
                update volunteerHour;
            }
            boolean LoggedIn = false;
            if( volunteerHour.hh_In_Time__c != null )
                LoggedIn = true;
            lstNewVolunteertTimeLog.add( new VolunteerTimeLog(volunteerHour.hh_Volunteer_Name__c,volunteerHour.GW_Volunteers__Contact__c,volunteerHour,volunteerHour.hh_In_Time__c,LoggedIn,volunteerHour.GW_Volunteers__Status__c) );
                        
        }
        else 
        {   
            if( oldVolunteerHour != null ){
                newVolunteerHour = new GW_Volunteers__Volunteer_Hours__c();
                newVolunteerHour.GW_Volunteers__Status__c = 'Confirmed';
                newVolunteerHour.GW_Volunteers__Volunteer_Shift__c = oldVolunteerHour.GW_Volunteers__Volunteer_Shift__c;
                newVolunteerHour.GW_Volunteers__Contact__c = oldVolunteerHour.GW_Volunteers__Contact__c;
                newVolunteerHour.GW_Volunteers__Hours_Worked__c = 0;
                newVolunteerHour.hh_In_Time__c = datetime.now();
                newVolunteerHour.GW_Volunteers__Number_of_Volunteers__c = oldVolunteerHour.GW_Volunteers__Number_of_Volunteers__c;
                newVolunteerHour.GW_Volunteers__Volunteer_Job__c = oldVolunteerHour.GW_Volunteers__Volunteer_Job__c;
                newVolunteerHour.GW_Volunteers__Start_Date__c = oldVolunteerHour.GW_Volunteers__Start_Date__c;
                insert newVolunteerHour;
                setContactId.remove(oldVolunteerHour.GW_Volunteers__Contact__c );
            }
            list<GW_Volunteers__Volunteer_Hours__c > lstNewVolHours = [SELECT id,Name,GW_Volunteers__End_Date__c,hh_Family_Id__c,GW_Volunteers__Full_Name__c,GW_Volunteers__Contact__c,GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,hh_In_Time__c,GW_Volunteers__Volunteer_Shift__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,hh_Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c Where Id =: newVolunteerHour.Id order by CreatedDate desc ];
            if( lstNewVolHours != null && lstNewVolHours.size() > 0 )
            {
                for( GW_Volunteers__Volunteer_Hours__c volunteerHours : lstNewVolHours )
                {
                    boolean LoggedIn = false;
                    if( volunteerHours.hh_In_Time__c != null )
                        LoggedIn = true;
                    //if( setContactId.contains(volunteerHours.GW_Volunteers__Contact__c ) == false )
                    {
                        mapContactVolHours.put(volunteerHours.GW_Volunteers__Contact__c,volunteerHours);
                        setContactId.add(volunteerHours.GW_Volunteers__Contact__c);
                        lstNewVolunteertTimeLog.add( new VolunteerTimeLog(volunteerHours.hh_Volunteer_Name__c,volunteerHours.GW_Volunteers__Contact__c,volunteerHours,volunteerHours.hh_In_Time__c,LoggedIn,volunteerHours.GW_Volunteers__Status__c) );
                    }
                }
                
            }
            
            List<VolunteerTimeLog> lstnewLog = new List<VolunteerTimeLog>();
            for(VolunteerTimeLog log : lstNewVolunteertTimeLog)
            {
                if(log.VolunteerHour.Id != oldVolunteerHour.id)
                {
                    lstnewLog.add(log);
                }
            }
            lstNewVolunteertTimeLog = new List<VolunteerTimeLog>();
            for(VolunteerTimeLog log : lstnewLog){
                lstNewVolunteertTimeLog.add(log);
            }
        }
         
    }
    
    
    /*When sign out is clicked update the volunteer hour with the elapsed time value
    * this method is called from the page and use the value of contact id in an volunteer hour
    * mapped previously and make new Object with same Volunteer Hours information and show it on VisualForce Page.
    */
  
   public  void UpdateOutTime()
    {
        system.debug('==>SignOutTime==>');
        DateTime logInTime;
        GW_Volunteers__Volunteer_Hours__c objVolunteerHour;
        GW_Volunteers__Volunteer_Hours__c volunteerHour = new GW_Volunteers__Volunteer_Hours__c();
        timeIntervalSetting = Volunteer_RoleCall_Custom_Settings__c.getValues('TimeInterval');
        if( mapContactVolHours.containsKey( VolunteerNameValue ))
        {
            volunteerHour =  mapContactVolHours.get( VolunteerNameValue );
        }
        if( volunteerHour.hh_In_Time__c != null )
        {
            logInTime = volunteerHour.hh_In_Time__c;
            volunteerHour.hh_Out_Time__c = DateTime.now();
            Double HoursWorked = Math.roundToLong((DateTime.now().getTime() - logInTime.getTime())/ (60.0*1000.0));  //Math.Floor((DateTime.now().getTime() - logInTime.getTime())/ (1000.0*60.0*60.0));
            if( HoursWorked != null && timeIntervalSetting.SignOutTime__c <= HoursWorked ){
                system.debug('==>==>In Update Out Time');
                Double volunteerHoursWorked =  HoursWorked /60.0;
                if(  volunteerHour.GW_Volunteers__Hours_Worked__c == null )
                    volunteerHour.GW_Volunteers__Hours_Worked__c = 0;
                volunteerHour.GW_Volunteers__Hours_Worked__c =  volunteerHour.GW_Volunteers__Hours_Worked__c + volunteerHoursWorked;
                volunteerHour.GW_Volunteers__Status__c = 'Completed';
            }
            else
            {
                volunteerHour.hh_In_Time__c = null;
                volunteerHour.hh_Out_Time__c = null;
            }
          if( volunteerHour != null )
              update volunteerHour;
                
        }
     
    }
/*This method is used for SignUp.When a new volunteer fill his firstname,lastname and emailand click on SignUp
*This method is called and search for contact,if contact does not exist,it creates a new contact and a new volunteer hour and show this VolunteerHour in out section
*/ 
/*
*When SignUp Button is clicked then this method will check the existing contact with their Email,FirstName and LastName.
*This method is called form the page and use the contact id and email.
*check the contact id,if not exist in Out Section then mapped,and make a new Volunteer Hours and show it on Out Section if exist and donot in in or out section,will make a new VolunteerHour.
*/ 
    public void SignUp()
    { 
        contactFlag = true;
        string strPetName,query;
        try{
            list<GW_Volunteers__Volunteer_Shift__c> listShift = [select GW_Volunteers__Number_of_Volunteers_Still_Needed__c, GW_Volunteers__Start_Date_Time__c from GW_Volunteers__Volunteer_Shift__c where Id = :strShiftId];
            shift = [SELECT id,Name,GW_Volunteers__Number_of_Volunteers_Still_Needed__c,GW_Volunteers__Volunteer_Job__c,hh_Type__c,GW_Volunteers__Total_Volunteers__c,GW_Volunteers__Job_Location_Street__c,GW_Volunteers__Job_Location_City__c,GW_Volunteers__Duration__c,GW_Volunteers__Start_Date_Time__c  FROM GW_Volunteers__Volunteer_Shift__c WHERE id =: strShiftId LIMIT 1]; 
            timeIntervalSetting = Volunteer_RoleCall_Custom_Settings__c.getValues('ContactPetName');
             if(timeIntervalSetting != null)
            {
                strPetName = timeIntervalSetting.NickName__c;
                query = 'SELECT Id, FirstName,LastName,Email,' + strPetName + ' FROM Contact where Email <>  NULL limit 50000';
            }
            else
            {
                query = 'Select id,FirstName,LastName,Email from Contact where Email <>  NULL limit 50000';
            }
            
            list<Contact> lstContact = new list<Contact>();
            lstContact = (list<Contact>)Database.query(query);
            system.debug('::==>Query value'+lstContact);
            Contact newVolunteerContact;
            GW_Volunteers__Volunteer_Hours__c signinVolunteerHour;
            GW_Volunteers__Volunteer_Hours__c contactExistVolHour;
            
            
            boolean flag = true;
            //list<RecordType> recordTypeList = [select Id, Name, SobjectType FROM RecordType where SobjectType='Contact' AND Name =: 'Contact']; // Fetch all RecordType on sObject Account
            
            if (listShift != null && listShift.size() > 0) {
                if (vhours.GW_Volunteers__Number_of_Volunteers__c > listShift[0].GW_Volunteers__Number_of_Volunteers_Still_Needed__c) {
                    string strError = String.Format('Too many volunteers for Shift',new string[] { string.valueOf(listShift[0].GW_Volunteers__Number_of_Volunteers_Still_Needed__c) }); 
                    throw (new MyException(strError));                  
                }
            }
            if( lstContact != null )
            {
                for(Contact conDetail : lstContact )
                {
                    if( contactDetails.Email == conDetail.Email &&  ( contactDetails.FirstName == conDetail.FirstName || (strPetName != null && contactDetails.FirstName == (string)conDetail.get(strPetName)) )&& contactDetails.LastName == conDetail.LastName )
                    {
                        system.debug('::In contact match Block3::');
                        flag = false;
                        if( !setContactId.contains( conDetail.id ) ){
                            setContactId.add(conDetail.id);
                            contactExistVolHour  = new GW_Volunteers__Volunteer_Hours__c();
                            contactExistVolHour.GW_Volunteers__Status__c = 'Confirmed';
                            contactExistVolHour.GW_Volunteers__Volunteer_Shift__c = strShiftId;
                            contactExistVolHour.GW_Volunteers__Contact__c =  conDetail.id;
                            contactExistVolHour.GW_Volunteers__Hours_Worked__c = 0;
                            contactExistVolHour.GW_Volunteers__Number_of_Volunteers__c = 1;
                            contactExistVolHour.GW_Volunteers__Volunteer_Job__c = shift.GW_Volunteers__Volunteer_Job__c;
                            contactExistVolHour.GW_Volunteers__Start_Date__c = Date.today();
                            insert contactExistVolHour;
                        }
                        else
                        {contactFlag = false;}
                        
                    }
                }
                
                if( flag )
                {
                    flag = true;
                    newVolunteerContact = new Contact();
                    //newVolunteerContact.recordTypeId = recordTypeList[0].id;
                    newVolunteerContact.FirstName = contactDetails.FirstName;
                    newVolunteerContact.LastName = contactDetails.LastName;
                    newVolunteerContact.Email = contactDetails.Email;
                    insert newVolunteerContact;
                    signinVolunteerHour  = new GW_Volunteers__Volunteer_Hours__c();
                    signinVolunteerHour.GW_Volunteers__Status__c = 'Confirmed';
                    signinVolunteerHour.GW_Volunteers__Volunteer_Shift__c = strShiftId;
                    signinVolunteerHour.GW_Volunteers__Contact__c = newVolunteerContact.Id;
                    signinVolunteerHour.GW_Volunteers__Hours_Worked__c = 0;
                    signinVolunteerHour.GW_Volunteers__Number_of_Volunteers__c = 1;
                    signinVolunteerHour.hh_In_Time__c = datetime.now();
                    signinVolunteerHour.GW_Volunteers__Volunteer_Job__c = shift.GW_Volunteers__Volunteer_Job__c;
                    signinVolunteerHour.GW_Volunteers__Start_Date__c = Date.today();
                    insert signinVolunteerHour;
                }
            }
            set<GW_Volunteers__Volunteer_Hours__c> setVolHourId = new set<GW_Volunteers__Volunteer_Hours__c>();
            setVolHourId.add(signinVolunteerHour); 
            setVolHourId.add(contactExistVolHour);
            list<GW_Volunteers__Volunteer_Hours__c > lstSignInVolHours = [SELECT id,Name,GW_Volunteers__End_Date__c,hh_Family_Id__c,GW_Volunteers__Full_Name__c,GW_Volunteers__Contact__c,GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,hh_In_Time__c,GW_Volunteers__Volunteer_Shift__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,hh_Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c Where id IN : setVolHourId order by CreatedDate desc ];
            if( lstSignInVolHours != null && lstSignInVolHours.size() > 0 )
            {
                for( GW_Volunteers__Volunteer_Hours__c volunteerHours : lstSignInVolHours )
                {
                    boolean LoggedIn = false;
                    if( volunteerHours.hh_In_Time__c != null )
                        LoggedIn = true;
                    if( setContactId.contains(volunteerHours.GW_Volunteers__Contact__c ) == false )
                    {
                        mapContactVolHours.put(volunteerHours.GW_Volunteers__Contact__c,volunteerHours);
                        setContactId.add(volunteerHours.GW_Volunteers__Contact__c);
                        lstNewVolunteertTimeLog.add( new VolunteerTimeLog(volunteerHours.hh_Volunteer_Name__c,volunteerHours.GW_Volunteers__Contact__c,volunteerHours,volunteerHours.hh_In_Time__c,LoggedIn,volunteerHours.GW_Volunteers__Status__c) );
                        system.debug('==>==>list after sign up==>==>'+lstVolunteerTimeLog);
                    }
                }
            } 
            lstSignInVolHours.clear();
        }
        catch (exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));         
        }
        //PageReference reRend = new PageReference('/apex/RollCall?shiftid='+strShiftId);
        //reRend.setRedirect(true);
        //return null;
    }
    
    /**** class is used to store the necessary information**************/
    class VolunteerTimeLog
    {
        public string VolunteerName { get; set; }
        public string VolunteerContactId { get; set; }
        public GW_Volunteers__Volunteer_Hours__c VolunteerHour { get; set; }
        public DateTime InTime { get; set; }
        public boolean LoggedIn { get; set; }
        public string VolHourStatus { get;set;}
        public VolunteerTimeLog( string VolunteerName,string VolunteerContactId,GW_Volunteers__Volunteer_Hours__c VolunteerHour,DateTime InTime,boolean LoggedIn,string VolHourStatus)
        {
            this.VolunteerName = VolunteerName;
            this.VolunteerContactId = VolunteerContactId;
            this.VolunteerHour = VolunteerHour; 
            this.InTime = InTime;
            this.LoggedIn = LoggedIn;
            this.VolHourStatus =  VolHourStatus;      
        }
    
    }
    private class MyException extends Exception {}
    
}