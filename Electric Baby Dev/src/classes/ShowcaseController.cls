public with sharing class ShowcaseController
{
   @RemoteAction
   public static List<Showcase__c> getData()
   {
        List<Showcase__c> lstShowcase = new List<Showcase__c>();
        lstShowcase = Database.query('select Name,URL__c,Description__c from Showcase__c ORDER BY Name');
        return lstShowcase;
   }

   @RemoteAction
   public static string demoMethod(){
     return 'Success';
   }
}