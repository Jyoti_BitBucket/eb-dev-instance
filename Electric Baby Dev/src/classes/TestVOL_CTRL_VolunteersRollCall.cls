/*
 * This class contains unit tests for validating the behavior of Apex classe.
 *
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class TestVOL_CTRL_VolunteersRollCall
{
    public static testMethod void TestVolunteersRollCall()
    {
        Profile SysAdProfileId = [SELECT id FROM Profile
                             WHERE name = 'System Administrator'];
       
        User newUser = new User
             (username='volunteer@test.com',
             lastName='myLastName', profileId=SysAdProfileId.id,
             email='foobar@a.com', alias='testUser',
             timeZoneSidKey='America/Denver',
             localeSidKey='en_CA', emailEncodingKey='UTF-8',
             languageLocaleKey='en_US');
        insert newUser;
        
        Volunteer_Roll_Call_Settings__c timeIntervalSetting = new Volunteer_Roll_Call_Settings__c();
        timeIntervalSetting.Refresh_Interval__c = 2;
        timeIntervalSetting.Hours_Worked_Time_Interval__c = 2;
        timeIntervalSetting.SetupOwnerId =  newUser.id;
        insert timeIntervalSetting;
        system.runas(newUser)
        {
            Boolean contactFlag = true;
            /////////////////////////////////
            
             //Create new campaign
            Campaign objCampaignA = new Campaign(Name='VolunteerTestCampaign',IsActive=True);
            insert objCampaignA;
            system.assertNotEquals(objCampaignA.Id,Null);
            
             // Creating record of VolunteerJob (GW_Volunteers__Volunteer_Job__c )   
            GW_Volunteers__Volunteer_Job__c objVolunteerJobA = new GW_Volunteers__Volunteer_Job__c ();
            objVolunteerJobA.Name = 'TestJob1';
            objVolunteerJobA.GW_Volunteers__Campaign__c = objCampaignA.id;
            insert objVolunteerJobA ;
            system.assertNotEquals(objVolunteerJobA.Id,Null);
            
            // Creating record of VolunteerShift (GW_Volunteers__Volunteer_Shift__c)
            GW_Volunteers__Volunteer_Shift__c objVolunteerShiftA = new GW_Volunteers__Volunteer_Shift__c ();
            objVolunteerShiftA.GW_Volunteers__Volunteer_Job__c = objVolunteerJobA.id;
            objVolunteerShiftA.GW_Volunteers__Start_Date_Time__c = date.today();
            objVolunteerShiftA.GW_Volunteers__Duration__c = 3;
            insert objVolunteerShiftA;
            system.assertNotEquals(objVolunteerShiftA.Id,Null);

            // first contact signing up
            
            //Getting current page shift id from volunteershift
            ApexPages.currentPage().getParameters().put('shiftid',objVolunteerShiftA.id);
            VOL_CTRL_VolunteersRollCall objVolunteerRollCallA = new VOL_CTRL_VolunteersRollCall();
           // objVolunteerRollCallA.VolunteerNameValue = objVolunteerHour1.GW_Volunteers__Contact__c;
            objVolunteerRollCallA.volunteerHourContact.Email = 'test31@volunteertest3.com';
            objVolunteerRollCallA.volunteerHourContact.FirstName = 'TestVolunteer31';
            objVolunteerRollCallA.volunteerHourContact.LastName = 'TestVolunteer31';
            objVolunteerRollCallA.SignUp();
            
            
            // when there is contact in SF but new contact is signing up
            
             //Create new Contact
            Contact objContactA = new Contact( FirstName = 'TestVolunteer',LastName = 'TestVolunteer',Email = 'testA@volunteertest.com');//,recordTypeId = recordTypeList[0].id  );
            insert objContactA;
            system.assertNotEquals(objContactA.Id,Null);
            
            
            
            ApexPages.currentPage().getParameters().put('shiftid',objVolunteerShiftA.id);
            objVolunteerRollCallA = new VOL_CTRL_VolunteersRollCall();
            objVolunteerRollCallA.volunteerHourContact.Email = 'test31A@volunteertest3.com';
            objVolunteerRollCallA.volunteerHourContact.FirstName = 'TestVolunteer31';
            objVolunteerRollCallA.volunteerHourContact.LastName = 'TestVolunteer31';
            objVolunteerRollCallA.SignUp();
            
            // when hour is completed then setting the in time
            
              //***** Creating record of VolunteerHour,when status is confirmed and SignInTime  null  
            GW_Volunteers__Volunteer_Hours__c objVolunteerHour = new GW_Volunteers__Volunteer_Hours__c ();
            objVolunteerHour.GW_Volunteers__Volunteer_Job__c = objVolunteerJobA.id;
            objVolunteerHour.GW_Volunteers__Volunteer_Shift__c = objVolunteerShiftA.id;
            objVolunteerHour.GW_Volunteers__Status__c = 'Completed';
            objVolunteerHour.GW_Volunteers__Contact__c = objContactA.id;
            objVolunteerHour.GW_Volunteers__Hours_Worked__c = 1;
            objVolunteerHour.GW_Volunteers__Number_of_Volunteers__c = 1;
            objVolunteerHour.In_Time__c = system.now().addMinutes(-20) ;
            objVolunteerHour.GW_Volunteers__Start_Date__c = Date.today()+2;
            insert objVolunteerHour;
            system.assertNotEquals(objVolunteerHour.Id,Null);
            
            
            ApexPages.currentPage().getParameters().put('shiftid',objVolunteerShiftA.id);
            objVolunteerRollCallA = new VOL_CTRL_VolunteersRollCall();
            objVolunteerRollCallA.VolunteerNameValue = objVolunteerHour.GW_Volunteers__Contact__c;
            objVolunteerRollCallA.SetInTime();
            
            
            
            
            // for outitme
            
            objVolunteerHour.GW_Volunteers__Volunteer_Job__c = objVolunteerJobA.id;
            objVolunteerHour.GW_Volunteers__Volunteer_Shift__c = objVolunteerShiftA.id;
            objVolunteerHour.GW_Volunteers__Status__c = 'Completed';
            objVolunteerHour.GW_Volunteers__Contact__c = objContactA.id;
            objVolunteerHour.GW_Volunteers__Hours_Worked__c = 1;
            objVolunteerHour.GW_Volunteers__Number_of_Volunteers__c = 1;
            objVolunteerHour.In_Time__c = system.now().addMinutes(-20) ;
            objVolunteerHour.GW_Volunteers__Start_Date__c = Date.today()+2;
            update objVolunteerHour;
            
            
            
            ApexPages.currentPage().getParameters().put('shiftid',objVolunteerShiftA.id);
            objVolunteerRollCallA = new VOL_CTRL_VolunteersRollCall();
            objVolunteerRollCallA.VolunteerNameValue = objVolunteerHour.GW_Volunteers__Contact__c;
            objVolunteerRollCallA.UpdateOutTime();
        
            
            
            /////////////////////
            //Create new Contact
            Contact objContact = new Contact( FirstName = 'TestVolunteer',LastName = 'TestVolunteer',Email = 'test@volunteertest.com');//,recordTypeId = recordTypeList[0].id  );
            insert objContact;
            system.assertNotEquals(objContact.Id,Null);
            
            //Create new Contact
            Contact objContact1 = new Contact( FirstName = 'TestVolunteer1',LastName = 'TestVolunteer1',Email = 'test1@volunteertest1.com');//,recordTypeId = recordTypeList[0].id  );
            insert objContact1;
            system.assertNotEquals(objContact1.Id,Null);
           
            //Create new Contact
            Contact objContact2 = new Contact( FirstName = 'TestVolunteer2',LastName = 'TestVolunteer2',Email = 'test2@volunteertest2.com'  );
            insert objContact2;
            system.assertNotEquals(objContact2.Id,Null);
            
            //***** list of contact
            list<contact> listOfContact = new list<contact>();
            listOfContact.add(objContact);
            listOfContact.add(objContact1);
            listOfContact.add(objContact2);

            system.assertNotEquals(listOfContact,Null);
            system.assertNotEquals(listOfContact.size(),0);
        
            //Create new campaign
            Campaign objCampaign = new Campaign(Name='VolunteerTestCampaign',IsActive=True);
            insert objCampaign ;
            system.assertNotEquals(objCampaign.Id,Null);
            
            // Creating record of VolunteerJob (GW_Volunteers__Volunteer_Job__c )   
            GW_Volunteers__Volunteer_Job__c objVolunteerJob = new GW_Volunteers__Volunteer_Job__c ();
            objVolunteerJob.Name = 'TestJob1';
            objVolunteerJob.GW_Volunteers__Campaign__c = objCampaign.id;
            insert objVolunteerJob ;
            system.assertNotEquals(objVolunteerJob.Id,Null);
            
            // Creating record of VolunteerShift (GW_Volunteers__Volunteer_Shift__c)
            GW_Volunteers__Volunteer_Shift__c objVolunteerShift = new GW_Volunteers__Volunteer_Shift__c ();
            objVolunteerShift.GW_Volunteers__Volunteer_Job__c = objVolunteerJob.id;
            objVolunteerShift.GW_Volunteers__Start_Date_Time__c = date.today();
            objVolunteerShift.GW_Volunteers__Duration__c = 3;
            insert objVolunteerShift;
            system.assertNotEquals(objVolunteerShift.Id,Null);
            
           
             //***** Creating record of VolunteerHour,when status is confirmed  and signInTime is not null
            GW_Volunteers__Volunteer_Hours__c objVolunteerHour1 = new GW_Volunteers__Volunteer_Hours__c ();
            objVolunteerHour1.GW_Volunteers__Volunteer_Job__c = objVolunteerJob.id;
            objVolunteerHour1.GW_Volunteers__Volunteer_Shift__c = objVolunteerShift.id;
            objVolunteerHour1.GW_Volunteers__Status__c = 'Confirmed';
            objVolunteerHour1.GW_Volunteers__Contact__c = objContact1.id;
            objVolunteerHour1.GW_Volunteers__Start_Date__c = Date.today();
            objVolunteerHour1.In_Time__c = DateTime.now().addMinutes(-50);
            insert objVolunteerHour1;
            system.assertNotEquals(objVolunteerHour1.Id,Null);
            
            
            //Getting current page shift id from volunteershift
            ApexPages.currentPage().getParameters().put('shiftid',objVolunteerShift.id);
            VOL_CTRL_VolunteersRollCall objVolunteerRollCall = new VOL_CTRL_VolunteersRollCall();
            objVolunteerRollCall.VolunteerNameValue = objVolunteerHour1.GW_Volunteers__Contact__c;
            objVolunteerRollCall.volunteerHourContact.Email = 'test31@volunteertest3.com';
            objVolunteerRollCall.volunteerHourContact.FirstName = 'TestVolunteer31';
            objVolunteerRollCall.volunteerHourContact.LastName = 'TestVolunteer31';
            objVolunteerRollCall.SetInTime();
            objVolunteerRollCall.UpdateOutTime();
            objVolunteerRollCall.setContactId.clear();
            objVolunteerRollCall.SignUp();
        
        }
        
    }
}