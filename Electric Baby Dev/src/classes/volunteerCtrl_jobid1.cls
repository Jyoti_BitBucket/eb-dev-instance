public class volunteerCtrl_jobid1 {

    public string strJobId { get; set; }
    public Contact volunteerHourContact {get;set;}
    public list<GW_Volunteers__Volunteer_Hours__c> lstVolunteerHours { get; set; }
    public GW_Volunteers__Volunteer_Shift__c objVolunteerShift { get; set; }
   	public string VolunteerNameValue { get; set; }
    map<string,GW_Volunteers__Volunteer_Hours__c> mapVolunteerHoursContact = new map<string,GW_Volunteers__Volunteer_Hours__c> ();
    public boolean contactFlag { get;set;}
	public set<id> setContactId {get; set;}    
    String strVolunteerName;
    public list<Contact> lstContact{get;set;}
    Set<ID> shiftID = new Set<ID>();
    
    
    public volunteerCtrl_jobid1(){
        lstVolunteerHours = new list<GW_Volunteers__Volunteer_Hours__c>();
         strJobId = ApexPages.currentPage().getParameters().get('jobid');
         objVolunteerShift = new GW_Volunteers__Volunteer_Shift__c();
         
         if( (strJobId == null || strJobId == '') )
         {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Input.');
            ApexPages.addMessage(myMsg);
         }
        else
        {
           try
             {
                 /***Retrive all the volunteer Hours for the particualr shift and stored in the map so that hours can be updated on sign out*****/
                list<GW_Volunteers__Volunteer_Shift__c> lstTempShift = new list<GW_Volunteers__Volunteer_Shift__c>();
                lstTempShift  = [SELECT id,Name,GW_Volunteers__Number_of_Volunteers_Still_Needed__c,GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Total_Volunteers__c,GW_Volunteers__Desired_Number_of_Volunteers__c FROM GW_Volunteers__Volunteer_Shift__c WHERE hh_Job_Id__c =: strJobId];
                
                 for(GW_Volunteers__Volunteer_Shift__c lstID : lstTempShift)
   				 {
        			if(!shiftID.contains(lstID.ID))
       				 {
         			    shiftID.add(lstID.ID);           
      				 }
   				 }
                 
               
                 lstVolunteerHours = [SELECT id,Name,GW_Volunteers__End_Date__c,GW_Volunteers__Full_Name__c,GW_Volunteers__Contact__c,
                 GW_Volunteers__Hours_Worked__c,GW_Volunteers__Shift_Start_Date_Time__c,In_Time__c,GW_Volunteers__Volunteer_Shift__c,
                 GW_Volunteers__Volunteer_Job__c,GW_Volunteers__Start_Date__c,GW_Volunteers__Number_of_Volunteers__c,GW_Volunteers__Status__c,
                 GW_Volunteers__Contact__r.FirstName,GW_Volunteers__Contact__r.LastName,
                 Volunteer_Name__c FROM GW_Volunteers__Volunteer_Hours__c WHERE GW_Volunteers__Volunteer_Shift__c IN: shiftID ];
			}
             catch( Exception ex )
             {
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Invalid Shift.'+ex);
                 ApexPages.addMessage(myMsg);
             }
        }
    }
}