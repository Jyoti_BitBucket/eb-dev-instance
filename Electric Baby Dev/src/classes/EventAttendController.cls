public with sharing class EventAttendController{
    
    public GW_Volunteers__Volunteer_Shift__c shift {get; private set;} 
	public GW_Volunteers__Volunteer_Shift__c objShift;
	public ApexPages.StandardController stdController;
	
	public List<GW_Volunteers__Volunteer_Hours__c> lstVolunteersRegistration {get;set;}  // List of Volunteers hours
	public List<AffiliatesDetails> lstRegisterdVolunteers { get; set;}  // List of Resgistred volunteers to display
	public Set<id> setVolunteersId {get; set;}  // Set of Affiliate IDs
	public Volunteer_Roll_Call_Settings__c timeIntervalSetting{get;set;}  // Custom settings peratining to shift Attendance
	public String refreshTimeInterval{get;set;}  // Custom Setting for refresh interval 
	public String strJobName{get;set;}  // Volunteer Job Name
	public String strJobID{get;set;}  // volunteer Job ID   
	public String strShiftName{get;set;}  // Shift Name    
	public String errorMessage{get;set;}  // Error Message 
	public String successMessage{get;set;}  // Success Message
	public String volunteerName;  // Affiliate Name
	public Id affiliateIdToUpdate{get; set;}    // Get Affiliate id to update occassion Attendance status 
	public Date Today { get { return Date.today(); }}
	public Boolean b_searchFlag{get;set;}
	public Boolean b_IsBeforeAfterEvent{get;set;}
	public String strStartDateTime{get;set;}
	public String strEmailId{get;set;} 
    
    
    
    
    /*
	* Page Constructor
	*/	
	public EventAttendController(ApexPages.StandardController stdController) {

		Id shiftId = stdController.getId();
		b_IsBeforeAfterEvent = TRUE;
		try {
			this.shift = [SELECT Id, Name,GW_Volunteers__Start_Date_Time__c, GW_Volunteers__Volunteer_Job__r.Name,
						 Status__c
						FROM 
						GW_Volunteers__Volunteer_Shift__c 
						WHERE 
						id = :shiftId];
	  
			strStartDateTime = shift.GW_Volunteers__Start_Date_Time__c.format();
			strShiftName = shift.Name;
			datetime dT = shift.GW_Volunteers__Start_Date_Time__c;
			date dt_ShiftStartDate = date.newinstance(dT.year(), dT.month(), dT.day());

			if(shift.Status__c != 'Open') {
				// If the Occasion is not Open
				b_IsBeforeAfterEvent = FALSE;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
					'This shift is already full.')); 

			} else if(dt_ShiftStartDate > Date.today()) { 
				// If the Occasion is before today
				b_IsBeforeAfterEvent = FALSE;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
					'You are early. This shift is not yet started.')); 

			} else if(dt_ShiftStartDate < Date.today()) {
				b_IsBeforeAfterEvent = FALSE;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
					'This page cannot be used after the event has been completed.'));

			} else {
				lstRegisterdVolunteers = new list<AffiliatesDetails>();
				setVolunteersId = new set<id>();
				setVolunteersList();
				b_searchFlag=false;
			}
		
		} catch(Exception ex) { 
			system.debug('Error Occurred');           
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
				'UnExpected Error : ' + ex.getMessage()));
		}
	}
    
    
    
    
    
    /*
	* Method to initialize lstRegisterdAffiliates of affiliate details for 
	* Platoon Events with Registrations affiliates 
	*/
	public void setVolunteersList() {
		// Set refresh interval from custom setting
		timeIntervalSetting = Volunteer_Roll_Call_Settings__c.getInstance();
		lstRegisterdVolunteers = new list<AffiliatesDetails>();
		setVolunteersId = new set<id>();
		if( timeIntervalSetting != null && timeIntervalSetting.Refresh_Interval__c != null && timeIntervalSetting.Hours_Worked_Time_Interval__c != null) {
			refreshTimeInterval = string.valueOf(timeIntervalSetting.Refresh_Interval__c*60000);  
		} else {
			refreshTimeInterval =  string.valueOf(60*60000);//default refresh time
		}
 
	   
		// Query all the volunteer hours for the shift
		 objShift = new GW_Volunteers__Volunteer_Shift__c();
		try
		{
			lstVolunteersRegistration = [SELECT id,Name,In_Time__c,Out_Time__c,GW_Volunteers__Hours_Worked__c,
										GW_Volunteers__Contact__c,GW_Volunteers__Contact__r.Name 
										FROM 
										GW_Volunteers__Volunteer_Hours__c 
										WHERE 
										GW_Volunteers__Volunteer_Shift__c =: shift.Id];
			system.debug('::lstRegistration ::'+lstVolunteersRegistration );
			// Query all Occasion details              
			objShift = [SELECT id, Name, GW_Volunteers__Volunteer_Job__c, GW_Volunteers__Volunteer_Job__r.Name, 
						GW_Volunteers__Start_Date_Time__c, End_Time__c, Event_End_Time__c  
						FROM 
						GW_Volunteers__Volunteer_Shift__c 
						WHERE 
						id =: shift.Id 
						LIMIT 1];
			strJobName = objShift.GW_Volunteers__Volunteer_Job__r.Name;
			strJobID = objShift.GW_Volunteers__Volunteer_Job__c;
			strShiftName = objShift.Name;
		 }
		 
		catch(DMLException ex)
		{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,
				'Platoon Event has not been specified. Please access check ins from the Occasion Record. ');
			ApexPages.addMessage(myMsg);
		}
			

		if(lstvolunteersRegistration != null && !lstvolunteersRegistration.isEmpty()) {    
			
			for(GW_Volunteers__Volunteer_Hours__c hour : lstVolunteersRegistration ) {
				lstRegisterdVolunteers.add(new AffiliatesDetails(hour));
				if(!setVolunteersId.contains(hour.GW_Volunteers__Contact__c)) {
					setVolunteersId.add(hour.GW_Volunteers__Contact__c); 
				}
			}
		}       
		
		lstRegisterdVolunteers.sort();      
	}
	
	
	
	/**
	* This method is called when the page loads
	* - This will check the whether the date of the event is before today
	*	and redirect the user if necessary.
	*/  
	public pagereference init() {
		datetime dT = shift.GW_Volunteers__Start_Date_Time__c;
		date dt_ShiftStartDate = date.newinstance(dT.year(), dT.month(), dT.day());
		
		if ( dt_ShiftStartDate < Date.Today()) {
			// If the event is before TODAY, then redirect the user to the
			// PlatoonEventAttendance page to handle post event entry
			/*PageReference pg = Page.PlatoonEventAttendance;
			pg.getParameters().put('id', occasion.Id);
			pg.setRedirect(true);
			return pg;
			*/
			return null;
		}
		return null;    
	}
	
	
	
	/*
	* This UpdateAffiliate() method to update affiliates InTime for occasion 
	* and to update their hours contribution for particular occasion.
	*/
	public void UpdateVolunteer() {
		List<GW_Volunteers__Volunteer_Hours__c> registerdVounteers = new List<GW_Volunteers__Volunteer_Hours__c>();
		GW_Volunteers__Volunteer_Hours__c objRegisteredVolunteer = new GW_Volunteers__Volunteer_Hours__c();
				system.debug('#0 objRegistrAffiliate.In_time__c=' +objRegisteredVolunteer.In_time__c);
		timeIntervalSetting = Volunteer_Roll_Call_Settings__c.getInstance();
		Boolean b_CheckInterval = true;
		String style;  // Used to set initial style for Registered Affiliate in interface
		String status;  // Used to set initial status for Registered Affiliate in interface

		// Registered Affiliate to update occasion attended status
		objRegisteredVolunteer = [SELECT 
								GW_Volunteers__Contact__c,GW_Volunteers__Contact__r.Name,
								GW_Volunteers__Hours_Worked__c,In_time__c,Out_time__c 
								FROM GW_Volunteers__Volunteer_Hours__c 
								WHERE id =: affiliateIdToUpdate 
								AND GW_Volunteers__Volunteer_Shift__c =: shift.id LIMIT 1]; 

		//TMC_PlatoonEventServices.updateEventEndTime(this.occasion);

		System.debug('registerdVounteers:test:'+registerdVounteers);
		if( registerdVounteers != null && registerdVounteers.size() > 0 ) {
				
			objRegisteredVolunteer = registerdVounteers[0];
			if(objRegisteredVolunteer.In_time__c != null) {  // Sign-out Registrant
			 
				objRegisteredVolunteer.Out_time__c = DateTime.now();  // Set Out Time to now

				// M.Smith, 12/06/2015:
				// If the current date/time is AFTER the event has ended,
				// use the Event End Time instead
			 	if (this.shift.End_Time__c != null 
			 			&& objRegisteredVolunteer.Out_time__c > this.shift.End_Time__c) {
			 		objRegisteredVolunteer.Out_time__c = this.shift.End_Time__c;
			 	}
				
				// Calculate the difference between the In Time an Out Time in hours
				Double l_affiliateInTime = (objRegisteredVolunteer.In_time__c).getTime();
				Double milliseconds = (DateTime.now().getTime()) - l_affiliateInTime;
				Double hours = (milliseconds / 1000)/(60*60);
				system.debug(':hours:'+hours+':milliseconds:'+milliseconds+':seconds:'+ (milliseconds/1000));

				// Set blank hours to 0
				if( objRegisteredVolunteer.GW_Volunteers__Hours_Worked__c == null ) {
					objRegisteredVolunteer.GW_Volunteers__Hours_Worked__c = 0; 
				}
				
				if(hours != null && hours > 0) {  // Have hours to increment Hours_Countributed if longer than 1/2 the Hours Worked Time Interval
					if((milliseconds / 1000) >= (timeIntervalSetting.Hours_Worked_Time_Interval__c*60/2)) {
						objRegisteredVolunteer.GW_Volunteers__Hours_Worked__c  =  objRegisteredVolunteer.GW_Volunteers__Hours_Worked__c + hours;
					}
				}

				objRegisteredVolunteer.In_time__c = null;  // Clear InTime
				objRegisteredVolunteer.Out_time__c = null;  //Clear OutTime

				if(objRegisteredVolunteer.GW_Volunteers__Hours_Worked__c == 0) {
					objRegisteredVolunteer.Attended__c = false;  // Reset Registrant
					style = 'Attendees Absent';   
					status = 'Absent';                  
				} else {
					objRegisteredVolunteer.Attended__c = true;  // Reset Attendee
					style = 'Attendees SignedOut'; 
					status = 'SignedOut';
				}             
			} else {  // Registrant has just signed in
				objRegisteredVolunteer.In_time__c = DateTime.now();  // Set InTime to now
				objRegisteredVolunteer.Attended__c = true ;  // Set as Attended
				objRegisteredVolunteer.Out_time__c = null;  // Clear OutTime
				style = 'Attendees SignedIn';  // Set stye to SignedIn
				status = 'SignedIn';
			}

			// To reflect affiliate color to visualforce page after update
			for(AffiliatesDetails Affiliate : lstRegisterdVolunteers) {
				if(Affiliate.VolunteerHourId == affiliateIdToUpdate) {
					Affiliate.style = style;                 
					Affiliate.status = status;
				}
			}

			try {
				update objRegisteredVolunteer;
			}
			catch(DMLException ex) {
				System.debug('Error: '+ex.getMessage()+ '  at line: '+ex.getLineNumber());
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,
					'Error: ' + ex.getMessage()) );
			}
		
			lstRegisterdVolunteers.sort();
		
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
				'Error: Affiliate not found.'));
		}
	}
	
	
	/* 
	* Method SearchAffiliate() invoke on drop button action. Method search 
	* for affiliates by email address and register for Occasion.
	*/
	public void SearchAffiliate() { 
		list<Contact> lstSearchContacts = new list<Contact>();
		GW_Volunteers__Volunteer_Hours__c objRegisteredVolunteer = new GW_Volunteers__Volunteer_Hours__c();
		GW_Volunteers__Volunteer_Shift__c tempShift = new GW_Volunteers__Volunteer_Shift__c();
		try{
			//Search for Affiliate whose emailId belonges to Platoon of the Platoon Event 
			system.debug('strEmailId::::'+strEmailId);

			// Affiliate_Name__c is contact id add back in Service_Platoon__c,    
			lstSearchContacts = [SELECT id, Name, Email 
				FROM contact 
				WHERE Email =: strEmailId 
				/*AND Service_Platoon__c =: strPlatoonID 
				AND Eligible_for_Registration__c = true*/ LIMIT 1]; 
			
			tempShift = [SELECT id,Name FROM GW_Volunteers__Volunteer_Shift__c WHERE id =: shift.Id  LIMIT 1]; 

			if (tempShift != null && !setVolunteersId.contains(lstSearchContacts[0].id)) { // this is set of volunteer hours id and we are checking for contact  
																								// need to refine code here
				objRegisteredVolunteer.GW_Volunteers__Volunteer_Shift__c =  tempShift.id;
				//objRegisteredVolunteer.Affiliate_Number__c = lstSearchAffiliates[0].id;
				//objRegisteredVolunteer.Attended__c = true; 
				objRegisteredVolunteer.In_time__c = DateTime.now();
				objRegisteredVolunteer.GW_Volunteers__Hours_Worked__c = 0;
				//objRegisteredVolunteer.Attendee_Role__c = TMC_Definitions.REGISTRATION_ROLE_PARTIC;

				if(lstSearchContacts[0].Active__c == false
						/*&& lstSearchAffiliates[0].Status__c != TMC_Definitions.AFFILIATE_STATUS_PENDINGEVENT*/){
					lstSearchContacts[0].Active__c = true;
					lstSearchContacts[0].Status__c = null;
					update lstSearchContacts[0];
				}                                 
				volunteerName = lstSearchContacts[0].Name;

				try{ 
					insert objRegisteredVolunteer;
				}
				catch(DMLEXception ex){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
						'Error: Invalid EmailId.'+ex.getMessage()));
				}
				
				setVolunteersId.add(lstSearchContacts[0].id);
				lstRegisterdVolunteers.add(new AffiliatesDetails(objRegisteredVolunteer,volunteerName));
				strEmailId = '';
				b_searchFlag =true;
				successMessage = lstSearchcontacts[0].Name + ' has been registered for ' + tempShift.Name;
				errorMessage ='';

			} 
			else {
				
				successMessage =  lstSearchContacts[0].Name +' is already registered for' + tempShift.Name + '. Please check in.' ;
				errorMessage ='';
				b_searchFlag =false; // for already exist
				if (lstSearchContacts[0].Active__c == false
						/*&& lstSearchContacts[0].Status__c != TMC_Definitions.AFFILIATE_STATUS_PENDINGEVENT*/){
					lstSearchContacts[0].Active__c = true;
					lstSearchContacts[0].Status__c = null;
					update lstSearchContacts[0];
				}
				
			}
			   lstRegisterdVolunteers.sort();
		}
		catch( Exception ex ) {
			b_searchFlag = false;
			errorMessage = strEmailId + ' is not a valid email associated with this Platoon or this Event. Please re-enter email address, or register for the Event on the registration website.';
			successMessage = '';
		}
	}
	

	/* 
	* Inner class AffiliatesDetails used to store the necessary information 
	* for processing VF page 
	*/
	public class AffiliatesDetails implements Comparable {
		public String VolunteerName { get; set; }
		public ID VolunteerHourId { get; set; }
		public String style { get; set; }
		public String status { get; set; }
		public Boolean hours { get; set; } 
		public Boolean inTime ;
	   
		public AffiliatesDetails(GW_Volunteers__Volunteer_Hours__c volHoursObj) 
		{   
			this.VolunteerName = volHoursObj.GW_Volunteers__Contact__r.Name;
			this.VolunteerHourId = volHoursObj.id; 
		   
			if(volHoursObj.In_time__c != null) {
				status = 'SignedIn';
			} else if(volHoursObj.GW_Volunteers__Hours_Worked__c == 0) {
				status = 'Absent';
			}else{
				status = 'SignedOut';
			}

			SetStyle(status);
		}
		
		public AffiliatesDetails(GW_Volunteers__Volunteer_Hours__c volHoursObj,String volunteerName) {   
			this.VolunteerName = volunteerName;
			this.VolunteerHourId = volHoursObj.id;
			
			if(volHoursObj.In_time__c != null) {
				status = 'SignedIn';
			} else if(volHoursObj.GW_Volunteers__Hours_Worked__c == 0) {
				status = 'Absent';
			}else{
				status = 'SignedOut';
			}

			SetStyle(status);
		}
		
		public void SetStyle(String status) {
			if(status == 'SignedIn') {
				style = 'Attendees SignedIn';
			} else if(status == 'SignedOut') {
				style = 'Attendees SignedOut';
			} else {
				style = 'Attendees Absent';
			}  
		}

		public Integer compareTo(Object ObjToCompare) {
			return VolunteerName.CompareTo(((AffiliatesDetails)ObjToCompare).VolunteerName);
		}
	}

}