/*
 *Summary : Trigger is used for the updation of the CongressionalDistrict in the Account. 
 *          Trigger calls the future method and then do the web service call to the GeoCoder API
 *          The return values from the Geocoder are extracted and updated in the Account. On the basis  of the
 *          values of CongressionalDistrict it retrives the Congressional District look up and update it to Account
 *
 *Written By : Pravin Waykar
 *
 *Date : 28/07/2014
 */ 


trigger OnAccountInsertUpdateProcessCongDist on Account (after insert, after update) {
    
/*
Can only make 10 calls to a future method and external webservice. In any method can only make 10 callouts.

CNTRL_AccountInsertUpdateAddCongDistrict will make a callout for each Account updated/inserted.

Divide the Account into batches of 8.
*/

list<Id> lstAcctIds = new list<Id>();
list<Account> lstAccount  = new list<Account>();
list<Account> lstAccUpdate  = new list<Account>();
if( checkRecursive.runOnce() && !ProcessorControl.inAccountFutureContext )
  {
        Triggers__c  TriggerSettingAccount  = Triggers__c.getValues('OnAccountInsertUpdateProcessCongDist');
        if( TriggerSettingAccount != null && TriggerSettingAccount.Active__c ) //If trigger is active from the custom setting then only call the trigger method
        {
            sObject oldAcc = new Account();
            lstAccount = new list<Account>([SELECT Name,ShippingCountry,ShippingPostalCode,Shipping_Zip9__c,USG_Trigger_Update__c,USG_Update_Requested__c,Metropolitan_Statistical_Area__c ,USG_Update_Status__c,ShippingStreet  FROM Account WHERE id IN : Trigger.new]);// NO Custom Object Congressional_District__c
            
            lstAccUpdate  = new list<Account>();
            for( Account Acc: lstAccount )
            {
                oldAcc = new Account();
                if( Trigger.isUpdate )
                        oldAcc = Trigger.oldMap.get(Acc.Id);
                  
                 // fire the trigger when address is edited or the update is requested by the user      
                if( Trigger.isUpdate )
                {   
                   
                   boolean isUpdate = (
                   
                   (
                        oldAcc.get('ShippingPostalCode') != Acc.ShippingPostalCode ||
                        oldAcc.get('ShippingStreet') != Acc.ShippingStreet ||
                            (
                                oldAcc.get('ShippingCountry') != Acc.get('ShippingCountry') &&
                                (
                                    oldAcc.get('ShippingCountry') != null || 
                                    oldAcc.get('ShippingCountry') != 'US' || 
                                    oldAcc.get('ShippingCountry') != 'USA' || 
                                    oldAcc.get('ShippingCountry') != 'United States'
                                )
                            )
                    )
                   
                   
                   
                   );
                   
                   
                   
                    if( Acc.USG_Trigger_Update__c == true ) // make API Call
                    {
                        Acc.USG_Trigger_Update__c = false;
                        Acc.USG_Update_Requested__c = DateTime.Now();
                        Acc.USG_Update_Status__c = 'Update Requested';
                        lstAcctIds.add(Acc.id);
                        lstAccUpdate.add(Acc);
                    }
                   else // make API Call
                   if ((Acc.ShippingCountry == 'United States' || Acc.ShippingCountry == '' || Acc.ShippingCountry == 'US' || Acc.ShippingCountry == 'USA' || Acc.ShippingCountry == null ))
                   {
                       if( (oldAcc.get('ShippingPostalCode') != Acc.ShippingPostalCode || oldAcc.get('ShippingStreet') != Acc.ShippingStreet) && Acc.ShippingPostalCode != null )
                       {
                                Acc.USG_Trigger_Update__c = false;
                                Acc.USG_Update_Requested__c = DateTime.Now();
                                Acc.USG_Update_Status__c = 'Update Requested';
                                lstAcctIds.add(Acc.id);
                                lstAccUpdate.add(Acc);
                        }
                   }
                    else
                    if( Acc.ShippingPostalCode == '' || Acc.ShippingPostalCode == null )// NO API Call If No zip code
                    {
                        Acc.USG_Trigger_Update__c = false;
                        Acc.USG_Update_Requested__c = DateTime.Now();
                        Acc.Longitude__c = '';
                        Acc.Latitude__c = '';
                        Acc.Shipping_Zip9__c = '';
                        Acc.Metropolitan_Statistical_Area__c = '';
                        //Acc.Congressional_District__c = null; // comment no Congressional_District__c  object
                        Acc.USG_Update_Status__c = 'Zip Code Required for Web Service';
                       
                        system.debug('in update trigger if');
                        lstAccUpdate.add(Acc);
                    }
                    else // NO API Call on if address not changed
                    if ((Acc.ShippingCountry == 'United States' || Acc.ShippingCountry == '' || Acc.ShippingCountry == 'US' || Acc.ShippingCountry == 'USA' || Acc.ShippingCountry == null ) && ( oldAcc.get('ShippingPostalCode') == Acc.ShippingPostalCode || oldAcc.get('ShippingStreet') == Acc.ShippingStreet ) && Acc.ShippingPostalCode != null )                   
                    {
                        Acc.USG_Trigger_Update__c = false;
                        Acc.USG_Update_Requested__c = DateTime.Now();
                        Acc.USG_Update_Status__c = 'No Address Change on Update';
                        system.debug('in update trigger if');
                        lstAccUpdate.add(Acc);
                    }
                    else // No API call if country is not US
                    if( Acc.ShippingCountry != 'United States' || Acc.ShippingCountry != '' || Acc.ShippingCountry != 'US' || Acc.ShippingCountry != 'USA' || Acc.ShippingCountry != null )  
                    {
                        Acc.USG_Trigger_Update__c = false;
                        Acc.USG_Update_Requested__c = DateTime.Now();
                        Acc.Longitude__c = '';
                        Acc.Latitude__c = '';
                        Acc.Shipping_Zip9__c = '';
                        Acc.Metropolitan_Statistical_Area__c = '';
                        //Acc.Congressional_District__c = null; // comment no Congressional_District__c  object
                        Acc.USG_Update_Status__c = ' Web Service for US Addresses only';
                        lstAccUpdate.add(Acc);  
                    }

               }
               
               
               if( Trigger.isInsert )
               {
                   
                    // fires trigger for the US contry 
                    if ( Acc.ShippingCountry == 'United States' || Acc.ShippingCountry == '' || Acc.ShippingCountry == 'US' || Acc.ShippingCountry == 'USA' || Acc.ShippingCountry == null )
                    {
                        Acc.USG_Trigger_Update__c = false;
                        Acc.USG_Update_Requested__c = DateTime.Now();
                        Acc.USG_Update_Status__c = 'Request Submitted';
                        lstAcctIds.add(Acc.id);
                        lstAccUpdate.add(Acc);       
                    }   
                    else // NO API Call If No zip code
                    if( Acc.ShippingPostalCode == '' || Acc.ShippingPostalCode == null )
                    {
                        Acc.USG_Trigger_Update__c = false;
                        Acc.USG_Update_Requested__c = DateTime.Now();
                        Acc.USG_Update_Status__c = 'Zip Code Required for Web Service';
                        //lstContIds.add(con.id);
                        system.debug('in update trigger if');
                        lstAccUpdate.add(Acc);
                    }
                    else // No API call if country is not US
                    if( Acc.ShippingCountry != 'United States' || Acc.ShippingCountry != '' || Acc.ShippingCountry != 'US' || Acc.ShippingCountry != 'USA' || Acc.ShippingCountry != null )  
                    {
                        Acc.USG_Trigger_Update__c = false;
                        Acc.USG_Update_Requested__c = DateTime.Now();
                        Acc.USG_Update_Status__c = ' Web Service for US Addresses only';
                       // lstContIds.add(con.id)
                        lstAccUpdate.add(Acc);  
                    }
                                   
               }
           }
           
          if( lstAccUpdate != null && lstAccUpdate.size() > 0 )
         {   
             set<Account> setAcct = new set<Account>();
             setAcct.addAll(lstAccUpdate);
             lstAccUpdate.clear();
             lstAccUpdate.addAll(setAcct);
             update lstAccUpdate;       
         }
                
         if(lstAcctIds.size() >0 ) {

        /*
        Governor limits only allow 10 calls to a future method per trigger.

        For each Account the Future method is GeoUtilities.updateGeocodes will make a callout for each Account.

        Divide the Accounts into batches of 10.

        */

        Integer count = 0;
        Integer i = 0;
        Integer numAccounts = lstAcctIds.size();
        numAccounts = (numAccounts > 100 ? 100: numAccounts);  // This is a govenor on number of Accounts that can be updated - but we don't need to cap it at 100


         while(i < numAccounts){
    
                List <ID> smallList = new List<ID>();
    
                count = ((numAccounts-i) > 10 ? 10 : (numAccounts-i));  // Break into batches of 10
     
                for(Integer k=0; k<count; k++) {
     
                   smallList.add(lstAcctIds[i]);
    
                   i++;
    
                }
    
                system.Debug('Calling Utilities');  
                CNTRL_AccountInsertUpdateAddCongDistrict.CNTRL_AccountInsertUpdateAddCongDistrict(smallList);
            }
    
        }    
        // end of for }
            
    }
  }
}