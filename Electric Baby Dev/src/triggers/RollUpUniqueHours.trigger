trigger RollUpUniqueHours on GW_Volunteers__Volunteer_Hours__c (after insert,after update)
{
    list<GW_Volunteers__Volunteer_Hours__c> lstVolunteerHours = new list<GW_Volunteers__Volunteer_Hours__c>();
    list<GW_Volunteers__Volunteer_Shift__c> lstShiftTobeUpdated = new list<GW_Volunteers__Volunteer_Shift__c>();
    lstVolunteerHours = trigger.new;
    set<id> setConId = new set<id>();
    set<id> setVolHourId = new set<id>();
    set<id> setVolShiftId = new set<id>();
    
    for( GW_Volunteers__Volunteer_Hours__c volHours : lstVolunteerHours )
    {
        setVolHourId.add(volHours.id);
        setVolShiftId.add(volHours.GW_Volunteers__Volunteer_Shift__c);
    }
    
    // query the related shift of the hours
    
    list<GW_Volunteers__Volunteer_Shift__c> lstShifts = new list<GW_Volunteers__Volunteer_Shift__c>([SELECT Id,Name,GW_Volunteers__Total_Volunteers__c,(SELECT Id,Name,GW_Volunteers__Contact__c FROM GW_Volunteers__Volunteer_Hours__r )FROM GW_Volunteers__Volunteer_Shift__c WHERE Id IN : setVolShiftId]);
    
    for(GW_Volunteers__Volunteer_Shift__c volShift : lstShifts )
    {
        setConId.clear();
        for(GW_Volunteers__Volunteer_Hours__c volunteerHours : volShift.GW_Volunteers__Volunteer_Hours__r )
        {
           setConId.add(volunteerHours.GW_Volunteers__Contact__c);       
        }
        
        volShift.GW_Volunteers__Total_Volunteers__c = setConId.size();
        lstShiftTobeUpdated.add(new GW_Volunteers__Volunteer_Shift__c(id = volShift.id));
    }
    
    if(lstShiftTobeUpdated != null && lstShiftTobeUpdated.size() > 0)
        update lstShiftTobeUpdated;

}