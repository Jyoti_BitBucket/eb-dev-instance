/*
 *Summary : Trigger is used for the updation of the CongressionalDistrict in the contact. 
 *          Trigger calls the future method and then do the web service call to the GeoCoder API
 *          The return values from the Geocoder are extracted and updated in the Contact. On the basis  of the
 *          values of CongressionalDistrict it retrives the Congressional District look up and update it to Contact
 *
 *Written By : Pravin Waykar
 *
 *Date : 28/07/2014
 */ 


trigger OnContactInsertUpdateProcessCongDist on Contact (after insert, after update) {
    
/*
Can only make 10 calls to a future method and external webservice. In any method can only make 10 callouts.

CNTRL_ContactInsertUpdateAddCongDistrict will make a callout for each contact updated/inserted.

Divide the contact into batches of 8.
*/

 list<Id> lstContIds = new list<Id>();
 list<Contact> lstContact = new list<Contact>();
 list<Contact> lstConUpdate = new list<Contact>();
 if( checkRecursive.runOnce() && !ProcessorControl.inContactFutureContext )
 {
        Triggers__c  TriggerSettingContact  = Triggers__c.getValues('OnContactInsertUpdateProcessCongDist');
        if( TriggerSettingContact != null && TriggerSettingContact.Active__c ) //If trigger is active from the custom setting then only call the trigger method
        {
            system.debug('Trigger run in custom setting active'); 
            lstContact = new list<Contact>([SELECT FirstName,LastName,OtherCountry,OtherPostalCode,Shipping_Zip9__c,USG_Trigger_Update__c,USG_Update_Requested__c,Metropolitan_Statistical_Area__c ,USG_Update_Status__c,OtherStreet FROM Contact WHERE id IN : Trigger.new]);// Comment No Object Congressional_District__c
            
            sObject oldCon = new Contact();
            
            lstConUpdate = new list<Contact>();
              
            for( Contact con : lstContact )
            {
                oldCon = new Contact();
                if( Trigger.isUpdate )
                        oldCon = Trigger.oldMap.get(con.Id);
                  
                 // fire the trigger when address is edited or the update is requested by the user      
                if( Trigger.isUpdate )
                {   system.debug('==========>> con.USG_Trigger_Update__c <<========='+con.USG_Trigger_Update__c);
                    if( con.USG_Trigger_Update__c == true ) // make API Call
                    {
                        con.USG_Trigger_Update__c = false;
                        con.USG_Update_Requested__c = DateTime.Now();
                        con.USG_Update_Status__c = 'Update Requested';
                        lstContIds.add(con.id);
                        lstConUpdate.add(con);
                    }
                    else // make API Call
                    if ((con.OtherCountry == 'United States' || con.OtherCountry == '' || con.OtherCountry == 'US' || con.OtherCountry == 'USA' || con.OtherCountry == null ) && ( oldCon.get('OtherPostalCode') != con.OtherPostalCode || oldCon.get('OtherStreet') != con.OtherStreet ) && con.OtherPostalCode != null )
                    {
                        con.USG_Trigger_Update__c = false;
                        con.USG_Update_Requested__c = DateTime.Now();
                        con.USG_Update_Status__c = 'Update Requested';
                        lstContIds.add(con.id);
                        lstConUpdate.add(con);       
                    }
                    else
                    if( con.OtherPostalCode == '' || con.OtherPostalCode == null )// NO API Call If No zip code
                    {
                        con.USG_Trigger_Update__c = false;
                        con.USG_Update_Requested__c = DateTime.Now();
                        con.Longitude__c = '';
                        con.Latitude__c = '';
                        con.Shipping_Zip9__c = '';
                        con.Metropolitan_Statistical_Area__c = ''; 
                        //con.Congressional_District__c = null; // Comment No Object Congressional_District__c
                        con.USG_Update_Status__c = 'Zip Code Required for Web Service';
                        //lstContIds.add(con.id);
                        system.debug('in update trigger if');
                        lstConUpdate.add(con);
                    }
                    else // NO API Call on if address not changed
                    if ( ( oldCon.get('OtherPostalCode') == con.OtherPostalCode && oldCon.get('OtherStreet') == con.OtherStreet && con.OtherCountry == oldCon.get('OtherCountry')) )                   
                    {
                        con.USG_Trigger_Update__c = false;
                        con.USG_Update_Requested__c = DateTime.Now();
                        con.USG_Update_Status__c = 'No Address Change on Update';
                        //lstContIds.add(con.id);
                        system.debug('in update trigger if');
                        lstConUpdate.add(con);
                    }
                    else // No API call if country is not US
                    if( (con.OtherCountry != 'United States' || con.OtherCountry != '' || con.OtherCountry != 'US' || con.OtherCountry != 'USA' || con.OtherCountry != null) && ( con.OtherCountry != oldCon.get('OtherCountry') )  )  
                    {
                        con.USG_Trigger_Update__c = false;
                        con.USG_Update_Requested__c = DateTime.Now();
                        con.Longitude__c = '';
                        con.Latitude__c = '';
                        con.Shipping_Zip9__c = '';
                        con.Metropolitan_Statistical_Area__c = '';
                        //con.Congressional_District__c = null; // Comment No Object Congressional_District__c
                        con.USG_Update_Status__c = ' Web Service for US Addresses only';
                       // lstContIds.add(con.id)
                        lstConUpdate.add(con);  
                    }
                    
               }
             
               if( Trigger.isInsert )
               {
                    if (( con.OtherCountry == 'United States' || con.OtherCountry == '' || con.OtherCountry == 'US' || con.OtherCountry == 'USA' || con.OtherCountry == null) && (con.OtherPostalCode != '' || con.OtherPostalCode != null) )
                    {
                        con.USG_Trigger_Update__c = false;
                        con.USG_Update_Requested__c = DateTime.Now();
                        con.USG_Update_Status__c = 'Request Submitted';
                        lstContIds.add(con.id);
                        lstConUpdate.add(con); 
                        system.debug('Insert trigger in list');      
                    }  
                    else// fires trigger for the US contry 
                    if( con.OtherPostalCode == '' || con.OtherPostalCode == null )// NO API Call If No zip code
                    {
                        con.USG_Trigger_Update__c = false;
                        con.USG_Update_Requested__c = DateTime.Now();
                        con.USG_Update_Status__c = 'Zip Code Required for Web Service';
                        //lstContIds.add(con.id);
                        system.debug('in update trigger if');
                        lstConUpdate.add(con);
                    }
                    else // No API call if country is not US
                    if( con.OtherCountry != 'United States' || con.OtherCountry != '' || con.OtherCountry != 'US' || con.OtherCountry != 'USA' || con.OtherCountry != null )  
                    {
                        con.USG_Trigger_Update__c = false;
                        con.USG_Update_Requested__c = DateTime.Now();
                        con.USG_Update_Status__c = ' Web Service for US Addresses only';
                        system.debug('No API call if country is not US in INSERT');
                       // lstContIds.add(con.id)
                        lstConUpdate.add(con);  
                    }
                }
           }
         if( lstConUpdate != null && lstConUpdate.size() > 0 )
         {   
             set<contact> setCont = new set<Contact>();
             setCont.addAll(lstConUpdate);
             lstConUpdate.clear();
             lstConUpdate.addAll(setCont);
             update lstConUpdate;       
         }
         if(lstContIds.size() >0 ) {

        /*
        Governor limits only allow 10 calls to a future method per trigger.

        For each Contact the Future method is GeoUtilities.updateGeocodes will make a callout for each Contact.

        Divide the Contacts into batches of 10.

        */

        Integer count = 0;
        Integer i = 0;
        Integer numContacts = lstContIds.size();
        numContacts = (numContacts > 100 ? 100: numContacts);  // This is a govenor on number of contacts that can be updated - but we don't need to cap it at 100


         while(i < numContacts){
    
                List <ID> smallList = new List<ID>();
    
                count = ((numContacts-i) > 10 ? 10 : (numContacts-i));  // Break into batches of 10
     
                for(Integer k=0; k<count; k++) {
     
                   smallList.add(lstContIds[i]);
    
                   i++;
    
                }
    
                system.Debug('Calling Utilities');
                 
                GeocoderController.updateGeocoderData(smallList,'Contact');
            }
    
        }
           // end of for }
    }
  }
}