trigger MailingAddressLocator on Contact (after insert,  before update) {

    system.debug(Logginglevel.ERROR,'Mailing Address Locator Geo Updates');

    List <ID> IDsToGeocode = new List<ID>(); // List of IDs to Update for before update

    List<Contact> ContactsToInsert = new List<Contact>();  // List of Contacts to Update for after insert

    Boolean insertOperation = false;  // Flag indicating to update Contacts After Insert
    Boolean needsGeocoding = false;  // Flag indicating if there are Contacts to update

 
    if( Trigger.isInsert ) {
    system.debug('Insert Operation');
        // For After Insert operation we make query for the Contacts that are in the Trigger because we cannot perform updates directly after insert
        ContactsToInsert = new list<Contact>([select id, Mailing_Location__latitude__s, Mailing_Location__longitude__s, Geocode_Update_Status__c from Contact WHERE Id IN: Trigger.newMap.keySet()]);
    }else{
        system.debug('Update Operation');
        // For Before Update operation we can make changes directly to the Trigger Contacts because the Update operation has not been completed 
        ContactsToInsert = Trigger.new;
    }

    system.debug('Number of contacts to update: ' + ContactsToInsert.size());
    for(Contact eachContact: ContactsToInsert ){  // Itterate through Contacts in Trigger

        needsGeocoding = false;  // Flag indicating if there are Contacts to update
        
        // Check if new Contacts for insert operation ot if update operationa and if any of those updated contacts have new addresses
                
        if(Trigger.isInsert){

            system.debug('New Contact for update');
            needsGeocoding = true;
            insertOperation = true;

            // Query Contacts in Trigger because we can't update them directly
             

        } else { // Trigger is before update operation

            Contact beforeUpdate = System.Trigger.oldMap.get(eachContact.Id);  // Retrieve Contact from Trigger Map and see if the address has been changed and needs new Geocodes

            needsGeocoding = (  // Check to see if updated Contact has updated address that requires geocodes be updated
                (beforeUpdate.MailingStreet != eachContact.MailingStreet) ||
                (beforeUpdate.MailingCity != eachContact.MailingCity) ||
                (beforeUpdate.MailingState != eachContact.MailingState) ||
                (beforeUpdate.MailingPostalCode != eachContact.MailingPostalCode)
            );

        }

        if(needsGeocoding) {

          //  If(!insertOperation) {  // Reset the coordinates and set Status = Requested for Inserted Contact records
                eachContact.Mailing_Location__latitude__s = 0;
                eachContact.Mailing_Location__longitude__s = 0;
                eachContact.Geocode_Update_Status__c = 'Requested';
         //   }

            IDsToGeocode.add(eachContact.Id);  // Add Id to list of Contacts to get coordinates for
        }

     }

    system.debug('Number of IDs to update: ' +IDsToGeocode.size());

    if(IDsToGeocode.size() >0 ) {

        /*
        Governor limits only allow 10 calls to a future method per trigger.

        For each Contact the Future method is GeoUtilities.updateGeocodes will make a callout for each Contact.

        Divide the Contacts into batches of 10.

        */

        // Set the Geocoordinates and status for inserted Contacts
        If(insertOperation) { 
            // Reset coordinates and set Status = Requested for Inserts
      //      for (Contact insertContacts: ContactsToInsert){
      //          insertContacts.Mailing_Location__latitude__s = 0;
      //          insertContacts.Mailing_Location__longitude__s = 0;
       //         insertContacts.Geocode_Update_Status__c = 'Requested';
       //     }

            update ContactsToInsert;  // Update inserted Contacts 
        }

        Integer count = 0;
        Integer i = 0;
        Integer numContacts = IDsToGeocode.size();
  system.debug('2Number of contacts to update: ' +numContacts);
        numContacts = (numContacts > 100 ? 100: numContacts);  // This is a govenor on number of contacts that can be updated - but we don't need to cap it at 100
system.debug('numcontacts truncated: ' + numContacts);

        while(i < numContacts){

            List <ID> smallList = new List<ID>();

            count = ((numContacts-i) > 10 ? 10 : (numContacts-i));  // Break into batches of 10
 
            for(Integer k=0; k<count; k++) {
 
               smallList.add(IDsToGeocode[i]);

               i++;

            }

            system.Debug('Calling Utilities');
            
            GeoUtilities.updateGeocodes(smallList);
        }

    }

}