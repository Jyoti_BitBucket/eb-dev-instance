trigger DeleteNonQualifiedVolunteer on GW_Volunteers__Volunteer_Hours__c (after insert, after undelete, after update, before delete)
{
/* Summary    : Delete Non qualified volunteer if check box on volunteer hours is checked delete the record
*             Also update the desired number of volunteer in sibling shift of volunteer hour shift if criteria is fullfilled
* 
*/
    List<GW_Volunteers__Volunteer_Hours__c> lstVolunteerHours;
    if( ! Trigger.isDelete )
        lstVolunteerHours = Trigger.New;
    else
        lstVolunteerHours = Trigger.Old;
    List<Triggers__c> lstTriggerSetting = new List<Triggers__c>();
    Triggers__c  TriggerSetting  = Triggers__c.getValues('DeleteNonQualifiedVolunteer');
    IF( TriggerSetting.Active__c )  // if custom setting is actve then on ly invoke the trigger
        CNTRL_DeleteNonQualifiedVolunteer.DeleteNonQualifiedVolunteer( lstVolunteerHours );

}